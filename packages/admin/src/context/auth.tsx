import React from 'react';
import { useDispatch } from 'react-redux';
import { LOGIN_ADMIN_START } from 'redux/constants';

type AuthProps = {
  isAuthenticated: boolean;
  authenticate: Function;
  signout: Function;
};

export const AuthContext = React.createContext({} as AuthProps);

const isValidToken = () => {
  const token = localStorage.getItem('pickbazar_token');
  // JWT decode & check token validity & expiration.
  if (token) return true;
  return false;
};
const AuthProvider = (props: any) => {
  let dispatch=useDispatch();
  const [isAuthenticated, makeAuthenticated] = React.useState(isValidToken());
  function authenticate({ username, password }, cb) {
    return new Promise((resolve, reject) => {
      dispatch({ type: LOGIN_ADMIN_START, payload: { username,password,resolve, reject } });
    })
    .then(({token}) => {
    makeAuthenticated(true);
    localStorage.setItem('pickbazar_token', `${token}`);
    }).catch((err) => {
      console.log("error")
      makeAuthenticated(false);
      setTimeout(cb, 100);
    });
  }
  function signout(cb) {
    makeAuthenticated(false);
    localStorage.removeItem('pickbazar_token');
    setTimeout(cb, 100);
  }
  return (
    <AuthContext.Provider
      value={{
        isAuthenticated,
        authenticate,
        signout,
      }}
    >
      <>{props.children}</>
    </AuthContext.Provider>
  );
};

export default AuthProvider;
