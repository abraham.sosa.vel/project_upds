import React, { useCallback, useEffect, useState } from 'react';
import dayjs from 'dayjs';
import { withStyle, createThemedUseStyletron } from 'baseui';
import { Grid, Row as Rows, Col as Column } from 'components/FlexBox/FlexBox';
import { useDrawerDispatch } from 'context/DrawerContext';
import ProgressBar from 'components/ProgressBar/ProgressBar';

import Select from 'components/Select/Select';
import Input from 'components/Input/Input';
import Button from 'components/Button/Button';

import { Plus } from 'assets/icons/PlusMinus';
import { useQuery, gql } from '@apollo/client';
import { Wrapper, Header, Heading } from 'components/Wrapper.style';
import Checkbox from 'components/CheckBox/CheckBox';

import {
  TableWrapper,
  StyledTable,
  StyledHeadCell,
  StyledBodyCell,
  ProgressWrapper,
  ProgressText,
  Status,
} from './Coupon.style';
import NoResult from 'components/NoResult/NoResult';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import { INITIAL_REQUEST_BRANCH_START } from 'redux/constants';
import { ErrorMessage } from 'formik';

const GET_COUPONS = gql`
  query getCoupons($status: String, $searchBy: String) {
    coupons(status: $status, searchBy: $searchBy) {
      id
      title
      code
      number_of_used_coupon
      number_of_coupon
      expiration_date
      creation_date
      status
    }
  }
`;

type CustomThemeT = { red400: string; textNormal: string; colors: any };
const themedUseStyletron = createThemedUseStyletron<CustomThemeT>();

const Col = withStyle(Column, () => ({
  '@media only screen and (max-width: 767px)': {
    marginBottom: '20px',

    ':last-child': {
      marginBottom: 0,
    },
  },
}));

const Row = withStyle(Rows, () => ({
  '@media only screen and (min-width: 768px)': {
    alignItems: 'center',
  },
}));

const statusSelectOptions = [
  { value: 'active', label: 'Active' },
  { value: 'revoked', label: 'Revoked' },
];

export default function Coupons() {
  const dispatch = useDrawerDispatch();
  const [checkedId, setCheckedId] = useState([]);
  const [checked, setChecked] = useState(false);
  let dispatcher=useDispatch();

  const openDrawer = useCallback(
    () => dispatch({ type: 'OPEN_DRAWER', drawerComponent: 'CAMPAING_FORM' }),
    [dispatch]
  );
  const [status, setStatus] = useState([]);
  const [search, setSearch] = useState('');
  const [useCss, theme] = themedUseStyletron();
  const active = useCss({
    ':before': {
      content: '""',
      backgroundColor: theme.colors.primary,
    },
  });
  const revoked = useCss({
    ':before': {
      content: '""',
      backgroundColor: theme.colors.red400,
    },
  });

  const { data, error, refetch } = useQuery(GET_COUPONS);
  function handleSelect({ value }) {
    setStatus(value);
    if (value.length) {
      refetch({ status: value[0].value, searchBy: search });
    } else {
      refetch({ status: null });
    }
  }

  function handleSearch(event) {
    const value = event.currentTarget.value;

    setSearch(value);
    refetch({
      status: status.length ? status[0].value : null,
      searchBy: value,
    });
  }
  function onAllCheck(event) {
    if (event.target.checked) {
      const idx = data && data.coupons.map((coupon) => coupon.id);
      setCheckedId(idx);
    } else {
      setCheckedId([]);
    }
    setChecked(event.target.checked);
  }

  function handleCheckbox(event) {
    const { name } = event.currentTarget;
    if (!checkedId.includes(name)) {
      setCheckedId((prevState) => [...prevState, name]);
    } else {
      setCheckedId((prevState) => prevState.filter((id) => id !== name));
    }
  }

  const numberToPercent = (num, total) => {
    return (num * 100) / total;
  };
  useEffect(()=>{
    dispatcher({type:INITIAL_REQUEST_BRANCH_START,payload:""})
  }, [])
  const datas = useSelector((state: RootStateOrAny) => state);
  console.log(datas)
  return (
    <Grid fluid={true}>
      <Row>
        <Col md={12}>
          <Header
            style={{
              marginBottom: 30,
              boxShadow: '0 0 5px rgba(0, 0 ,0, 0.05)',
            }}
          >
            <Col md={2}>
              <Heading>Sucursales</Heading>
            </Col>

            <Col md={10}>
              <Row>

                <Col md={4}>
                  <Button
                    onClick={openDrawer}
                    startEnhancer={() => <Plus />}
                    overrides={{
                      BaseButton: {
                        style: ({ $theme, $size, $shape }) => {
                          return {
                            width: '100%',
                            borderTopLeftRadius: '3px',
                            borderTopRightRadius: '3px',
                            borderBottomLeftRadius: '3px',
                            borderBottomRightRadius: '3px',
                          };
                        },
                      },
                    }}
                  >
                    Agregar sucursal
                  </Button>
                </Col>
              </Row>
            </Col>
          </Header>

          <Wrapper style={{ boxShadow: '0 0 5px rgba(0, 0 , 0, 0.05)' }}>
            <TableWrapper>
              <StyledTable $gridTemplateColumns="minmax(200px, 200px) minmax(170px, auto) minmax(200px, auto) minmax(100px, 100px) ">
                <StyledHeadCell>ID</StyledHeadCell>
                <StyledHeadCell>Nombre</StyledHeadCell>
                <StyledHeadCell>Direccion</StyledHeadCell>
                <StyledHeadCell>Telefono</StyledHeadCell>

                {datas ? (
                  datas.coupons.length ? (
                    datas.coupons
                      .map((item) => Object.values(item))
                      .map((row, index) => {
                        return (
                          <React.Fragment key={index}>
                            <StyledBodyCell>{row[0]}</StyledBodyCell>
                            <StyledBodyCell>{row[1]}</StyledBodyCell>
                            <StyledBodyCell>{row[2]}</StyledBodyCell>
                            <StyledBodyCell>{row[3]}</StyledBodyCell>
                          </React.Fragment>
                        );
                      })
                  ) : (
                    <NoResult
                      hideButton={false}
                      style={{
                        gridColumnStart: '1',
                        gridColumnEnd: 'one',
                      }}
                    />
                  )
                ) : null}
              </StyledTable>
            </TableWrapper>
          </Wrapper>
        </Col>
      </Row>
    </Grid>
  );
}
