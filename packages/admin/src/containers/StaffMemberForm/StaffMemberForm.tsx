import React, { useCallback, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { v4 as uuidv4 } from 'uuid';
import { useMutation, gql } from '@apollo/client';
import { Scrollbars } from 'react-custom-scrollbars';
import { useDrawerDispatch, useDrawerState } from 'context/DrawerContext';
import Input from 'components/Input/Input';
import Checkbox from 'components/CheckBox/CheckBox';
import PhoneInput from 'components/PhoneInput/PhoneInput';
import Button, { KIND } from 'components/Button/Button';

import DrawerBox from 'components/DrawerBox/DrawerBox';
import { Row, Col } from 'components/FlexBox/FlexBox';
import {
  Form,
  DrawerTitleWrapper,
  DrawerTitle,
  FieldDetails,
  ButtonGroup,
} from '../DrawerItems/DrawerItems.style';
import { FormFields, FormLabel } from 'components/FormFields/FormFields';
import { Select } from 'baseui/select';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import { INITIAL_REQUEST_ORDERS_SECOND_START, REGISTER_DETAIL_DELIVERY_START, REQUEST_BRANCHES_START, UPDATE_DETAIL_DELIVERY_START } from 'redux/constants';

const GET_STAFFS = gql`
  query getStaffs($role: String, $searchBy: String) {
    staffs(role: $role, searchBy: $searchBy) {
      id
      name
      email
      contact_number
      creation_date
      role
    }
  }
`;

const CREATE_STAFF = gql`
  mutation createStaff($staff: AddStaffInput!) {
    createStaff(staff: $staff) {
      id
      first_name
      last_name
      name
      email
      contact_number
      creation_date
      role
    }
  }
`;

type Props = any;

const StaffMemberForm: React.FC<Props> = (props) => {
  const dispatch = useDrawerDispatch();
  const closeDrawer = useCallback(() => dispatch({ type: 'CLOSE_DRAWER' }), [
    dispatch,
  ]);
  const [country, setCountry] = React.useState(undefined);
  const [checked, setChecked] = React.useState(true);
  const [text, setText] = React.useState('');
  const dispacher=useDispatch();
  const data = useDrawerState('data');
  const { register, handleSubmit, setValue } = useForm({
    defaultValues: data,
  });
  const [tag, setTag] = useState([]);
  const [type, setType] = useState([]);
  const [type2, setType2] = useState([]);
  const [createStaff] = useMutation(CREATE_STAFF, {
    update(cache, { data: { createStaff } }) {
      const { staffs } = cache.readQuery({
        query: GET_STAFFS,
      });

      cache.writeQuery({
        query: GET_STAFFS,
        data: { staffs: staffs.concat([createStaff]) },
      });
    },
  });
  useEffect(()=>{
    dispacher({type:INITIAL_REQUEST_ORDERS_SECOND_START,payload:data})
    dispacher({type:REQUEST_BRANCHES_START})
  },[])
  const datass = useSelector((state: RootStateOrAny) => state);
  console.log(datass)
  const onSubmit = (data) => {
    if(tag[0]==undefined){
      dispacher({type:UPDATE_DETAIL_DELIVERY_START,payload:{id:datass.orderDetail[0]?.detailDeliveries[0]?.id,branchId:datass.orderDetail[0]?.detailDeliveries[0]?.branch.id,statusDeliveryId:type2[0].value,detailPaymentId:datass.orderDetail[0]?.id}})
    }else{
      dispacher({type:REGISTER_DETAIL_DELIVERY_START,payload:{branchId:tag[0].value,statusDeliveryId:type2[0].value,detailPaymentId:datass.orderDetail[0]?.id}})
    }
    closeDrawer();
  };
  const handleMultiChange = ({ value }) => {
    setValue('categories', value);
    setTag(value);
  };
  const handleTypeChange = ({ value }) => {
    /*if(value[0]?.value!==undefined){
    dispatcher({type:CATEGORIES_FILTER_START,payload:value[0]?.value})
    }*/
    setValue('type', value);
    setType(value);
  };
  const handleTypeChange2 = ({ value }) => {
    /*if(value[0]?.value!==undefined){
    dispatcher({type:CATEGORIES_FILTER_START,payload:value[0]?.value})
    }*/
    setValue('type2', value);
    setType2(value);
  };

  const typeOptions = [
    { value: '43efd3a7-3645-40ca-9a75-436ee6c31ae8', name: 'recibido', id: '1' },
    { value: '4c46ae87-3cf9-4b69-b2a8-b3445b920430', name: 'rechazado', id: '2' },
    { value: '78a23114-6e96-4259-9d21-c8bb5a609c2d', name: 'entregado', id: '3' },
    { value: '8f062347-0f72-4369-b947-bac4497d4ce8', name: 'enviando', id: '4' },
   
  ];
  return (
    <>
      <DrawerTitleWrapper>
        <DrawerTitle>Detalles del pedido</DrawerTitle>
      </DrawerTitleWrapper>

      <Form onSubmit={handleSubmit(onSubmit)} style={{ height: '100%' }}>
        <Scrollbars
          autoHide
          renderView={(props) => (
            <div {...props} style={{ ...props.style, overflowX: 'hidden' }} />
          )}
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              style={{ display: 'none' }}
              className="track-horizontal"
            />
          )}
        >
          <Row>
            <Col lg={4}>
              <FieldDetails>
                Datos del pedido
              </FieldDetails>
            </Col>

            <Col lg={8}>
              <DrawerBox>
                <FormFields>
                  <FormLabel>Nombres</FormLabel>
                  <p>{datass.orderDetail[0]?.payment.user.firstName} {datass.orderDetail[0]?.payment.user.lastName}</p>
                </FormFields>

                <FormFields>
                  <FormLabel>Direccion</FormLabel>
                  <p>{datass.orderDetail[0]?.payment.user.direction}</p>
                </FormFields>

                <FormFields>
                  <FormLabel>Fecha de entrega</FormLabel>
                  <p>{datass.orderDetail[0]?.dateDelivery.substr(0, 10)}</p>
                </FormFields>
                <FormFields>
                  <FormLabel>Hora de entrega</FormLabel>
                  <p>{datass.orderDetail[0]?.hoursDelivery}</p>
                </FormFields>
              </DrawerBox>
            </Col>
          </Row>

          <Row>
            <Col lg={4}>
              <FieldDetails>
               Ingrese los datos para el envio
              </FieldDetails>
            </Col>
            <Col lg={8}>
              <DrawerBox>
                {datass.orderDetail[0]?.detailDeliveries==undefined
                ?
            <FormFields>
                  <FormLabel>Sucursal de envio</FormLabel>
                  <Select
                    options={datass.branch}
                    labelKey="name"
                    valueKey="value"
                    placeholder="etiqueta sub categoria"
                    value={tag}
                    onChange={handleMultiChange}
                    overrides={{
                      Placeholder: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      DropdownListItem: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      Popover: {
                        props: {
                          overrides: {
                            Body: {
                              style: { zIndex: 5 },
                            },
                          },
                        },
                      },
                    }}
                  />
                </FormFields>
            :<FormFields>
            <FormLabel>Sucursal de envio</FormLabel>
            <p>{datass.orderDetail[0]?.detailDeliveries[0]?.branch.name}</p>
            </FormFields>
            }
                <FormFields>
                  <FormLabel>Estado</FormLabel>
                  <Select
                    options={typeOptions}
                    labelKey="name"
                    valueKey="value"
                    placeholder="Tipo de categoria"
                    value={type2}
                    searchable={false}
                    onChange={handleTypeChange2}
                    overrides={{
                      Placeholder: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      DropdownListItem: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      OptionContent: {
                        style: ({ $theme, $selected }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $selected
                              ? $theme.colors.textDark
                              : $theme.colors.textNormal,
                          };
                        },
                      },
                      SingleValue: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      Popover: {
                        props: {
                          overrides: {
                            Body: {
                              style: { zIndex: 5 },
                            },
                          },
                        },
                      },
                    }}
                  />
                </FormFields>

              </DrawerBox>
              </Col>
          </Row>


          
          <Row>
            <Col lg={4}>
              <FieldDetails>
               Datos del envio
              </FieldDetails>
            </Col>
              <Col lg={8}>
              <DrawerBox>
              

              {datass.orderDetail[0]?.orderDetails.map((e,i)=>{
                return(
                  <FormFields>
                  <FormLabel>Producto {i+1}</FormLabel>
                  <p>{e.product.name}</p> <p>{e.product.description}</p>
                  <p><span style={{color:"#092b00"}}>Unidad de producto: </span> {e.product.unit}</p>
                  <h4 style={{color:"#092b00"}}>Costos de envio </h4>
                  <p> <span style={{color:"#092b00"}}>Distancia corta:</span>{e.product.shortDelivery} Bs <span style={{color:"#00C58D"}}>--</span><span style={{color:"#092b00"}}> Distancia larga:</span>{e.product.longDelivery} Bs</p>
                </FormFields>

                )
              })}

                <FormFields>
                  <FormLabel>Total a pagar</FormLabel>
                  <p>{datass.orderDetail[0]?.total} Bs</p>
                </FormFields>
                </DrawerBox>
                </Col>
          </Row>
        </Scrollbars>


        <ButtonGroup>
          <Button
            kind={KIND.minimal}
            onClick={closeDrawer}
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: '50%',
                  borderTopLeftRadius: '3px',
                  borderTopRightRadius: '3px',
                  borderBottomRightRadius: '3px',
                  borderBottomLeftRadius: '3px',
                  marginRight: '15px',
                  color: $theme.colors.red400,
                }),
              },
            }}
          >
            Cancelar
          </Button>

          <Button
            type="submit"
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: '50%',
                  borderTopLeftRadius: '3px',
                  borderTopRightRadius: '3px',
                  borderBottomRightRadius: '3px',
                  borderBottomLeftRadius: '3px',
                }),
              },
            }}
          >
            Actualizar estado
          </Button>
        </ButtonGroup>
      </Form>
    </>
  );
};

export default StaffMemberForm;
