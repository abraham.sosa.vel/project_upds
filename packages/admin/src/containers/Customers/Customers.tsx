import React, { useEffect, useState } from 'react';
import dayjs from 'dayjs';
import { styled, withStyle } from 'baseui';
import { Grid, Row as Rows, Col as Column } from 'components/FlexBox/FlexBox';
import Input from 'components/Input/Input';
import Select from 'components/Select/Select';
import { useQuery, gql } from '@apollo/client';
import { Wrapper, Header, Heading } from 'components/Wrapper.style';

import {
  TableWrapper,
  StyledTable,
  StyledHeadCell,
  StyledBodyCell,
} from './Customers.style';
import NoResult from 'components/NoResult/NoResult';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import { INITIAL_REQUEST_SEARCH_USERS_START, INITIAL_REQUEST_USERS_START } from 'redux/constants';
import { userFilterMax, userFilterMin } from 'redux/actions';

const GET_CUSTOMERS = gql`
  query getCustomers($searchBy: String, $sortBy: String) {
    customers(searchBy: $searchBy, sortBy: $sortBy) {
      id
      image
      name
      contacts {
        id
        type
        number
      }
      total_order
      total_order_amount
      creation_date
    }
  }
`;

const Col = withStyle(Column, () => ({
  '@media only screen and (max-width: 767px)': {
    marginBottom: '20px',

    ':last-child': {
      marginBottom: 0,
    },
  },
}));

const Row = withStyle(Rows, () => ({
  '@media only screen and (min-width: 768px)': {
    alignItems: 'center',
  },
}));

const ImageWrapper = styled('div', ({ $theme }) => ({
  width: '38px',
  height: '38px',
  overflow: 'hidden',
  display: 'inline-block',
  borderTopLeftRadius: '20px',
  borderTopRightRadius: '20px',
  borderBottomRightRadius: '20px',
  borderBottomLeftRadius: '20px',
  backgroundColor: $theme.colors.backgroundF7,
}));

const Image = styled('img', () => ({
  width: '100%',
  height: 'auto',
}));

const sortByOptions = [
  { value: 'mayorCantidad', label: 'Mayor cantidad' },
  { value: 'menorCantidad', label: 'Menor cantidad' },
];
const sortByOptions2 = [
  { value: 'ultimosPedidos', label: 'Pedidos mas recientes' },
  { value: 'pedidosAntiguos', label: 'Pedidos mas antiguos' },
];

export default function Customers() {
  const { data, error, refetch } = useQuery(GET_CUSTOMERS);
  const [stock, setStock] = useState([]);
  const [stock2, setStock2] = useState([]);
  const [search, setSearch] = useState([]);

  let dispatch=useDispatch();
  useEffect(()=>{
    dispatch({type:INITIAL_REQUEST_USERS_START})
  },[])
  const datas = useSelector((state: RootStateOrAny) => state);
  function handleSort({ value }) {
    setStock(value);
    if (value.length) {
      if(value[0].value=="menorCantidad"){
       dispatch(userFilterMin(datas.customers.sort((a, b) => a.Pedidos - b.Pedidos)))
      }else{
        dispatch(userFilterMax((datas.customers.sort((a, b) => a.Pedidos - b.Pedidos).reverse())))
      }
      refetch({
        sortBy: value[0].value,
      });
    } else {
      dispatch({type:INITIAL_REQUEST_USERS_START})
      refetch({
        sortBy: null,
      });
    }
  }
  function handleSort2({ value }) {
    setStock2(value);
    if (value.length) {
      if(value[0].value=="pedidosAntiguos"){
        dispatch(userFilterMin((datas.customers.sort((a, b) => {
          if(a.fecha < b.fecha) return -1;
          if(a.fecha > b.fecha) return 1;
      
          return 0;
      }))))
      }else{
      dispatch(userFilterMax((datas.customers.sort((a, b) => {
        if(a.fecha < b.fecha) return 1;
        if(a.fecha > b.fecha) return -1;
    
        return 0;
    }))))
      }
      refetch({
        sortBy: value[0].value,
      });
    } else {
      dispatch({type:INITIAL_REQUEST_USERS_START})
      refetch({
        sortBy: null,
      });
    }
  }
  function handleSearch(event) {
    const value = event.currentTarget.value;
    setSearch(value);
    if(value.length!=0){
      dispatch({type:INITIAL_REQUEST_SEARCH_USERS_START,payload:value})
    }else{
      dispatch({type:INITIAL_REQUEST_USERS_START})
    }
    refetch({ searchBy: value });
  }


  return (
    <Grid fluid={true}>
      <Row>
        <Col md={12}>
          <Header
            style={{
              marginBottom: 30,
              boxShadow: '0 0 5px rgba(0, 0 ,0, 0.05)',
            }}
          >
            <Col md={2}>
              <Heading>Clientes</Heading>
            </Col>

            <Col md={10}>
              <Row>
                <Col md={6}>
                  <Input
                    value={search}
                    placeholder="Buscar por celular"
                    onChange={handleSearch}
                    clearable
                  />
                </Col>

                <Col md={3}>
                  <Select
                    options={sortByOptions}
                    labelKey="label"
                    valueKey="value"
                    placeholder="Cantidad pedidos"
                    value={stock}
                    searchable={false}
                    onChange={handleSort}
                  />
                </Col>
                <Col md={3}>
                  <Select
                    options={sortByOptions2}
                    labelKey="label"
                    valueKey="value"
                    placeholder="Pedidos"
                    value={stock2}
                    searchable={false}
                    onChange={handleSort2}
                  />
                </Col>
              </Row>
            </Col>
          </Header>

          <Wrapper style={{ boxShadow: '0 0 5px rgba(0, 0 , 0, 0.05)' }}>
            <TableWrapper>
              <StyledTable $gridTemplateColumns="minmax(120px, 120px) minmax(200px, 200px) minmax(100px, 100px) minmax(150px, auto) minmax(300px, max-content)">
                <StyledHeadCell>Celular</StyledHeadCell>
                <StyledHeadCell>Direccion</StyledHeadCell>
                <StyledHeadCell>Pedidos</StyledHeadCell>
                <StyledHeadCell>Fecha ultimo pedido</StyledHeadCell>
                <StyledHeadCell>Nombre y apellido</StyledHeadCell>

                {datas ? (
                  datas.customers.length ? (
                    datas.customers
                      .map((item) => Object.values(item))
                      .map((row, index) => (
                        <React.Fragment key={index}>
                          <StyledBodyCell>{row[0]}</StyledBodyCell>
                          <StyledBodyCell>{row[1]}</StyledBodyCell>
                          <StyledBodyCell>{row[2]}</StyledBodyCell>
                          <StyledBodyCell>{row[3]}</StyledBodyCell>
                          <StyledBodyCell>{row[4]}</StyledBodyCell>
                        </React.Fragment>
                      ))
                  ) : (
                    <NoResult
                      hideButton={false}
                      style={{
                        gridColumnStart: '1',
                        gridColumnEnd: 'one',
                      }}
                    />
                  )
                ) : null}
              </StyledTable>
            </TableWrapper>
          </Wrapper>
        </Col>
      </Row>
    </Grid>
  );
}
