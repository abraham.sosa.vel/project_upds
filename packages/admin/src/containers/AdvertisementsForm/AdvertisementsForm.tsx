import React, { useState, useCallback } from 'react';
import { useForm } from 'react-hook-form';
import { Scrollbars } from 'react-custom-scrollbars';
import { useDrawerDispatch, useDrawerState } from 'context/DrawerContext';
import Uploader from 'components/Uploader/Uploader';
import Button, { KIND } from 'components/Button/Button';
import DrawerBox from 'components/DrawerBox/DrawerBox';
import { Row, Col } from 'components/FlexBox/FlexBox';
import Input from 'components/Input/Input';
import { Textarea } from 'components/Textarea/Textarea';
import Select from 'components/Select/Select';
import { FormFields, FormLabel } from 'components/FormFields/FormFields';

import {
  Form,
  DrawerTitleWrapper,
  DrawerTitle,
  FieldDetails,
  ButtonGroup,
} from '../DrawerItems/DrawerItems.style';
import { useDispatch } from 'react-redux';
import { UPLOAD_ADS_START, UPLOAD_FILES_START } from 'redux/constants';


type Props = any;

const AddProduct: React.FC<Props> = () => {
  const dispatch = useDrawerDispatch();
  const data = useDrawerState('data');
  const dispcher=useDispatch();
  const closeDrawer = useCallback(() => dispatch({ type: 'CLOSE_DRAWER' }), [
    dispatch,
  ]);
  const { register, handleSubmit, setValue } = useForm({
    defaultValues: data,
  });
  const [type, setType] = useState([{ value: data.type }]);
  const [tag, setTag] = useState(false);
  const [image,setImage]=useState({
    image:[],
  });
  React.useEffect(() => {
    register({ name: 'type' });
    register({ name: 'image' });
  }, [register]);


  const handleUploader = (files) => {
    setTag(true);
   setImage({image:files})
   // dispcher({type:UPLOAD_FILES_START,payload:files})
    setValue('image', files[0].path);
  };
  console.log(data)
  const onSubmit = (e) => {
    console.log(tag);
    return new Promise((resolve, reject) => {
      dispcher({type:UPLOAD_ADS_START ,payload: {title:e.title,celphones:e.celphones,lastUpdate:(new Date()).toISOString(),id:data.id,resolve,reject,}});
    })
      .then((err) => {
        if(tag){
          return new Promise((resolve, reject) => {
            dispcher({type:UPLOAD_FILES_START ,payload: {image:image,datos:data.fileStorages[0],resolve,reject,}});
          })
            .then((err) => {
              closeDrawer();
            })
            .catch((err) => {
              console.log("error");
            });
        }else{
          closeDrawer();
        }
      })
      .catch((err) => {
        console.log("error");
      });
  };
  return (
    <>
      <DrawerTitleWrapper>
        <DrawerTitle>Actualizar producto</DrawerTitle>
      </DrawerTitleWrapper>

      <Form
        onSubmit={handleSubmit(onSubmit)}
        style={{ height: '100%' }}
        noValidate
      >
        <Scrollbars
          autoHide
          renderView={(props) => (
            <div {...props} style={{ ...props.style, overflowX: 'hidden' }} />
          )}
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              style={{ display: 'none' }}
              className="track-horizontal"
            />
          )}
        >
          <Row>
            <Col lg={4}>
              <FieldDetails>Sube la imagen de tu producto aqui</FieldDetails>
            </Col>
            <Col lg={8}>
              <DrawerBox>
                <Uploader onChange={handleUploader} imageURL={data.image} />
              </DrawerBox>
            </Col>
          </Row>

          <Row>
            <Col lg={4}>
              <FieldDetails>
              Ingrese la descripcion del producto
              </FieldDetails>
            </Col>

            <Col lg={8}>
              <DrawerBox>
                <FormFields>
                  <FormLabel>Descripcion del anuncio</FormLabel>
                  <Input
                    inputRef={register({ required: true, })}
                    name="title"
                  />
                </FormFields>


                <FormFields>
                  <FormLabel>Numeros de contacto</FormLabel>
                  <Row>
                    <Col md={4} xs={12}>
                      <FormLabel>numero 1</FormLabel>
                      <Input
                        inputRef={register}
                        name="celphones[0]"
                      />
                    </Col>
                    <Col md={4} xs={12}>
                      <FormLabel>numero 2</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="celphones[1]"
                      />
                    </Col>
                    <Col md={4} xs={12}>
                      <FormLabel>numero 3</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="celphones[2]"
                      />
                    </Col>
                  </Row>
                </FormFields>
               
              </DrawerBox>
            </Col>
          </Row>
        </Scrollbars>

        <ButtonGroup>
          <Button
            kind={KIND.minimal}
            onClick={closeDrawer}
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: '50%',
                  borderTopLeftRadius: '3px',
                  borderTopRightRadius: '3px',
                  borderBottomRightRadius: '3px',
                  borderBottomLeftRadius: '3px',
                  marginRight: '15px',
                  color: $theme.colors.red400,
                }),
              },
            }}
          >
            Cancelar
          </Button>

          <Button
            type="submit"
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: '50%',
                  borderTopLeftRadius: '3px',
                  borderTopRightRadius: '3px',
                  borderBottomRightRadius: '3px',
                  borderBottomLeftRadius: '3px',
                }),
              },
            }}
          >
            Actualizar producto
          </Button>
        </ButtonGroup>
      </Form>
    </>
  );
};

export default AddProduct;
