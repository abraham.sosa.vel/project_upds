import React, { useEffect, useState } from "react";
import { styled, withStyle } from "baseui";
import Button from "components/Button/Button";
import { Grid, Row as Rows, Col as Column } from "components/FlexBox/FlexBox";
import Input from "components/Input/Input";
import Select from "components/Select/Select";
import { useQuery, gql } from "@apollo/client";
import { Header, Heading } from "components/Wrapper.style";
import Fade from "react-reveal/Fade";
import ProductCard from "components/ProductCard/ProductCard";
import NoResult from "components/NoResult/NoResult";
import { CURRENCY } from "settings/constants";
import Placeholder from "components/Placeholder/Placeholder";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import { INITIAL_REQUEST_PRODUCT_START, INITIAL_REQUEST_PRODUCT_START2, SEARCH_PRODUCT_START } from "redux/constants";
import { userFilterMaxProducts, userFilterMinProducts } from "redux/actions";

export const ProductsRow = styled("div", ({ $theme }) => ({
  display: "flex",
  flexWrap: "wrap",
  marginTop: "25px",
  backgroundColor: $theme.colors.backgroundF7,
  position: "relative",
  zIndex: "1",

  "@media only screen and (max-width: 767px)": {
    marginLeft: "-7.5px",
    marginRight: "-7.5px",
    marginTop: "15px",
  },
}));

export const Col = withStyle(Column, () => ({
  "@media only screen and (max-width: 767px)": {
    marginBottom: "20px",

    ":last-child": {
      marginBottom: 0,
    },
  },
}));

const Row = withStyle(Rows, () => ({
  "@media only screen and (min-width: 768px) and (max-width: 991px)": {
    alignItems: "center",
  },
}));

export const ProductCardWrapper = styled("div", () => ({
  height: "100%",
}));

export const LoaderWrapper = styled("div", () => ({
  width: "100%",
  height: "100vh",
  display: "flex",
  flexWrap: "wrap",
}));

export const LoaderItem = styled("div", () => ({
  width: "25%",
  padding: "0 15px",
  marginBottom: "30px",
}));

const GET_PRODUCTS = gql`
  query getProducts(
    $type: String
    $sortByPrice: String
    $searchText: String
    $offset: Int
  ) {
    products(
      type: $type
      sortByPrice: $sortByPrice
      searchText: $searchText
      offset: $offset
    ) {
      items {
        id
        name
        description
        image
        type
        price
        unit
        salePrice
        discountInPercent
      }
      totalCount
      hasMore
    }
  }
`;

const typeSelectOptions = [
  { value: "6c441d0f-9662-412d-9bb0-447f74873546", label: "Construccion" },
  { value: "e07367c7-fdac-4347-99ae-75d32b5c9499", label: "Herramientas" },
  { value: "1e11c24e-e829-4a9e-ad46-28a9945f89b1", label: "Mano de obra" },
];
const priceSelectOptions = [
  { value: "mayorPrecio", label: "Mayor precio" },
  { value: "menorPrecio", label: "Menor precio" },
];

export default function Products() {
  const { data, error, refetch, fetchMore } = useQuery(GET_PRODUCTS);
  const [loadingMore, toggleLoading] = useState(false);
  const [type, setType] = useState([]);
  const [priceOrder, setPriceOrder] = useState([]);
  const [search, setSearch] = useState([]);
  const dispatch=useDispatch();
  /*if (error) {
    return <div>Error! {error.message}</div>;
  }*/
  useEffect(()=>{
    dispatch({type:INITIAL_REQUEST_PRODUCT_START})
  },[])
  function loadMore() {
    toggleLoading(true);
    fetchMore({
      variables: {
        offset: data.products.items.length,
      },
      updateQuery: (prev, { fetchMoreResult }) => {
        toggleLoading(false);
        if (!fetchMoreResult) return prev;
        return Object.assign({}, prev, {
          products: {
            __typename: prev.products.__typename,
            items: [...prev.products.items, ...fetchMoreResult.products.items],
            hasMore: fetchMoreResult.products.hasMore,
          },
        });
      },
    });
  }
  const datas = useSelector((state: RootStateOrAny) => state);
  function handlePriceSort({ value }) {
    setPriceOrder(value);
    if (value.length) {
      if(value[0].value=="menorPrecio"){
        let cat={
          items:[],
          hasMore:{}
        }
        cat.items=datas.products.items.sort((a, b) => a.price - b.price)
        dispatch(userFilterMinProducts(cat))
       }else{
        let cat={
          items:[],
          hasMore:{}
        }
        cat.items=(datas.products.items.sort((a, b) => a.price - b.price).reverse())
         dispatch(userFilterMaxProducts(cat))
       }
      refetch({
        sortByPrice: value[0].value,
      });
    } else {
      dispatch({type:INITIAL_REQUEST_PRODUCT_START})
      refetch({
        sortByPrice: null,
      });
    }
  }
  function handleCategoryType({ value }) {
    setType(value);
    if (value.length) {
    dispatch({type:INITIAL_REQUEST_PRODUCT_START2,payload:value[0].value})
      refetch({
        type: value[0].value,
      });
    } else {
      dispatch({type:INITIAL_REQUEST_PRODUCT_START})
      refetch({
        type: null,
      });
    }
  }
  function handleSearch(event) {
    const value = event.currentTarget.value;
    setSearch(value);
    if(value.length!=0){
      dispatch({type:SEARCH_PRODUCT_START,payload:value})
    }else{
      dispatch({type:INITIAL_REQUEST_PRODUCT_START})
    }
    refetch({ searchText: value });
  }
  return (
    <Grid fluid={true}>
      <Row>
        <Col md={12}>
          <Header style={{ marginBottom: 15 }}>
            <Col md={2} xs={12}>
              <Heading>Productos</Heading>
            </Col>

            <Col md={10} xs={12}>
              <Row>
                <Col md={3} xs={12}>
                  <Select
                    options={typeSelectOptions}
                    labelKey="label"
                    valueKey="value"
                    placeholder="Categoria"
                    value={type}
                    searchable={false}
                    onChange={handleCategoryType}
                  />
                </Col>

                <Col md={3} xs={12}>
                  <Select
                    options={priceSelectOptions}
                    labelKey="label"
                    valueKey="value"
                    value={priceOrder}
                    placeholder="Precio"
                    searchable={false}
                    onChange={handlePriceSort}
                  />
                </Col>

                <Col md={6} xs={12}>
                  <Input
                    value={search}
                    placeholder="Buscar un producto"
                    onChange={handleSearch}
                    clearable
                  />
                </Col>
              </Row>
            </Col>
          </Header>

          <Row>
            {datas ? (
              datas.products && datas.products.items? (
                datas.products?.items.map((item: any, index: number) => (
                  <Col
                    md={4}
                    lg={3}
                    sm={6}
                    xs={12}
                    key={index}
                    style={{ margin: "15px 0" }}
                  >
                    <Fade bottom duration={800} delay={index * 10}>
                      <ProductCard
                        title={item.name}
                        weight={item.unit}
                        image={item.image}
                        currency={CURRENCY}
                        price={item.price}
                        salePrice={item.salePrice}
                        discountInPercent={item.discountInPercent}
                        data={item}
                      />
                    </Fade>
                  </Col>
                ))
              ) : (
                <NoResult />
              )
            ) : (
              <LoaderWrapper>
                <LoaderItem>
                  <Placeholder />
                </LoaderItem>
                <LoaderItem>
                  <Placeholder />
                </LoaderItem>
                <LoaderItem>
                  <Placeholder />
                </LoaderItem>
                <LoaderItem>
                  <Placeholder />
                </LoaderItem>
              </LoaderWrapper>
            )}
          </Row>

        </Col>
      </Row>
    </Grid>
  );
}
