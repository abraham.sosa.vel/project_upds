import React from "react";
import {
  Grid,
  Row as Rows,
  Col as Column,
  Row,
  Col,
} from "components/FlexBox/FlexBox";
import { Wrapper, Header, Heading } from "components/Wrapper.style";
import {
  Form,
  DrawerTitleWrapper,
  DrawerTitle,
  FieldDetails,
  ButtonGroup,
} from "../DrawerItems/DrawerItems.style";
import DrawerBox from "components/DrawerBox/DrawerBox";
import { FormFields, FormLabel } from "components/FormFields/FormFields";
import Button, { KIND } from "components/Button/Button";
import Input from "components/Input/Input";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { CHANGE_PASSWORD } from "redux/constants";
export default function Change() {
  const { register, handleSubmit, setValue } = useForm();
  const dispatch=useDispatch();
  React.useEffect(() => {
    register({ name: "category" });
  }, [register]);
  
  
  const onSubmit = (data) => {
   dispatch({type:CHANGE_PASSWORD,payload:{confirmPassword:data.confirmPassword,newPassword:data.newPassword,oldPassword:data.oldPassword,userId:"eae697de-144e-4216-8f13-40d0a566a573"}})
  };
  return (
    <Grid fluid={true}>
      <Row>
        <Col md={12} >
          <Header
            style={{
              marginBottom: 30,
              boxShadow: "0 0 5px rgba(0, 0 ,0, 0.05)",
            }}
          >
            <Col md={12}>
              <Heading>Cambiar contraseña</Heading>
            </Col>
            <Col md={8}>
              <Form onSubmit={handleSubmit(onSubmit)} style={{ height: "100%",backgroundColor:"white" }} noValidate>
              <DrawerBox>
                  <FormFields>
                    <FormLabel>Contraseña antigua</FormLabel>
                    <Input
                     inputRef={register({ required: true })}
                      name="oldPassword" />
                  </FormFields>
                </DrawerBox>
                <DrawerBox>
                  <FormFields>
                    <FormLabel>Contraseña nueva</FormLabel>
                    <Input
                    inputRef={register({ required: true })}
                    name="newPassword" />
                  </FormFields>
                </DrawerBox>
                <DrawerBox>
                  <FormFields>
                    <FormLabel>Repita la nueva contraseña</FormLabel>
                    <Input 
                     inputRef={register({ required: true })}
                    name="confirmPassword" />
                  </FormFields>
                </DrawerBox>
                <Button
                  type="submit"
                  overrides={{
                    BaseButton: {
                      style: ({ $theme }) => ({
                        width: "50%",
                        borderTopLeftRadius: "3px",
                        borderTopRightRadius: "3px",
                        borderBottomRightRadius: "3px",
                        borderBottomLeftRadius: "3px",
                      }),
                    },
                  }}
                >
                  Guardar
                </Button>
              </Form>
            </Col>
          </Header>
        </Col>
      </Row>
    </Grid>
  );
}
