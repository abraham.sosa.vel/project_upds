import React, { useState, useCallback, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { Scrollbars } from 'react-custom-scrollbars';
import { useDrawerDispatch, useDrawerState } from 'context/DrawerContext';
import Uploader from 'components/Uploader/Uploader';
import Button, { KIND } from 'components/Button/Button';
import DrawerBox from 'components/DrawerBox/DrawerBox';
import { Row, Col } from 'components/FlexBox/FlexBox';
import Input from 'components/Input/Input';
import { Textarea } from 'components/Textarea/Textarea';
import Select from 'components/Select/Select';
import { FormFields, FormLabel } from 'components/FormFields/FormFields';

import {
  Form,
  DrawerTitleWrapper,
  DrawerTitle,
  FieldDetails,
  ButtonGroup,
} from '../DrawerItems/DrawerItems.style';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import { CATEGORIES_FILTER_START, CHANGE_IMAGE_PRODUCT_START, CHANGE_PRICES_PRODUCT_START, CHANGE_PRODUCT_START } from 'redux/constants';


const typeOptions = [
  { value: '1e11c24e-e829-4a9e-ad46-28a9945f89b1', name: 'Mano de Obra', id: '1' },
  { value: '6c441d0f-9662-412d-9bb0-447f74873546', name: 'Material de Construcción', id: '2' },
  { value: 'e07367c7-fdac-4347-99ae-75d32b5c9499', name: 'Herramientas', id: '3' },
];

type Props = any;

const AddProduct: React.FC<Props> = () => {
  const dispatch = useDrawerDispatch();
  const data = useDrawerState('data');
  const closeDrawer = useCallback(() => dispatch({ type: 'CLOSE_DRAWER' }), [
    dispatch,
  ]);
  const { register, handleSubmit, setValue } = useForm({
    defaultValues: data,
  });
  const [type, setType] = useState([{ value: data.type }]);
  const [tag, setTag] = useState([]);
  const [tag2, setTag2] = useState(false);
  const [description, setDescription] = useState(data.description);
  const dispatcher=useDispatch();
  const [image,setImage]=useState({
    image:[],
  });
  const {options} = useSelector((state: RootStateOrAny) => state);
  const dispcher=useDispatch();
  React.useEffect(() => {
    register({ name: 'type' });
    register({ name: 'categories' });
    register({ name: 'image' });
    register({ name: 'description' });
  }, [register]);

  const handleMultiChange = ({ value }) => {
    setValue('categories', value);
    setTag(value);
  };
  const handleDescriptionChange = (e) => {
    const value = e.target.value;
    setValue('description', value);
    setDescription(value);
  };

  const handleTypeChange = ({ value }) => {
    if(value[0]?.value!==undefined){
    dispatcher({type:CATEGORIES_FILTER_START,payload:value[0]?.value})
    }
    setValue('type', value);
    setType(value);
  };
  const handleUploader = (files) => {
    setTag2(true);
    setImage({image:files})
    setValue('image', files[0].path);
  };

  console.log(data)
  const onSubmit = (e) => {
    console.log(e);
    return new Promise((resolve, reject) => {
      dispcher({type:CHANGE_PRODUCT_START ,payload: {
        id: data.id,
        name: e.name,
        description: e.description,
        amountMin: parseInt(e.amountMin),
        amountUnit: 1,
        shortDelivery: parseInt(e.shortDelivery),
        longDelivery: parseInt(e.longDelivery),
        unit: e.unit,
        categoryId: e.categories[0].id,
        resolve,reject,}});
    })
      .then((err) => {
        dispcher({type:CHANGE_PRICES_PRODUCT_START,payload:{
          id: data.prices[0].id,
          amountMin: parseInt(e.prices[0].amountMin),
          amountMax: parseInt(e.prices[0].amountMax),
          price: parseInt(e.prices[0].price),
          productId: data.id
        }
        })
        dispcher({type:CHANGE_PRICES_PRODUCT_START,payload:{
          id: data.prices[1].id,
          amountMin: parseInt(e.prices[1].amountMin),
          amountMax: parseInt(e.prices[1].amountMax),
          price: parseInt(e.prices[1].price),
          productId: data.id
        }
        })
        dispcher({type:CHANGE_PRICES_PRODUCT_START,payload:{
          id: data.prices[2].id,
          amountMin: parseInt(e.prices[2].amountMin),
          amountMax: parseInt(e.prices[2].amountMax),
          price: parseInt(e.prices[2].price),
          productId: data.id
        }
        })
        if(tag2){
          return new Promise((resolve, reject) => {
            dispcher({type:CHANGE_IMAGE_PRODUCT_START ,payload: {image:image,datos:data.fileStorages[0],resolve,reject,}});
          })
            .then((err) => {
              closeDrawer();
            })
            .catch((err) => {
              console.log("error");
            });
        }else{
          closeDrawer();
        }
       console.log(err)
      })
      .catch((err) => {
        console.log("error");
      });
  };



  return (
    <>
      <DrawerTitleWrapper>
        <DrawerTitle>Actualizar producto</DrawerTitle>
      </DrawerTitleWrapper>

      <Form
        onSubmit={handleSubmit(onSubmit)}
        style={{ height: '100%' }}
        noValidate
      >
        <Scrollbars
          autoHide
          renderView={(props) => (
            <div {...props} style={{ ...props.style, overflowX: 'hidden' }} />
          )}
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              style={{ display: 'none' }}
              className="track-horizontal"
            />
          )}
        >
          <Row>
            <Col lg={4}>
              <FieldDetails>Sube la imagen de tu producto aqui</FieldDetails>
            </Col>
            <Col lg={8}>
              <DrawerBox>
                <Uploader onChange={handleUploader} imageURL={data.image} />
              </DrawerBox>
            </Col>
          </Row>

          <Row>
            <Col lg={4}>
              <FieldDetails>
              Ingrese la descripcion del producto
              </FieldDetails>
            </Col>

            <Col lg={8}>
              <DrawerBox>
                <FormFields>
                  <FormLabel>Nombre</FormLabel>
                  <Input
                    inputRef={register({ required: true })}
                    name="name"
                  />
                </FormFields>

                <FormFields>
                  <FormLabel>Descripcion</FormLabel>
                  <Textarea
                    value={description}
                    onChange={handleDescriptionChange}
                  />
                </FormFields>

                <FormFields>
                  <FormLabel>Unidad</FormLabel>
                  <Input type="text" inputRef={register} name="unit" />
                </FormFields>

                <FormFields>
                  <FormLabel>Categoria</FormLabel>
                  <Select
                    options={typeOptions}
                    labelKey="name"
                    valueKey="value"
                    placeholder="Tipo de categoria"
                    value={type}
                    searchable={false}
                    onChange={handleTypeChange}
                    overrides={{
                      Placeholder: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      DropdownListItem: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      OptionContent: {
                        style: ({ $theme, $selected }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $selected
                              ? $theme.colors.textDark
                              : $theme.colors.textNormal,
                          };
                        },
                      },
                      SingleValue: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      Popover: {
                        props: {
                          overrides: {
                            Body: {
                              style: { zIndex: 5 },
                            },
                          },
                        },
                      },
                    }}
                  />
                </FormFields>

                <FormFields>
                  <FormLabel>Sub Categoria</FormLabel>
                  <Select
                    options={options}
                    labelKey="name"
                    valueKey="value"
                    placeholder="etiqueta sub categoria"
                    value={tag}
                    onChange={handleMultiChange}
                    overrides={{
                      Placeholder: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      DropdownListItem: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      Popover: {
                        props: {
                          overrides: {
                            Body: {
                              style: { zIndex: 5 },
                            },
                          },
                        },
                      },
                    }}
                  />
                </FormFields>
                <FormLabel>
                  En este apartado podra realizar diferentes rangos de precios
                  opcionales para dar rebajas por la cantidas de compras del
                  producto
                </FormLabel>
                <FormFields>
                  <FormLabel>Precios</FormLabel>
                  <Row>
                    <Col md={4} xs={12}>
                      <FormLabel>minimo</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="prices[0].amountMin"
                      />
                    </Col>
                    <Col md={4} xs={12}>
                      <FormLabel>maximo</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="prices[0].amountMax"
                      />
                    </Col>
                    <Col md={4} xs={12}>
                      <FormLabel>Precio x unidad</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="prices[0].price"
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col md={4} xs={12}>
                      <FormLabel>minimo</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="prices[1].amountMin"
                      />
                    </Col>
                    <Col md={4} xs={12}>
                      <FormLabel>maximo</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="prices[1].amountMax"
                      />
                    </Col>
                    <Col md={4} xs={12}>
                      <FormLabel>Precio x unidad</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="prices[1].price"
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col md={4} xs={12}>
                      <FormLabel>minimo</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="prices[2].amountMin"
                      />
                    </Col>
                    <Col md={4} xs={12}>
                      <FormLabel>maximo</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="prices[2].amountMax"
                      />
                    </Col>
                    <Col md={4} xs={12}>
                      <FormLabel>Precio x unidad</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="prices[2].price"
                      />
                    </Col>
                  </Row>
                </FormFields>
                <FormLabel>
                En este apartado podra elegir a partir de cuantos pedidos se realizara el envio y el costo de estos
                </FormLabel>
                <FormFields>
                  <FormLabel>Pedido minimo puesto en obra</FormLabel>
                  <Input type="number" inputRef={register} name="amountMin" />
                </FormFields>
                <FormLabel>
                  Costo de envio
                </FormLabel>
                <FormFields>
                  <FormLabel>Distancia corta</FormLabel>
                  <Input
                    type="number"
                    inputRef={register}
                    name="shortDelivery"
                  />
                </FormFields>

                <FormFields>
                  <FormLabel>Distancia larga</FormLabel>
                  <Input
                    type="number"
                    inputRef={register({ required: true })}
                    name="longDelivery"
                  />
                </FormFields>
              </DrawerBox>
            </Col>
          </Row>
        </Scrollbars>

        <ButtonGroup>
          <Button
            kind={KIND.minimal}
            onClick={closeDrawer}
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: '50%',
                  borderTopLeftRadius: '3px',
                  borderTopRightRadius: '3px',
                  borderBottomRightRadius: '3px',
                  borderBottomLeftRadius: '3px',
                  marginRight: '15px',
                  color: $theme.colors.red400,
                }),
              },
            }}
          >
            Cancelar
          </Button>

          <Button
            type="submit"
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: '50%',
                  borderTopLeftRadius: '3px',
                  borderTopRightRadius: '3px',
                  borderBottomRightRadius: '3px',
                  borderBottomLeftRadius: '3px',
                }),
              },
            }}
          >
            Actualizar producto
          </Button>
        </ButtonGroup>
      </Form>
    </>
  );
};

export default AddProduct;
