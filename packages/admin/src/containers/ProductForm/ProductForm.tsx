import React, { useState, useCallback } from "react";
import { useForm } from "react-hook-form";
import { v4 as uuidv4 } from "uuid";
import { useMutation, gql } from "@apollo/client";
import { Scrollbars } from "react-custom-scrollbars";
import { useDrawerDispatch } from "context/DrawerContext";
import Uploader from "components/Uploader/Uploader";
import Button, { KIND } from "components/Button/Button";
import DrawerBox from "components/DrawerBox/DrawerBox";
import { Row, Col } from "components/FlexBox/FlexBox";
import Input from "components/Input/Input";
import { Textarea } from "components/Textarea/Textarea";
import Select from "components/Select/Select";
import { FormFields, FormLabel } from "components/FormFields/FormFields";

import {
  Form,
  DrawerTitleWrapper,
  DrawerTitle,
  FieldDetails,
  ButtonGroup,
} from "../DrawerItems/DrawerItems.style";
import { CATEGORIES_FILTER_START,CREATE_IMAGE_PRODUCT_START,CREATE_PRICES_PRODUCT_START,CREATE_PRODUCT_START } from "redux/constants";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";



const typeOptions = [
  { value: '1e11c24e-e829-4a9e-ad46-28a9945f89b1', name: 'Mano de Obra', id: '1' },
  { value: '6c441d0f-9662-412d-9bb0-447f74873546', name: 'Material de Construcción', id: '2' },
  { value: 'e07367c7-fdac-4347-99ae-75d32b5c9499', name: 'Herramientas', id: '3' },
];

const GET_PRODUCTS = gql`
  query getProducts(
    $type: String
    $sortByPrice: String
    $searchText: String
    $offset: Int
  ) {
    products(
      type: $type
      sortByPrice: $sortByPrice
      searchText: $searchText
      offset: $offset
    ) {
      items {
        id
        name
        image
        type
        price
        unit
        salePrice
        discountInPercent
      }
      totalCount
      hasMore
    }
  }
`;
const CREATE_PRODUCT = gql`
  mutation createProduct($product: AddProductInput!) {
    createProduct(product: $product) {
      id
      name
      image
      slug
      type
      price
      unit
      description
      salePrice
      discountInPercent
      # per_unit
      quantity
      # creation_date
    }
  }
`;
type Props = any;

const AddProduct: React.FC<Props> = (props) => {
  const dispatch = useDrawerDispatch();
  const closeDrawer = useCallback(() => dispatch({ type: "CLOSE_DRAWER" }), [
    dispatch,
  ]);
  const { register, handleSubmit, setValue } = useForm();
  const [type, setType] = useState([]);
  const [tag, setTag] = useState([]);
  const [description, setDescription] = useState("");
  const dispatcher=useDispatch();
  const {options} = useSelector((state: RootStateOrAny) => state);
  const [image,setImage]=useState({
    image:[],
  });
  React.useEffect(() => {
    register({ name: "type" });
    register({ name: "categories" });
    register({ name: "image", required: true });
    register({ name: "description" });
  }, [register]);

  const handleDescriptionChange = (e) => {
    const value = e.target.value;
    setValue("description", value);
    setDescription(value);
  };

  const [createProduct] = useMutation(CREATE_PRODUCT, {
    update(cache, { data: { createProduct } }) {
      const { products } = cache.readQuery({
        query: GET_PRODUCTS,
      });

      cache.writeQuery({
        query: GET_PRODUCTS,
        data: {
          products: {
            __typename: products.__typename,
            items: [createProduct, ...products.items],
            hasMore: true,
            totalCount: products.items.length + 1,
          },
        },
      });
    },
  });
  const handleMultiChange = ({ value }) => {
    setValue("categories", value);
    setTag(value);
  };

  const handleTypeChange = ({ value }) => {
    if(value[0]?.value!==undefined){
    dispatcher({type:CATEGORIES_FILTER_START,payload:value[0]?.value})
    }
    setValue('type', value);
    setType(value);
  };
  const handleUploader = (files) => {
    setImage({image:files})
    setValue("image", files[0].path);
  };
  const onSubmit = (e) => {
   console.log(e)

   return new Promise((resolve, reject) => {
    dispatcher({type: CREATE_PRODUCT_START ,payload: {
        name:e.name,
        description: e.description,
        amountMin: parseInt(e.minimumOrder),
        amountUnit: 1,
        shortDelivery: parseInt(e.shortDistance),
        longDelivery: parseInt(e.longDistance),
        unit: e.unit,
        categoryId: e.categories[0].id,
      resolve,reject,}});
  })
  .then((err) => {
    dispatcher({type:CREATE_PRICES_PRODUCT_START,payload:{
      amountMin: parseInt(e.min1==""?e.min1="0":e.min1),
      amountMax: parseInt(e.max1==""?e.max1="0":e.max1),
      price: parseInt(e.priceUnity1==""?e.priceUnity1="0":e.priceUnity1),
    }
    })
    dispatcher({type:CREATE_PRICES_PRODUCT_START,payload:{
      amountMin: parseInt(e.min2==""?e.min2="0":e.min2),
      amountMax: parseInt(e.max2==""?e.max2="0":e.max2),
      price: parseInt(e.priceUnity2==""?e.priceUnity2="0":e.priceUnity2),
    }
    })
    dispatcher({type:CREATE_PRICES_PRODUCT_START,payload:{
      amountMin: parseInt(e.min3==""?e.min3="0":e.min3),
      amountMax: parseInt(e.max3==""?e.max3="0":e.max3),
      price: parseInt(e.priceUnity3==""?e.priceUnity3="0":e.priceUnity3),
    }
    })
      return new Promise((resolve, reject) => {
        dispatcher({type:CREATE_IMAGE_PRODUCT_START ,payload: {image:image,resolve,reject,}});
      })
        .then((err) => {
          closeDrawer();
        })
        .catch((err) => {
          console.log("error");
        });
  })
  .catch((err) => {
    console.log("error");
  });
    //closeDrawer();
  };

  return (
    <>
      <DrawerTitleWrapper>
        <DrawerTitle>Añadir producto</DrawerTitle>
      </DrawerTitleWrapper>

      <Form onSubmit={handleSubmit(onSubmit)} style={{ height: "100%" }}>
        <Scrollbars
          autoHide
          renderView={(props) => (
            <div {...props} style={{ ...props.style, overflowX: "hidden" }} />
          )}
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              style={{ display: "none" }}
              className="track-horizontal"
            />
          )}
        >
          <Row>
            <Col lg={4}>
              <FieldDetails>Sube la imagen de tu producto aqui</FieldDetails>
            </Col>
            <Col lg={8}>
              <DrawerBox
                overrides={{
                  Block: {
                    style: {
                      width: "100%",
                      height: "auto",
                      padding: "30px",
                      borderRadius: "3px",
                      backgroundColor: "#ffffff",
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    },
                  },
                }}
              >
                <Uploader onChange={handleUploader} />
              </DrawerBox>
            </Col>
          </Row>

          <Row>
            <Col lg={4}>
              <FieldDetails>Ingrese la descripcion del producto</FieldDetails>
            </Col>

            <Col lg={8}>
              <DrawerBox>
                <FormFields>
                  <FormLabel>Nombre</FormLabel>
                  <Input
                    inputRef={register({ required: true, maxLength: 20 })}
                    name="name"
                  />
                </FormFields>

                <FormFields>
                  <FormLabel>Descripcion</FormLabel>
                  <Textarea
                    value={description}
                    onChange={handleDescriptionChange}
                  />
                </FormFields>

                <FormFields>
                  <FormLabel>Unidad</FormLabel>
                  <Input type="text" inputRef={register} name="unit" />
                </FormFields>
                <FormFields>
                  <FormLabel>Categoria</FormLabel>
                  <Select
                    options={typeOptions}
                    labelKey="name"
                    valueKey="value"
                    placeholder="Tipo de categoria"
                    value={type}
                    searchable={false}
                    onChange={handleTypeChange}
                    overrides={{
                      Placeholder: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      DropdownListItem: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      OptionContent: {
                        style: ({ $theme, $selected }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $selected
                              ? $theme.colors.textDark
                              : $theme.colors.textNormal,
                          };
                        },
                      },
                      SingleValue: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      Popover: {
                        props: {
                          overrides: {
                            Body: {
                              style: { zIndex: 5 },
                            },
                          },
                        },
                      },
                    }}
                  />
                </FormFields>

                <FormFields>
                  <FormLabel>Sub Categoria</FormLabel>
                  <Select
                    options={options}
                    labelKey="name"
                    valueKey="value"
                    placeholder="etiqueta sub categoria"
                    value={tag}
                    onChange={handleMultiChange}
                    overrides={{
                      Placeholder: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      DropdownListItem: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      Popover: {
                        props: {
                          overrides: {
                            Body: {
                              style: { zIndex: 5 },
                            },
                          },
                        },
                      },
                    }}
                  />
                </FormFields>
                <FormLabel>
                  En este apartado podra realizar diferentes rangos de precios
                  opcionales para dar rebajas por la cantidas de compras del
                  producto
                </FormLabel>
                <FormFields>
                  <FormLabel>Precios</FormLabel>
                  <Row>
                    <Col md={4} xs={12}>
                      <FormLabel>minimo</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="min1"
                      />
                    </Col>
                    <Col md={4} xs={12}>
                      <FormLabel>maximo</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="max1"
                      />
                    </Col>
                    <Col md={4} xs={12}>
                      <FormLabel>Precio x unidad</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="priceUnity1"
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col md={4} xs={12}>
                      <FormLabel>minimo</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="min2"
                      />
                    </Col>
                    <Col md={4} xs={12}>
                      <FormLabel>maximo</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="max2"
                      />
                    </Col>
                    <Col md={4} xs={12}>
                      <FormLabel>Precio x unidad</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="priceUnity2"
                      />
                    </Col>
                  </Row>
                  <Row>
                    <Col md={4} xs={12}>
                      <FormLabel>minimo</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="min3"
                      />
                    </Col>
                    <Col md={4} xs={12}>
                      <FormLabel>maximo</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="max3"
                      />
                    </Col>
                    <Col md={4} xs={12}>
                      <FormLabel>Precio x unidad</FormLabel>
                      <Input
                        type="number"
                        inputRef={register}
                        name="priceUnity3"
                      />
                    </Col>
                  </Row>
                </FormFields>
                <FormLabel>
                En este apartado podra elegir a partir de cuantos pedidos se realizara el envio y el costo de estos
                </FormLabel>
                <FormFields>
                  <FormLabel>Pedido minimo puesto en obra</FormLabel>
                  <Input      type="number" inputRef={register} name="minimumOrder" />
                </FormFields>
                <FormLabel>
                  Costo de envio
                </FormLabel>
                <FormFields>
                  <FormLabel>Distancia corta</FormLabel>
                  <Input
                    type="number"
                    inputRef={register}
                    name="shortDistance"
                  />
                </FormFields>

                <FormFields>
                  <FormLabel>Distancia larga</FormLabel>
                  <Input
                    type="number"
                    inputRef={register({ required: true })}
                    name="longDistance"
                  />
                </FormFields>
              </DrawerBox>
            </Col>
          </Row>
        </Scrollbars>

        <ButtonGroup>
          <Button
            kind={KIND.minimal}
            onClick={closeDrawer}
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: "50%",
                  borderTopLeftRadius: "3px",
                  borderTopRightRadius: "3px",
                  borderBottomRightRadius: "3px",
                  borderBottomLeftRadius: "3px",
                  marginRight: "15px",
                  color: $theme.colors.red400,
                }),
              },
            }}
          >
            Cancelar
          </Button>

          <Button
            type="submit"
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: "50%",
                  borderTopLeftRadius: "3px",
                  borderTopRightRadius: "3px",
                  borderBottomRightRadius: "3px",
                  borderBottomLeftRadius: "3px",
                }),
              },
            }}
          >
            Crear producto
          </Button>
        </ButtonGroup>
      </Form>
    </>
  );
};

export default AddProduct;
