import React, { useState, useCallback } from "react";
import { useForm } from "react-hook-form";
import { v4 as uuidv4 } from "uuid";
import { useMutation, gql } from "@apollo/client";
import { useDrawerDispatch } from "context/DrawerContext";
import { Scrollbars } from "react-custom-scrollbars";
import Input from "components/Input/Input";
import Select from "components/Select/Select";
import Button, { KIND } from "components/Button/Button";
import DrawerBox from "components/DrawerBox/DrawerBox";
import { Row, Col } from "components/FlexBox/FlexBox";
import * as icons from "assets/icons/category-icons";
import {
  Form,
  DrawerTitleWrapper,
  DrawerTitle,
  FieldDetails,
  ButtonGroup,
} from "../DrawerItems/DrawerItems.style";
import { FormFields, FormLabel } from "components/FormFields/FormFields";
import { Icon } from "containers/Category/Category.style";
import { ImageWrapper } from "components/NoResult/NoResult.style";
import { useDispatch } from "react-redux";
import { INITIAL_REQUEST_CATEGORIES_CREATE_START } from "redux/constants";

const GET_CATEGORIES = gql`
  query getCategories($type: String, $searchBy: String) {
    categories(type: $type, searchBy: $searchBy) {
      id
      icon
      name
      slug
      type
    }
  }
`;
const CREATE_CATEGORY = gql`
  mutation createCategory($category: AddCategoryInput!) {
    createCategory(category: $category) {
      id
      name
      type
      icon
      # creation_date
      slug
      # number_of_product
    }
  }
`;

const options = [
  {
    value: "6c441d0f-9662-412d-9bb0-447f74873546",
    name: "Construccion",
    id: "1",
  },
  {
    value: "e07367c7-fdac-4347-99ae-75d32b5c9499",
    name: "Herramientas",
    id: "2",
  },
  {
    value: "1e11c24e-e829-4a9e-ad46-28a9945f89b1",
    name: "Mano de obra",
    id: "3",
  },
];
type Props = any;

const AddCategory: React.FC<Props> = (props) => {
  const dispatch = useDrawerDispatch();
  const closeDrawer = useCallback(() => dispatch({ type: "CLOSE_DRAWER" }), [
    dispatch,
  ]);
  const { register, handleSubmit, setValue } = useForm();
  const [category, setCategory] = useState([]);
  const Dipatch = useDispatch();
  React.useEffect(() => {
    register({ name: "parent" });
    register({ name: "image" });
  }, [register]);
  const [createCategory] = useMutation(CREATE_CATEGORY, {
    update(cache, { data: { createCategory } }) {
      const { categories } = cache.readQuery({
        query: GET_CATEGORIES,
      });

      cache.writeQuery({
        query: GET_CATEGORIES,
        data: { categories: categories.concat([createCategory]) },
      });
    },
  });
  const onSubmit = ({ name, parent }) => {
    return new Promise((resolve, reject) => {
      Dipatch({ 
        type: INITIAL_REQUEST_CATEGORIES_CREATE_START,
        payload: {
          title: name,
          slug: name.replace(" ", "_"),
          icon: icon.value,
          children:[],
          typeCategoryId: parent[0].value,
          resolve,
          reject
        },
      });
    })
      .then((err) => {
        closeDrawer();
      })
      .catch((err) => {
        console.log("error");
      });
  };
  const handleChange = ({ value }) => {
    setValue("parent", value);
    setCategory(value);
  };
  let data = [
    "Whatsapp",
    "People",
    "Archive",
    "Bitbucket",
    "Brick",
    "Briefcase",
  ];
  let data2=[
    "Brush",
    "Drilling",
    "Dumpster",
    "Enginner",
    "HardHat",
    "PaintRoller",
  ]
  let data3=[
    "RulerCombined",
    "RulerHorizontal",
    "RulerVertical",
    "Screwdriver",
    "Shovel",
    "StepLadder",
  ]
  let data4=[
    "Toolbox",
    "ToolSolid",
    "TruckPicku",
    "Wrench",
  ]
  const Icon = ({ name }) => {
    const TagName = icons[name];
    return !!TagName ? <TagName /> : <p>Invalid icon {name}</p>;
  };
  const [icon, setIcon] = useState({
    value: "",
  });
  let selectIcon = (e) => {
    setIcon({
      value: e,
    });
  };
  return (
    <>
      <DrawerTitleWrapper>
        <DrawerTitle>Añadir categoria</DrawerTitle>
      </DrawerTitleWrapper>

      <Form onSubmit={handleSubmit(onSubmit)} style={{ height: "100%" }}>
        <Scrollbars
          autoHide
          renderView={(props) => (
            <div {...props} style={{ ...props.style, overflowX: "hidden" }} />
          )}
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              style={{ display: "none" }}
              className="track-horizontal"
            />
          )}
        >
          <Row>
            <Col lg={4}>
              <FieldDetails>
                Ingrese los datos de la nueva categoria
              </FieldDetails>
            </Col>

            <Col lg={8}>
              <DrawerBox>
                <FormFields>
                  <FormLabel>Nombre</FormLabel>
                  <Input
                    inputRef={register({ required: true, maxLength: 20 })}
                    name="name"
                  />
                </FormFields>
                <FormFields>
                  <FormLabel>Tipo</FormLabel>
                  <Select
                    options={options}
                    labelKey="name"
                    valueKey="value"
                    placeholder="Ingrese el tipo"
                    value={category}
                    searchable={false}
                    onChange={handleChange}
                    overrides={{
                      Placeholder: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      DropdownListItem: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      OptionContent: {
                        style: ({ $theme, $selected }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $selected
                              ? $theme.colors.textDark
                              : $theme.colors.textNormal,
                          };
                        },
                      },
                      SingleValue: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      Popover: {
                        props: {
                          overrides: {
                            Body: {
                              style: { zIndex: 5 },
                            },
                          },
                        },
                      },
                    }}
                  />
                </FormFields>
                <ImageWrapper>
                  {icon.value == "" ? (
                    <div style={{ color: "#00C58D" }}>Seleccione un icono</div>
                  ) : (
                   <div>
                      <Icon name={icon.value} />
                   </div>
                  )}
                </ImageWrapper>

               <div style={{ display: "flex", flexDirection: "row" }}>
                  {data.map((e) => (
                    <div
                      style={{
                        margin: "1rem",
                        display: "inline !important",
                        width: "20px",
                        cursor: "pointer",
                      }}
                      onClick={() => selectIcon(e)}
                    >
                      <ImageWrapper>
                        <Icon name={e} />
                      </ImageWrapper>
                    </div>
                  ))}
                </div>
                <div style={{ display: "flex", flexDirection: "row" }}>
                  {data2.map((e) => (
                    <div
                      style={{
                        margin: "1rem",
                        display: "inline !important",
                        width: "20px",
                        cursor: "pointer",
                      }}
                      onClick={() => selectIcon(e)}
                    >
                      <ImageWrapper>
                        <Icon name={e} />
                      </ImageWrapper>
                    </div>
                  ))}
                </div>
                <div style={{ display: "flex", flexDirection: "row" }}>
                  {data3.map((e) => (
                    <div
                      style={{
                        margin: "1rem",
                        display: "inline !important",
                        width: "20px",
                        cursor: "pointer",
                      }}
                      onClick={() => selectIcon(e)}
                    >
                      <ImageWrapper>
                        <Icon name={e} />
                      </ImageWrapper>
                    </div>
                  ))}
                </div>
                <div style={{ display: "flex", flexDirection: "row" }}>
                  {data4.map((e) => (
                    <div
                      style={{
                        margin: "1rem",
                        display: "inline !important",
                        width: "20px",
                        cursor: "pointer",
                      }}
                      onClick={() => selectIcon(e)}
                    >
                      <ImageWrapper>
                        <Icon name={e} />
                      </ImageWrapper>
                    </div>
                  ))}
                </div>
              </DrawerBox>
            </Col>
          </Row>
        </Scrollbars>

        <ButtonGroup>
          <Button
            kind={KIND.minimal}
            onClick={closeDrawer}
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: "50%",
                  borderTopLeftRadius: "3px",
                  borderTopRightRadius: "3px",
                  borderBottomRightRadius: "3px",
                  borderBottomLeftRadius: "3px",
                  marginRight: "15px",
                  color: $theme.colors.red400,
                }),
              },
            }}
          >
            Cancelar
          </Button>

          <Button
            type="submit"
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: "50%",
                  borderTopLeftRadius: "3px",
                  borderTopRightRadius: "3px",
                  borderBottomRightRadius: "3px",
                  borderBottomLeftRadius: "3px",
                }),
              },
            }}
          >
            Crear categoria
          </Button>
        </ButtonGroup>
      </Form>
    </>
  );
};

export default AddCategory;
