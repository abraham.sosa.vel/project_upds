import React, { useCallback, useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { v4 as uuidv4 } from 'uuid';
import { useMutation, gql } from '@apollo/client';
import { Scrollbars } from 'react-custom-scrollbars';
import { useDrawerDispatch, useDrawerState } from 'context/DrawerContext';
import Input from 'components/Input/Input';
import Checkbox from 'components/CheckBox/CheckBox';
import PhoneInput from 'components/PhoneInput/PhoneInput';
import Button, { KIND } from 'components/Button/Button';

import DrawerBox from 'components/DrawerBox/DrawerBox';
import { Row, Col } from 'components/FlexBox/FlexBox';
import {
  Form,
  DrawerTitleWrapper,
  DrawerTitle,
  FieldDetails,
  ButtonGroup,
} from '../DrawerItems/DrawerItems.style';
import { FormFields, FormLabel } from 'components/FormFields/FormFields';
import { Select } from 'baseui/select';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import { INITIAL_REQUEST_ORDERS_SECOND_START, REGISTER_DETAIL_DELIVERY_START, REGISTER_REQUEST_ROLES_START, REQUEST_BRANCHES_START, UPDATE_DETAIL_DELIVERY_START, UPDATE_REQUEST_ROLES_START } from 'redux/constants';

const GET_STAFFS = gql`
  query getStaffs($role: String, $searchBy: String) {
    staffs(role: $role, searchBy: $searchBy) {
      id
      name
      email
      contact_number
      creation_date
      role
    }
  }
`;

const CREATE_STAFF = gql`
  mutation createStaff($staff: AddStaffInput!) {
    createStaff(staff: $staff) {
      id
      first_name
      last_name
      name
      email
      contact_number
      creation_date
      role
    }
  }
`;

type Props = any;

const UsersForm: React.FC<Props> = (props) => {
  const dispatch = useDrawerDispatch();
  const closeDrawer = useCallback(() => dispatch({ type: 'CLOSE_DRAWER' }), [
    dispatch,
  ]);
  const [country, setCountry] = React.useState(undefined);
  const [checked, setChecked] = React.useState(true);
  const [text, setText] = React.useState('');
  const dispacher=useDispatch();
  const data = useDrawerState('data');
  const { register, handleSubmit, setValue } = useForm({
    defaultValues: data,
  });
  const [tag, setTag] = useState([]);
  const [type, setType] = useState([]);
  const [type2, setType2] = useState([]);
  const [createStaff] = useMutation(CREATE_STAFF, {
    update(cache, { data: { createStaff } }) {
      const { staffs } = cache.readQuery({
        query: GET_STAFFS,
      });

      cache.writeQuery({
        query: GET_STAFFS,
        data: { staffs: staffs.concat([createStaff]) },
      });
    },
  });

  const onSubmit = (e) => {
    if(type2[0]==undefined){
      console.log("Debe ingresar un rol")
    }else{
        data.length===undefined
      ?dispacher({type:REGISTER_REQUEST_ROLES_START,payload:{id:data.id,firstName:e.first,lastName:e.second,celphone:parseInt(e.celphone),email:e.email,gps:type2[0].value,password:e.password,userName:e.userName}})
      :dispacher({type:UPDATE_REQUEST_ROLES_START,payload:{firstName:e.first,lastName:e.second,celphone:parseInt(e.celphone),email:e.email,gps:type2[0].value,userName:`${e.first.substr(0,5)}_${e.second.substr(0,5)}`,password:`${e.second.substr(0,5)}${e.celphone}`,roleId:"fe571a2f-e540-4564-91cc-39c061e53828"}})
    }
    closeDrawer();
  };
  const handleTypeChange2 = ({ value }) => {
    /*if(value[0]?.value!==undefined){
    dispatcher({type:CATEGORIES_FILTER_START,payload:value[0]?.value})
    }*/
    setValue('type2', value);
    setType2(value);
  };

  const typeOptions = [
    { value: 'Chofer', name: 'Chofer', id: '1' },
    { value: 'Gerente', name: 'Gerente', id: '2' },
    { value: 'Contador', name: 'Contador', id: '3' },
    { value: 'Administrado', name: 'Administrado', id: '3' },
  ];
  console.log(data.length)
  console.log(data.length===0)
  return (
    <>
      <DrawerTitleWrapper>
        <DrawerTitle>Detalles del usuario</DrawerTitle>
      </DrawerTitleWrapper>

      <Form onSubmit={handleSubmit(onSubmit)} style={{ height: '100%' }}>
        <Scrollbars
          autoHide
          renderView={(props) => (
            <div {...props} style={{ ...props.style, overflowX: 'hidden' }} />
          )}
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              style={{ display: 'none' }}
              className="track-horizontal"
            />
          )}
        >
          <Row>
            <Col lg={4}>
              <FieldDetails>
                Datos del usuario
              </FieldDetails>
            </Col>

            <Col lg={8}>
              <DrawerBox>
                <FormFields>
                <FormLabel>Nombre</FormLabel>
                <Input
                    inputRef={register({ required: true, maxLength: 20 })}
                    name="first"
                  />
                </FormFields>
                <FormFields>
                  <FormLabel>Apellido</FormLabel>
                  <Input
                    inputRef={register({ required: true, maxLength: 20 })}
                    name="second"
                  />
                  </FormFields>
                  <FormFields>
                  <FormLabel>Celular</FormLabel>
                  <Input
                  type="number"
                    inputRef={register({ required: true, maxLength: 8, minLength:7 })}
                    name="celphone"
                  />
                  </FormFields>
                  {data.length===undefined
                  ?
                  <><FormFields>
                  <FormLabel>Contraseña</FormLabel>
                  <Input
                    inputRef={register({ required: true, maxLength: 20 })}
                    name="password"
                  />
                  </FormFields>
                  <FormFields>
                  <FormLabel>Nombre de usario</FormLabel>
                  <Input
                    inputRef={register({ required: true, maxLength: 20 })}
                    name="userName"
                  />
                  </FormFields>
                  </>
                :""}
                  <FormFields>
                  <FormLabel>Correo electronico</FormLabel>
                  <Input
                  type="email"
                    inputRef={register({ required: true, })}
                    name="email"
                  />
                  </FormFields>

              </DrawerBox>
            </Col>
          </Row>

          <Row>
            <Col lg={4}>
              <FieldDetails>
               Ingrese el Rol del usuario
              </FieldDetails>
            </Col>
            <Col lg={8}>
              <DrawerBox>
                <FormFields>
                  <FormLabel>Rol</FormLabel>
                  <Select
                    options={typeOptions}
                    labelKey="name"
                    valueKey="value"
                    placeholder="Tipo de categoria"
                    value={type2}
                    searchable={false}
                    onChange={handleTypeChange2}
                    overrides={{
                      Placeholder: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      DropdownListItem: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      OptionContent: {
                        style: ({ $theme, $selected }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $selected
                              ? $theme.colors.textDark
                              : $theme.colors.textNormal,
                          };
                        },
                      },
                      SingleValue: {
                        style: ({ $theme }) => {
                          return {
                            ...$theme.typography.fontBold14,
                            color: $theme.colors.textNormal,
                          };
                        },
                      },
                      Popover: {
                        props: {
                          overrides: {
                            Body: {
                              style: { zIndex: 5 },
                            },
                          },
                        },
                      },
                    }}
                  />
                </FormFields>

              </DrawerBox>
              </Col>
          </Row>
        </Scrollbars>


        <ButtonGroup>
          <Button
            kind={KIND.minimal}
            onClick={closeDrawer}
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: '50%',
                  borderTopLeftRadius: '3px',
                  borderTopRightRadius: '3px',
                  borderBottomRightRadius: '3px',
                  borderBottomLeftRadius: '3px',
                  marginRight: '15px',
                  color: $theme.colors.red400,
                }),
              },
            }}
          >
            Cancelar
          </Button>

          <Button
            type="submit"
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: '50%',
                  borderTopLeftRadius: '3px',
                  borderTopRightRadius: '3px',
                  borderBottomRightRadius: '3px',
                  borderBottomLeftRadius: '3px',
                }),
              },
            }}
          >
              {data.length===undefined
              ?"Actualizar usuario"
              :"Crear usuario"}
          </Button>
        </ButtonGroup>
      </Form>
    </>
  );
};

export default UsersForm;