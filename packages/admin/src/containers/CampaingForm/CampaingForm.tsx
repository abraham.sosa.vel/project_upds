import React, { useState, useCallback } from "react";
import { useForm } from "react-hook-form";
import { v4 as uuidv4 } from "uuid";
import { useMutation, gql } from "@apollo/client";
import { useDrawerDispatch } from "context/DrawerContext";
import { Scrollbars } from "react-custom-scrollbars";
import Input from "components/Input/Input";
import Select from "components/Select/Select";
import Button, { KIND } from "components/Button/Button";
import DrawerBox from "components/DrawerBox/DrawerBox";
import { Row, Col } from "components/FlexBox/FlexBox";
import {
  Form,
  DrawerTitleWrapper,
  DrawerTitle,
  FieldDetails,
  ButtonGroup,
} from "../DrawerItems/DrawerItems.style";
import { FormFields, FormLabel } from "components/FormFields/FormFields";
import { REGISTER_BRANCH_START } from "redux/constants";
import { useDispatch } from "react-redux";

const GET_COUPONS = gql`
  query getCoupons($status: String, $searchBy: String) {
    coupons(status: $status, searchBy: $searchBy) {
      id
      title
      code
      number_of_used_coupon
      number_of_coupon
      expiration_date
      creation_date
      status
    }
  }
`;
const CREATE_COUPON = gql`
  mutation createCoupon($coupon: AddCouponInput!) {
    createCoupon(coupon: $coupon) {
      id
      title
      code
      number_of_used_coupon
      number_of_coupon
      expiration_date
      creation_date
      status
    }
  }
`;
type Props = any;

const AddCampaing: React.FC<Props> = (props) => {
  const dispatch = useDrawerDispatch();
  const dispachere = useDispatch();
  const closeDrawer = useCallback(() => dispatch({ type: "CLOSE_DRAWER" }), [
    dispatch,
  ]);

  const { register, handleSubmit, setValue } = useForm();
  const [category, setCategory] = useState([]);
  React.useEffect(() => {
    register({ name: "category" });
  }, [register]);
  const [createCoupon] = useMutation(CREATE_COUPON, {
    update(cache, { data: { createCoupon } }) {
      const { coupons } = cache.readQuery({
        query: GET_COUPONS,
      });

      cache.writeQuery({
        query: GET_COUPONS,
        data: { coupons: coupons.concat([createCoupon]) },
      });
    },
  });
  const onSubmit = (data) => {
    return new Promise((resolve, reject) => {
      dispachere({
        type: REGISTER_BRANCH_START,
        payload: {
          name: data.name,
          direction: data.direction,
          celphone: data.phone,
          resolve,
          reject,
        },
      });
    })
      .then((err) => {
        closeDrawer();
      })
      .catch((err) => {
        console.log("error");
      });
  };
  const handleCategoryChange = ({ value }) => {
    setValue("category", value);
    setCategory(value);
  };

  return (
    <>
      <DrawerTitleWrapper>
        <DrawerTitle>Agregar sucursal</DrawerTitle>
      </DrawerTitleWrapper>

      <Form onSubmit={handleSubmit(onSubmit)} style={{ height: "100%" }}>
        <Scrollbars
          autoHide
          renderView={(props) => (
            <div {...props} style={{ ...props.style, overflowX: "hidden" }} />
          )}
          renderTrackHorizontal={(props) => (
            <div
              {...props}
              style={{ display: "none" }}
              className="track-horizontal"
            />
          )}
        >
          <Row>
            <Col lg={4}>
              <FieldDetails>Ingrese los datos de la sucursal</FieldDetails>
            </Col>

            <Col lg={8}>
              <DrawerBox>
                <FormFields>
                  <FormLabel>Nombre</FormLabel>
                  <Input inputRef={register({ required: true })} name="name" />
                </FormFields>

                <FormFields>
                  <FormLabel>Direccion</FormLabel>
                  <Input
                    inputRef={register({ required: true })}
                    name="direction"
                  />
                </FormFields>
                <FormFields>
                  <FormLabel>Telefono</FormLabel>
                  <Input
                    type="number"
                    inputRef={register({ required: true })}
                    name="phone"
                  />
                </FormFields>
              </DrawerBox>
            </Col>
          </Row>
        </Scrollbars>

        <ButtonGroup>
          <Button
            kind={KIND.minimal}
            onClick={closeDrawer}
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: "50%",
                  borderTopLeftRadius: "3px",
                  borderTopRightRadius: "3px",
                  borderBottomRightRadius: "3px",
                  borderBottomLeftRadius: "3px",
                  marginRight: "15px",
                  color: $theme.colors.red400,
                }),
              },
            }}
          >
            Cancelar
          </Button>

          <Button
            type="submit"
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: "50%",
                  borderTopLeftRadius: "3px",
                  borderTopRightRadius: "3px",
                  borderBottomRightRadius: "3px",
                  borderBottomLeftRadius: "3px",
                }),
              },
            }}
          >
            Crer sucursal
          </Button>
        </ButtonGroup>
      </Form>
    </>
  );
};

export default AddCampaing;
