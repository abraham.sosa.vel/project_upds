import React, { useEffect } from 'react';
import {  withStyle } from 'baseui';
import { Grid, Row as Rows, Col as Column } from 'components/FlexBox/FlexBox';
import { Wrapper, Header, Heading } from 'components/Wrapper.style';

import {
  TableWrapper,
  StyledTable,
  StyledHeadCell,
  StyledBodyCell,
} from './balance.style';
import NoResult from 'components/NoResult/NoResult';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import { INITIAL_REQUEST_SUGERENCY_START, DELETE_REQUEST_SUGERENCY, INITIAL_REQUEST_BALANCE_START } from 'redux/constants';

const Col = withStyle(Column, () => ({
  '@media only screen and (max-width: 767px)': {
    marginBottom: '20px',

    ':last-child': {
      marginBottom: 0,
    },
  },
}));

const Row = withStyle(Rows, () => ({
  '@media only screen and (min-width: 768px)': {
    alignItems: 'center',
  },
}));



export default function Balance() {

  let dispatch=useDispatch();
  useEffect(()=>{
    dispatch({type:INITIAL_REQUEST_BALANCE_START})
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])
  const datas = useSelector((state: RootStateOrAny) => state);
 

  return (
    <Grid fluid={true}>
      <Row>
        <Col md={12}>
          <Header
            style={{
              marginBottom: 30,
              boxShadow: '0 0 5px rgba(0, 0 ,0, 0.05)',
            }}
          >
            <Col md={2}>
              <Heading>Balance</Heading>
            </Col>
          </Header>

          <Wrapper style={{ boxShadow: '0 0 5px rgba(0, 0 , 0, 0.05)' }}>
            <TableWrapper>
              <StyledTable $gridTemplateColumns="minmax(300px, 400px) minmax(200px, 200px) minmax(150px, auto)">
              <StyledHeadCell>ID</StyledHeadCell>
              <StyledHeadCell>Monto</StyledHeadCell>
                <StyledHeadCell>Fecha pedido</StyledHeadCell>
                {datas ? (
                  datas.balance.length ? (
                    datas.balance
                      .map((item) => Object.values(item))
                      .map((row, index) => (
                        <React.Fragment key={index}>
                          <StyledBodyCell  style={{cursor:"pointer"}} >{row[0]}</StyledBodyCell>
                          <StyledBodyCell  style={{cursor:"pointer"}}>{row[1]} Bs</StyledBodyCell>
                          <StyledBodyCell  style={{cursor:"pointer"}} >{row[2]}</StyledBodyCell>
                        </React.Fragment>
                      ))
                  ) : (
                    <NoResult
                      hideButton={false}
                      style={{
                        gridColumnStart: '1',
                        gridColumnEnd: 'one',
                      }}
                    />
                  )
                ) : null}
              </StyledTable>
            </TableWrapper>
            <div style={{textAlign:"center"}}>
             <h2 style={{color:"#00C58D"}}>Total ganancias: <span style={{color:"#161F6A"}}>{datas.balance[datas.balance.length-1]?.suma} Bs</span></h2>
            </div>
          </Wrapper>
        </Col>
      </Row>
    </Grid>
  );
}
