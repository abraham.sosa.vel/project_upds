import React, { useEffect } from 'react';
import {  withStyle } from 'baseui';
import { Grid, Row as Rows, Col as Column } from 'components/FlexBox/FlexBox';
import { Wrapper, Header, Heading } from 'components/Wrapper.style';

import {
  TableWrapper,
  StyledTable,
  StyledHeadCell,
  StyledBodyCell,
} from './sugerency.style';
import NoResult from 'components/NoResult/NoResult';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import { INITIAL_REQUEST_ROLES_START,DELETE_REQUEST_ROLES_START, INITIAL_REQUEST_SUGERENCY_START, DELETE_REQUEST_SUGERENCY } from 'redux/constants';

const Col = withStyle(Column, () => ({
  '@media only screen and (max-width: 767px)': {
    marginBottom: '20px',

    ':last-child': {
      marginBottom: 0,
    },
  },
}));

const Row = withStyle(Rows, () => ({
  '@media only screen and (min-width: 768px)': {
    alignItems: 'center',
  },
}));



export default function Sugerency() {

  let dispatch=useDispatch();
  useEffect(()=>{
    dispatch({type:INITIAL_REQUEST_SUGERENCY_START})
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])
  const datas = useSelector((state: RootStateOrAny) => state);
 

const deleteOpen=(e)=>{
  dispatch({type:DELETE_REQUEST_SUGERENCY,payload:e})
}
  return (
    <Grid fluid={true}>
      <Row>
        <Col md={12}>
          <Header
            style={{
              marginBottom: 30,
              boxShadow: '0 0 5px rgba(0, 0 ,0, 0.05)',
            }}
          >
            <Col md={2}>
              <Heading>Sugerencias</Heading>
            </Col>
          </Header>

          <Wrapper style={{ boxShadow: '0 0 5px rgba(0, 0 , 0, 0.05)' }}>
            <TableWrapper>
              <StyledTable $gridTemplateColumns="minmax(300px, 300px) minmax(400px, 400px) minmax(150px, 150px) minmax(90px, auto)">
              <StyledHeadCell>ID</StyledHeadCell>
              <StyledHeadCell>Descripcion</StyledHeadCell>
                <StyledHeadCell>Fecha</StyledHeadCell>
                <StyledHeadCell>Eliminar</StyledHeadCell>
                {datas ? (
                  datas.sugerency.length ? (
                    datas.sugerency
                      .map((item) => Object.values(item))
                      .map((row, index) => (
                        <React.Fragment key={index}>
                          <StyledBodyCell  style={{cursor:"pointer"}} >{row[0]}</StyledBodyCell>
                          <StyledBodyCell  style={{cursor:"pointer"}}>{row[1]}</StyledBodyCell>
                          <StyledBodyCell  style={{cursor:"pointer"}} >{row[2]}</StyledBodyCell>
                          <StyledBodyCell onClick={()=>{deleteOpen(row[0])}} style={{cursor:"pointer"}}>Eliminar</StyledBodyCell>  
                        </React.Fragment>
                      ))
                  ) : (
                    <NoResult
                      hideButton={false}
                      style={{
                        gridColumnStart: '1',
                        gridColumnEnd: 'one',
                      }}
                    />
                  )
                ) : null}
              </StyledTable>
            </TableWrapper>
          </Wrapper>
        </Col>
      </Row>
    </Grid>
  );
}
