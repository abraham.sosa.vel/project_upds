import React, { useEffect, useState } from 'react';
import dayjs from 'dayjs';
import { styled, withStyle } from 'baseui';
import { Grid, Row as Rows, Col as Column } from 'components/FlexBox/FlexBox';
import Input from 'components/Input/Input';
import Select from 'components/Select/Select';
import { useQuery, gql } from '@apollo/client';
import { Wrapper, Header, Heading } from 'components/Wrapper.style';

import {
  TableWrapper,
  StyledTable,
  StyledHeadCell,
  StyledBodyCell,
} from './users.style';
import NoResult from 'components/NoResult/NoResult';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import { INITIAL_REQUEST_ROLES_START,DELETE_REQUEST_ROLES_START } from 'redux/constants';
import { userFilterMax, userFilterMin } from 'redux/actions';
import { Button } from 'baseui/button';
import { useDrawerDispatch } from 'context/DrawerContext';

const GET_CUSTOMERS = gql`
  query getCustomers($searchBy: String, $sortBy: String) {
    customers(searchBy: $searchBy, sortBy: $sortBy) {
      id
      image
      name
      contacts {
        id
        type
        number
      }
      total_order
      total_order_amount
      creation_date
    }
  }
`;

const Col = withStyle(Column, () => ({
  '@media only screen and (max-width: 767px)': {
    marginBottom: '20px',

    ':last-child': {
      marginBottom: 0,
    },
  },
}));

const Row = withStyle(Rows, () => ({
  '@media only screen and (min-width: 768px)': {
    alignItems: 'center',
  },
}));

const ImageWrapper = styled('div', ({ $theme }) => ({
  width: '38px',
  height: '38px',
  overflow: 'hidden',
  display: 'inline-block',
  borderTopLeftRadius: '20px',
  borderTopRightRadius: '20px',
  borderBottomRightRadius: '20px',
  borderBottomLeftRadius: '20px',
  backgroundColor: $theme.colors.backgroundF7,
}));

const Image = styled('img', () => ({
  width: '100%',
  height: 'auto',
}));

const sortByOptions = [
  { value: 'mayorCantidad', label: 'Mayor cantidad' },
  { value: 'menorCantidad', label: 'Menor cantidad' },
];
const sortByOptions2 = [
  { value: 'ultimosPedidos', label: 'Pedidos mas recientes' },
  { value: 'pedidosAntiguos', label: 'Pedidos mas antiguos' },
];

export default function Users() {
  const { data, error, refetch } = useQuery(GET_CUSTOMERS);
  const [stock, setStock] = useState([]);
  const [stock2, setStock2] = useState([]);
  const [search, setSearch] = useState([]);
  const dispatcher = useDrawerDispatch();

  let dispatch=useDispatch();
  useEffect(()=>{
    dispatch({type:INITIAL_REQUEST_ROLES_START})
  },[])
  const datas = useSelector((state: RootStateOrAny) => state);
 

  const openDrawer = React.useCallback((e) =>
  dispatcher({
    type: "OPEN_DRAWER",
    drawerComponent: "USERS_FORM",
    data: e,
  }),
[dispatcher, data]
);
const deleteOpen=(e)=>{
  dispatch({type:DELETE_REQUEST_ROLES_START,payload:e})
}
  return (
    <Grid fluid={true}>
      <Row>
        <Col md={12}>
          <Header
            style={{
              marginBottom: 30,
              boxShadow: '0 0 5px rgba(0, 0 ,0, 0.05)',
            }}
          >
            <Col md={2}>
              <Heading>Usuarios</Heading>
            </Col>

            <Col md={10}>
              <Row>
                <Col md={4}>
                  <Button  onClick={()=>{openDrawer([])}}>
                      Agregar nuevo usuario
                  </Button>
                </Col>
              </Row>
            </Col>
          </Header>

          <Wrapper style={{ boxShadow: '0 0 5px rgba(0, 0 , 0, 0.05)' }}>
            <TableWrapper>
              <StyledTable $gridTemplateColumns="minmax(200px, 200px) minmax(120px, 120px) minmax(200px, 200px) minmax(100px, 100px) minmax(150px, auto) minmax(200px, 200px)minmax(90px, 90px)">
              <StyledHeadCell>id</StyledHeadCell>
                <StyledHeadCell>Celular</StyledHeadCell>
                <StyledHeadCell>Correo</StyledHeadCell>
                <StyledHeadCell>Rol</StyledHeadCell>
                <StyledHeadCell>Contraseña</StyledHeadCell>
                <StyledHeadCell>Nombre Usuario</StyledHeadCell>
                <StyledHeadCell>Eliminar</StyledHeadCell>
                {datas ? (
                  datas.roles.length ? (
                    datas.roles
                      .map((item) => Object.values(item))
                      .map((row, index) => (
                        <React.Fragment key={index}>
                          <StyledBodyCell  onClick={()=>{openDrawer(datas.roles[index])}} style={{cursor:"pointer"}} >{row[0]}</StyledBodyCell>
                          <StyledBodyCell onClick={()=>{openDrawer(datas.roles[index])}} style={{cursor:"pointer"}}>{row[1]}</StyledBodyCell>
                          <StyledBodyCell onClick={()=>{openDrawer(datas.roles[index])}} style={{cursor:"pointer"}}>{row[2]}</StyledBodyCell>
                          <StyledBodyCell onClick={()=>{openDrawer(datas.roles[index])}} style={{cursor:"pointer"}}>{row[3]}</StyledBodyCell>
                          <StyledBodyCell onClick={()=>{openDrawer(datas.roles[index])}} style={{cursor:"pointer"}}>{row[4]}</StyledBodyCell>
                          <StyledBodyCell onClick={()=>{openDrawer(datas.roles[index])}} style={{cursor:"pointer"}}>{row[5]}</StyledBodyCell>  
                          <StyledBodyCell onClick={()=>{deleteOpen(row[0])}} style={{cursor:"pointer"}}>Eliminar</StyledBodyCell>  
                        </React.Fragment>
                      ))
                  ) : (
                    <NoResult
                      hideButton={false}
                      style={{
                        gridColumnStart: '1',
                        gridColumnEnd: 'one',
                      }}
                    />
                  )
                ) : null}
              </StyledTable>
            </TableWrapper>
          </Wrapper>
        </Col>
      </Row>
    </Grid>
  );
}
