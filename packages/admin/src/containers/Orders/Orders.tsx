import React, { useEffect, useState } from 'react';
import { styled, withStyle, createThemedUseStyletron } from 'baseui';
import dayjs from 'dayjs';
import { Grid, Row as Rows, Col as Column } from 'components/FlexBox/FlexBox';
import Select from 'components/Select/Select';
import Input from 'components/Input/Input';

import { useQuery, gql } from '@apollo/client';
import { Wrapper, Header, Heading } from 'components/Wrapper.style';
import Checkbox from 'components/CheckBox/CheckBox';

import {
  TableWrapper,
  StyledTable,
  StyledHeadCell,
  StyledCell,
} from './Orders.style';
import NoResult from 'components/NoResult/NoResult';
import { Plus } from 'assets/icons/Plus';
import { useDrawerDispatch } from 'context/DrawerContext';
import { INITIAL_REQUEST_ORDERS_START, ORDER_FILTER_SEARCH_START, ORDER_FILTER_STATUS_START } from 'redux/constants';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import { orderFilterMax, orderFilterMin, orderFilterStatus2 } from 'redux/actions';

const GET_ORDERS = gql`
  query getOrders($status: String, $limit: Int, $searchText: String) {
    orders(status: $status, limit: $limit, searchText: $searchText) {
      id
      customer_id
      creation_date
      delivery_address
      amount
      payment_method
      contact_number
      status
    }
  }
`;

type CustomThemeT = { red400: string; textNormal: string; colors: any };
const themedUseStyletron = createThemedUseStyletron<CustomThemeT>();

const Status = styled('div', ({ $theme }) => ({
  ...$theme.typography.fontBold14,
  color: $theme.colors.textDark,
  display: 'flex',
  alignItems: 'center',
  lineHeight: '1',
  textTransform: 'capitalize',

  ':before': {
    content: '""',
    width: '10px',
    height: '10px',
    display: 'inline-block',
    borderTopLeftRadius: '10px',
    borderTopRightRadius: '10px',
    borderBottomRightRadius: '10px',
    borderBottomLeftRadius: '10px',
    backgroundColor: $theme.borders.borderE6,
    marginRight: '10px',
  },
}));

const Col = withStyle(Column, () => ({
  '@media only screen and (max-width: 767px)': {
    marginBottom: '20px',

    ':last-child': {
      marginBottom: 0,
    },
  },
}));

const Row = withStyle(Rows, () => ({
  '@media only screen and (min-width: 768px)': {
    alignItems: 'center',
  },
}));

const statusSelectOptions = [
  { value: 'recibido', label: 'recibido' },
    { value: 'rechazado', label: 'rechazado' },
    { value: 'entregado', label: 'entregado' },
    { value: 'enviando', label: 'enviando' },
    { value: 'Sin revisar', label: 'Sin revisar' },
];
const limitSelectOptions = [
  { value: "recent", label: 'Pedidos mas recientes' },
  { value: "last", label: 'Pedidos mas antiguos' },
];

export default function Orders() {
  const [checkedId, setCheckedId] = useState([]);
  const [checked, setChecked] = useState(false);
  const dispatch = useDrawerDispatch();

  const [useCss, theme] = themedUseStyletron();
  const sent = useCss({
    ':before': {
      content: '""',
      backgroundColor: theme.colors.primary,
    },
  });
  const failed = useCss({
    ':before': {
      content: '""',
      backgroundColor: theme.colors.red400,
    },
  });
  const processing = useCss({
    ':before': {
      content: '""',
      backgroundColor: theme.colors.textNormal,
    },
  });
  const revised = useCss({
    ':before': {
      content: '""',
      backgroundColor: theme.colors.textNormal,
    },
  });
  const paid = useCss({
    ':before': {
      content: '""',
      backgroundColor: theme.colors.blue400,
    },
  });

  const [status, setStatus] = useState([]);
  const [limit, setLimit] = useState([]);
  const [search, setSearch] = useState([]);
  const [idClick, setidClick] = useState([]);
  const [tag, setTag] = useState([]);
  const { data, error, refetch } = useQuery(GET_ORDERS);
  const dispatcher=useDispatch();

  function handleStatus({ value }) {
    setStatus(value);
    if (value.length) {
      if(value[0].value=="Sin revisar"){
        dispatcher(orderFilterStatus2(datas.orders.filter(e=>e.state=="Sin revisar")))
      }else{
        console.log("sin")
        dispatcher({type:ORDER_FILTER_STATUS_START,payload:value[0].value})
      }
      refetch({
        status: value[0].value,
        limit: limit.length ? limit[0].value : null,
      });
    } else {
      dispatcher({type:INITIAL_REQUEST_ORDERS_START})
      refetch({ status: null });
    }
  }
  const datas = useSelector((state: RootStateOrAny) => state);
  function handleLimit({ value }) {
    setLimit(value);
    if (value.length) {
      if(value[0].value=="last"){
        dispatcher(orderFilterMin((datas.orders.sort((a, b) => {
          if(a.pedido < b.pedido) return -1;
          if(a.pedido > b.pedido) return 1;
      
          return 0;
      }))))
      }else{
        dispatcher(orderFilterMax((datas.orders.sort((a, b) => {
        if(a.pedido < b.pedido) return 1;
        if(a.pedido > b.pedido) return -1;
    
        return 0;
    }))))
      }
      refetch({
        status: status.length ? status[0].value : null,
        limit: value[0].value,
      });
    } else {
      dispatcher({type:INITIAL_REQUEST_ORDERS_START})
      refetch({
        limit: null,
      });
    }
  }
  function handleSearch(event) {
    const { value } = event.currentTarget;
    setSearch(value);
    console.log(value)
    if(value.length!=0){
      dispatcher({type:ORDER_FILTER_SEARCH_START,payload:value})
    }else{
      dispatcher({type:INITIAL_REQUEST_ORDERS_START})
    }
    refetch({ searchText: value });
  }
  function onAllCheck(event) {
    if (event.target.checked) {
      const idx = data && data.orders.map((order) => order.id);
      setCheckedId(idx);
    } else {
      setCheckedId([]);
    }
    setChecked(event.target.checked);
  }

  function handleCheckbox(event) {
    const { name } = event.currentTarget;
    if (!checkedId.includes(name)) {
      setCheckedId((prevState) => [...prevState, name]);
    } else {
      setCheckedId((prevState) => prevState.filter((id) => id !== name));
    }
  }
  useEffect(()=>{
    dispatcher({type:INITIAL_REQUEST_ORDERS_START})
  },[])
 
  const openDrawer = React.useCallback((e) =>
      dispatch({
        type: "OPEN_DRAWER",
        drawerComponent: "STAFF_MEMBER_FORM",
        data: e,
      }),
    [dispatch, data]
  );
  return (
    <Grid fluid={true}>
      <Row>
        <Col md={12}>
          <Header
            style={{
              marginBottom: 30,
              boxShadow: '0 0 8px rgba(0, 0 ,0, 0.1)',
            }}
          >
            <Col md={3} xs={12}>
              <Heading>Ordenes</Heading>
            </Col>

            <Col md={9} xs={12}>
              <Row>
                <Col md={3} xs={12}>
                  <Select
                    options={statusSelectOptions}
                    labelKey="label"
                    valueKey="value"
                    placeholder="Estado"
                    value={status}
                    searchable={false}
                    onChange={handleStatus}
                  />
                </Col>

                <Col md={3} xs={12}>
                  <Select
                    options={limitSelectOptions}
                    labelKey="label"
                    valueKey="value"
                    value={limit}
                    placeholder="Tiempo pedido"
                    searchable={false}
                    onChange={handleLimit}
                  />
                </Col>

                <Col md={6} xs={12}>
                  <Input
                    value={search}
                    placeholder="Buscar celular"
                    onChange={handleSearch}
                    clearable
                  />
                </Col>
              </Row>
            </Col>
          </Header>

          <Wrapper style={{ boxShadow: '0 0 5px rgba(0, 0 , 0, 0.05)' }}>
            <TableWrapper>
              <StyledTable $gridTemplateColumns=" minmax(150px, 150px) minmax(150px, auto) minmax(150px, auto) minmax(200px, max-content) minmax(150px, auto) minmax(150px, auto) ">
                <StyledHeadCell>ID</StyledHeadCell>
                <StyledHeadCell>Celular</StyledHeadCell>
                <StyledHeadCell>Fecha de pedido</StyledHeadCell>
                <StyledHeadCell>Fecha de entrega</StyledHeadCell>
                <StyledHeadCell>Verificar</StyledHeadCell>
                <StyledHeadCell>Estado</StyledHeadCell>

                {datas ? (
                  datas.orders.length ? (
                    datas.orders
                      .map((item) => Object.values(item))
                      .map((row, index) => (
                        <React.Fragment key={index}>
                          <StyledCell>{row[0]}</StyledCell>
                          <StyledCell>{row[1]}</StyledCell>
                          <StyledCell>{row[2]}</StyledCell>
                          <StyledCell>{row[3]}</StyledCell>
                          <StyledCell  onClick={()=>{openDrawer(row[0])}} style={{cursor:"pointer"}}><Plus />{` Revisar`}</StyledCell>
                          <StyledCell style={{ justifyContent: 'center' }}>
                            <Status
                              className={
                                row[4] === 'entregado'
                                  ? sent
                                  : row[4] === 'enviando'
                                  ? revised
                                  : row[4] === 'Sin revisar'
                                  ? processing
                                  : row[4]=== 'recibido'
                                  ? paid
                                  : row[4] === 'rechazado'
                                  ? failed
                                  : ''
                              }
                            >
                              {row[4]}
                            </Status>
                          </StyledCell>
                        </React.Fragment>
                      ))
                  ) : (
                    <NoResult
                      hideButton={false}
                      style={{
                        gridColumnStart: '1',
                        gridColumnEnd: 'one',
                      }}
                    />
                  )
                ) : null}
              </StyledTable>
            </TableWrapper>
          </Wrapper>
        </Col>
      </Row>
    </Grid>
  );
}
