import { styled, withStyle } from "baseui";
import { Grid, Row as Rows, Col as Column } from "components/FlexBox/FlexBox";
import { gql } from "@apollo/client";
import Fade from "react-reveal/Fade";
import Products2 from "components/Products2/Products2";
import NoResult from "components/NoResult/NoResult";
import { CURRENCY } from "settings/constants";
import Placeholder from "components/Placeholder/Placeholder";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { INITIAL_REQUEST_ADS_START } from "redux/constants";

export const ProductsRow = styled("div", ({ $theme }) => ({
  display: "flex",
  flexWrap: "wrap",
  marginTop: "25px",
  backgroundColor: $theme.colors.backgroundF7,
  position: "relative",
  zIndex: "1",

  "@media only screen and (max-width: 767px)": {
    marginLeft: "-7.5px",
    marginRight: "-7.5px",
    marginTop: "15px",
  },
}));

export const Col = withStyle(Column, () => ({
  "@media only screen and (max-width: 767px)": {
    marginBottom: "20px",

    ":last-child": {
      marginBottom: 0,
    },
  },
}));

const Row = withStyle(Rows, () => ({
  "@media only screen and (min-width: 768px) and (max-width: 991px)": {
    alignItems: "center",
  },
}));

export const ProductCardWrapper = styled("div", () => ({
  height: "100%",
}));

export const LoaderWrapper = styled("div", () => ({
  width: "100%",
  height: "100vh",
  display: "flex",
  flexWrap: "wrap",
}));

export const LoaderItem = styled("div", () => ({
  width: "25%",
  padding: "0 15px",
  marginBottom: "30px",
}));

export default function Advertisements() {
  /*if (error) {
    return <div>Error! {error.message}</div>;
  }*/
  let dispatch=useDispatch();
  useEffect(()=>{
    dispatch({type:INITIAL_REQUEST_ADS_START})
  },[])
  const datas = useSelector((state: RootStateOrAny) => state);
  console.log(datas)
  return (
    <Grid fluid={true}>
      <Row>
        <Col md={12}>
          <Row>
            {datas ? (
              datas.ads && datas.ads.items?.length !== 0 ? (
                datas.ads.items?.map((item: any, index: number) => (
                  <Col
                    md={4}
                    lg={4}
                    sm={6}
                    xs={12}
                    key={index}
                    style={{ margin: "15px 0" }}
                  >
                    <Fade bottom duration={800} delay={index * 10}>
                      <Products2 
                      title={item.title}
                      image={item.image}
                      price={2323}
                      data={item}
                      phone1={item.celphones[0]}
                      phone2={item.celphones[1]}
                      phone3={item.celphones[2]}
                      />
                    </Fade>
                  </Col>
                ))
              ) : (
                <NoResult />
              )
            ) : (
              <LoaderWrapper>
                <LoaderItem>
                  <Placeholder />
                </LoaderItem>
                <LoaderItem>
                  <Placeholder />
                </LoaderItem>
                <LoaderItem>
                  <Placeholder />
                </LoaderItem>
                <LoaderItem>
                  <Placeholder />
                </LoaderItem>
              </LoaderWrapper>
            )}
          </Row>
        </Col>
      </Row>
    </Grid>
  );
}
