import React, { useCallback, useEffect, useState } from "react";
import { withStyle } from "baseui";
import { Grid, Row as Rows, Col as Column } from "components/FlexBox/FlexBox";
import { useDrawerDispatch } from "context/DrawerContext";
import Select from "components/Select/Select";
import Input from "components/Input/Input";
import Button from "components/Button/Button";
import Checkbox from "components/CheckBox/CheckBox";
import { useQuery, gql } from "@apollo/client";
import { Wrapper, Header, Heading } from "components/Wrapper.style";
import {
  TableWrapper,
  StyledTable,
  StyledHeadCell,
  StyledCell,
  ImageWrapper,
} from "./Category.style";
import { Plus } from "assets/icons/Plus";
import * as icons from "assets/icons/category-icons";
import NoResult from "components/NoResult/NoResult";

import { useDispatch, useSelector,RootStateOrAny } from "react-redux";
import { INITIAL_REQUEST_CATEGORIES_START, INITIAL_REQUEST_SEARCH_CATEGORIES_START } from "redux/constants";

const GET_CATEGORIES = gql`
  query getCategories($type: String, $searchBy: String) {
    categories(type: $type, searchBy: $searchBy) {
      id
      icon
      name
      slug
      type
    }
  }
`;

const Col = withStyle(Column, () => ({
  "@media only screen and (max-width: 767px)": {
    marginBottom: "20px",

    ":last-child": {
      marginBottom: 0,
    },
  },
}));

const Row = withStyle(Rows, () => ({
  "@media only screen and (min-width: 768px)": {
    alignItems: "center",
  },
}));

const categorySelectOptions = [
  { value: "6c441d0f-9662-412d-9bb0-447f74873546", label: "Construccion" },
  { value: "e07367c7-fdac-4347-99ae-75d32b5c9499", label: "Herramientas" },
  { value: "1e11c24e-e829-4a9e-ad46-28a9945f89b1", label: "Mano de obra" },
];
export default function Category() {
  const [category, setCategory] = useState([]);
  const [search, setSearch] = useState("");
  const dispatche = useDrawerDispatch();
  const openDrawer = useCallback(
    () => dispatche({ type: "OPEN_DRAWER", drawerComponent: "CATEGORY_FORM" }),
    [dispatche]
  );
  const dispatch=useDispatch();
  useEffect(() => {
    dispatch({ type: INITIAL_REQUEST_CATEGORIES_START,payload:"" });
  }, []);
  const spin = useSelector((state: RootStateOrAny) => state);
  const { data, error, refetch } = useQuery(GET_CATEGORIES);
  {
    /*if (error) {
    return <div>Error! {error.message}</div>;
  }*/
  }
  function handleSearch(event) {
    const value = event.currentTarget.value;
    setSearch(value);
    if(value.length!=0){
      dispatch({type:INITIAL_REQUEST_SEARCH_CATEGORIES_START,payload:value})
    }else{
      dispatch({ type: INITIAL_REQUEST_CATEGORIES_START,payload:"" });
    }
    refetch({
      type: category.length ? category[0].value : null,
      searchBy: value,
    });
  }
  function handleCategory({ value }) {
    setCategory(value);
    if (value.length) {
      dispatch({ type: INITIAL_REQUEST_CATEGORIES_START,payload:value[0].value });
      refetch({
        type: value[0].value,
      });
    } else {
      dispatch({ type: INITIAL_REQUEST_CATEGORIES_START,payload:"" });
      refetch({
        type: null,
      });
    }
  }
  const Icon = ({ name }) => {
    const TagName = icons[name];
    return !!TagName ? <TagName /> : <p>Invalid icon {name}</p>;
  };
  return (
    <Grid fluid={true}>
      <Row>
        <Col md={12}>
          <Header
            style={{
              marginBottom: 30,
              boxShadow: "0 0 5px rgba(0, 0 ,0, 0.05)",
            }}
          >
            <Col md={2}>
              <Heading>Categorias</Heading>
            </Col>

            <Col md={10}>
              <Row>
                <Col md={3} lg={3}>
                  <Select
                    options={categorySelectOptions}
                    labelKey="label"
                    valueKey="value"
                    placeholder="Tipo"
                    value={category}
                    searchable={false}
                    onChange={handleCategory}
                  />
                </Col>

                <Col md={5} lg={6}>
                  <Input
                    value={search}
                    placeholder="Buscar categoria"
                    onChange={handleSearch}
                    clearable
                  />
                </Col>

                <Col md={4} lg={3}>
                  <Button
                    onClick={openDrawer}
                    startEnhancer={() => <Plus />}
                    overrides={{
                      BaseButton: {
                        style: () => ({
                          width: "100%",
                          borderTopLeftRadius: "3px",
                          borderTopRightRadius: "3px",
                          borderBottomLeftRadius: "3px",
                          borderBottomRightRadius: "3px",
                        }),
                      },
                    }}
                  >
                    Añadir Categoria
                  </Button>
                </Col>
              </Row>
            </Col>
          </Header>

          <Wrapper style={{ boxShadow: "0 0 5px rgba(0, 0 , 0, 0.05)" }}>
            <TableWrapper>
              <StyledTable $gridTemplateColumns="minmax(200px, auto) minmax(100px, auto) minmax(100px, auto) minmax(70px, 70px) minmax(130px, 200px) ">
                <StyledHeadCell>Id</StyledHeadCell>
                <StyledHeadCell>Nombre</StyledHeadCell>
                <StyledHeadCell>Url</StyledHeadCell>
                <StyledHeadCell>Icono</StyledHeadCell>
                <StyledHeadCell>Tipo</StyledHeadCell>

                {spin ? (
                  spin.categories.length ? (
                    spin.categories
                      .map((item) => Object.values(item))
                      .map((row, index) => (
                        <React.Fragment key={index}>
                          <StyledCell>{row[0]}</StyledCell>
                          <StyledCell>{row[1]}</StyledCell>
                          <StyledCell>{row[2]}</StyledCell>
                          <StyledCell>
                            <ImageWrapper>
                              <Icon name={row[3]} />
                            </ImageWrapper>
                          </StyledCell>
                          <StyledCell>{row[4]}</StyledCell>
                        </React.Fragment>
                      ))
                  ) : (
                    <NoResult
                      hideButton={false}
                      style={{
                        gridColumnStart: "1",
                        gridColumnEnd: "one",
                      }}
                    />
                  )
                ) : null}
              </StyledTable>
            </TableWrapper>
          </Wrapper>
        </Col>
      </Row>
    </Grid>
  );
}
