import React from "react";
import {
  ProductCardWrapper,
  ProductImageWrapper,
  ProductInfo,
  SaleTag,
  DiscountPercent,
  Image,
  ProductTitle,
  ProductWeight,
  ProductMeta,
  OrderID,
  ProductPriceWrapper,
  ProductPrice,
  DiscountedPrice,
} from "./Products.style";
import { useDrawerDispatch } from "context/DrawerContext";

type ProductCardProps = {
  title: string;
  image: any;
  weight?: string;
  currency?: string;
  price: number;
  salePrice?: number;
  orderId?: number;
  discountInPercent?: number;
  phone1?:number;
  phone2?:number;
  phone3?:number;
  data: any;
};

const Products2: React.FC<ProductCardProps> = ({
  title,
  image,
  phone1,
  phone2,
  phone3,
  data,
  ...props
}) => {
  const dispatch = useDrawerDispatch();

  const openDrawer = React.useCallback(
    () =>
      dispatch({
        type: "OPEN_DRAWER",
        drawerComponent: "ADVERTISEMENTS_FORM",
        data: data,
      }),
    [dispatch, data]
  );
  return (
    <ProductCardWrapper
      {...props}
      className="product-card"
      onClick={openDrawer}
    >
      <ProductImageWrapper>
        <Image url={image} className="product-image" />
      </ProductImageWrapper>
      <ProductInfo>
        <h3 style={{marginBottom:"4px",color:"#161f6a"}}>Descripcion</h3>
        <ProductWeight style={{fontSize:"1.1rem" }}>{title}</ProductWeight>
          <h3 style={{marginBottom:"4px",color:"#161f6a"}}>Telefono</h3>
        <ProductMeta>
        <ProductPrice>{phone1}</ProductPrice>
        <ProductPrice>{phone2}</ProductPrice>
        <ProductPrice>{phone3}</ProductPrice>
         </ProductMeta>
      </ProductInfo>
    </ProductCardWrapper>
  );
};

export default Products2;
