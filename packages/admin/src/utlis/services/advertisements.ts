import { StorageServices } from "./storage";

export class Advertisements extends StorageServices {
  async getAds() {
    const filter = {
        fields: {
            lastUpdate:false
        },
        include:[
            {
                relation:"fileStorages",
                scope:{
                    fields:{
                        linkFile:true,
                        adId:true,
                        id:true,
                        }
                      }
            }
        ],
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`ads`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}
