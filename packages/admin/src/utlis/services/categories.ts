import { StorageServices } from "./storage";

export class Categories extends StorageServices {
  async getCategories(data) {
    let filter;
    if(data==""){
       filter = {
        fields:{
          "children":false,
        }
      };
    }else{
      filter = {
        fields:{
          "children":false,
        },
        where:{
          typeCategoryId:data
        }
      };
    }
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`categories`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getCategoriesSearch(data) {
    let filter = {
      fields:{
        "children":false,
      },
      where:{
        title:data
      }
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`categories`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }


  async getCategoriesSearchData(data) {
    let filter = {
      fields: {
        id:true,
        title:true
     },
     where:{
        typeCategoryId:data
     }
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`categories`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }

}
