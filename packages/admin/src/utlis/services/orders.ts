import { StorageServices } from "./storage";

export class Orders extends StorageServices {
  async getOrders() {
    const filter = {
        fields:{
             id:true,
             dateDelivery:true,
             hoursDelivery:true,
             paymentId:true,
        },
         include:[
                {
                    relation:"detailDeliveries",
                    scope:{
                        fields:{
                        },
                        include:[
                            {
                                relation:"statusDelivery"
                                
                            }
                        ]
                    },
                },
                {
                    relation:"payment",
                    scope:{
                        fields:{
                            lastUpdate:false
                        }, 
                        include:[
                             {
                                relation:"user",
                                scope:{
                                    fields:{
                                        firstName:false,
                                        lastName:false,
                                        email:false,
                                        gps:false,
                                        password:false,
                                        userName:false
                                    },
                                }
                            },
                        ]
                    }
                },
            ],
     };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpointWhitToken(`detail-payments`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }


  async getOrdersFilter(data) {
    const filter = {
        fields:{
             priceDelivery:false
        },
        where:{
            id:data
        },
         include:[
                {
                    relation:"detailDeliveries",
                    scope:{
                        fields:{
                        },
                        include:[
                            {
                                relation:"statusDelivery"
                                
                            },
                            {
                                relation:"branch"
                                
                            }
                        ]
                    },
                },
                {
                    relation:"payment",
                    scope:{
                        fields:{
                            lastUpdate:false
                        }, 
                        include:[
                             {
                                relation:"user",
                                scope:{
                                    fields:{
                                        email:false,
                                        gps:false,
                                        password:false,
                                        userName:false
                                    },
                                }
                            },
                        ]
                    }
                },
                 {
                    relation:"orderDetails",
                    scope:{
                        fields:{},
                         include:[
                             {
                                relation:"product",
                                scope:{
                                    fields:{
                                        amountMin:false,
                                    },
                                    include:[
                                        {
                                            relation:"fileStorages",
                                            scope:{
                                                fields:{
                                                    id:true,
                                                    linkFile:true,
                                                    productId:true
                                                }
                                            }},
                                        {
                                            relation:"prices",
                                            scope:{
                                                fields:{
                                                 
                                                }
                                            }}
                                    ]
                                }
                            },
                        ]
                    }
                },
            ],
     };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpointWhitToken(`detail-payments`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}