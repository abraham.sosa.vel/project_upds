import { StorageServices } from "./storage";

export class Users extends StorageServices {
  async getUsers() {
    const filter = {
        fields: {
        },
        where:{
          roleId:"d1254697-6d43-451a-876f-7b1ca235e0ae"
        },
        order:"celphone",
        include:[
             {
                relation:"payments",
            },
        ],
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpointWhitToken(`users`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getUsersSearch(data) {
    const filter = {
        fields: {
        },
        where:{
          roleId:"d1254697-6d43-451a-876f-7b1ca235e0ae",
          celphone:parseInt(data)
        },
        order:"celphone",
        include:[
             {
                relation:"payments",
            },
        ],
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpointWhitToken(`users`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}
