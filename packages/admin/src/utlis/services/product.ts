import { StorageServices } from "./storage";

export class Products extends StorageServices {
  async getProducts() {
    const filter = {
        fields: {
         },
         include:[
             {
                 relation:"fileStorages",
                 scope:{
                     fields:{
                           linkFile:true,
                         productId:true,
                         id:true,
                             }
                       }
             },
               {
                 relation:"prices",
                 scope:{
                     fields:{
                             },
                       }
             },
              {
                 relation:"category",
                 scope:{
                     fields:{
                             },
                       }
             }
         ],
  };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpointWhitToken(`products`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getProductsSearch(data) {
    const filter = {
        fields: {
         },
        where:{
          name:data
        },
         include:[
             {
                 relation:"fileStorages",
                 scope:{
                     fields:{
                           linkFile:true,
                         productId:true,
                         id:true,
                             }
                       }
             },
               {
                 relation:"prices",
                 scope:{
                     fields:{
                             },
                       }
             },
              {
                 relation:"category",
                 scope:{
                     fields:{
                             },
                       }
             }
         ],
  };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpointWhitToken(`products`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}
