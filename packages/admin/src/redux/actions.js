import {
  SPIN_TRUE,
  INITIAL_REQUEST_CATEGORIES_SUCCESS,
  INITIAL_REQUEST_CATEGORIES_CREATE_SUCCESS,
  INITIAL_REQUEST_BRANCH_SUCCESS,
  REGISTER_BRANCH_SUCCESS,
  INITIAL_REQUEST_USERS_SUCCESS,
  INITIAL_REQUEST_SEARCH_CATEGORIES_SUCCESS,
  INITIAL_REQUEST_SEARCH_USERS_SUCCESS,
  USER_FILTER_MAX,
  USER_FILTER_MIN,
  INITIAL_REQUEST_ADS_SUCCESS,
  UPLOAD_FILES_SUCCESS,
  UPLOAD_ADS_SUCCESS,
  CATEGORIES_FILTER_START,
  CATEGORIES_FILTER_SUCCESS,
  INITIAL_REQUEST_PRODUCT_SUCCESS,
  INITIAL_REQUEST_PRODUCT_SUCCESS2,
  USER_FILTER_MAX_PRODUCTS,
  USER_FILTER_MIN_PRODUCTS,
  SEARCH_PRODUCT_SUCCESS,
  CHANGE_PRODUCT_SUCCESS,
  CHANGE_IMAGE_PRODUCT_SUCCESS,
  CHANGE_PRICES_PRODUCT_SUCCESS,
  CREATE_PRODUCT_SUCCESS,
  INITIAL_REQUEST_ORDERS_SUCCESS,
  ORDER_FILTER_MAX,
  ORDER_FILTER_MIN,
  ORDER_FILTER_SEARCH_SUCCESS,
  ORDER_FILTER_STATUS_SUCCESS,
  ORDER_FILTER_STATUS2,
  INITIAL_REQUEST_ORDERS_SECOND_SUCCESS,
  REQUEST_BRANCHES_SUCCESS,
  REGISTER_DETAIL_DELIVERY_SUCCESS,
  UPDATE_DETAIL_DELIVERY_SUCCESS,
  INITIAL_REQUEST_ROLES_SUCCESS,
  INITIAL_REQUEST_SUGERENCY_SUCCESS,
  INITIAL_REQUEST_BALANCE_SUCCESS,
} from './constants';

export const spinTrue = () => ({
  type: SPIN_TRUE,
});
export const initialRequestCategoriesSuccess = (payload) => ({
  type: INITIAL_REQUEST_CATEGORIES_SUCCESS,
  payload,
});
export const initialRequestCategoriesCreateSuccess = (payload) => ({
  type: INITIAL_REQUEST_CATEGORIES_CREATE_SUCCESS,
  payload,
});

export const initialRequestBranchSuccess = (payload) => ({
  type: INITIAL_REQUEST_BRANCH_SUCCESS,
  payload,
});

export const registerBranchSuccess = (payload) => ({
  type: REGISTER_BRANCH_SUCCESS,
  payload,
});

export const initialRequestUsersSuccess = (payload) => ({
  type: INITIAL_REQUEST_USERS_SUCCESS,
  payload,
});

export const initialRequestSearchCategoriesSuccess = (payload) => ({
  type: INITIAL_REQUEST_SEARCH_CATEGORIES_SUCCESS,
  payload,
});

export const initialRequestSearchUsersSuccess = (payload) => ({
  type: INITIAL_REQUEST_SEARCH_USERS_SUCCESS,
  payload,
});

export const userFilterMax = (payload) => ({
  type: USER_FILTER_MAX,
  payload,
});

export const userFilterMin = (payload) => ({
  type: USER_FILTER_MIN,
  payload,
});

export const userFilterMaxProducts = (payload) => ({
  type: USER_FILTER_MAX_PRODUCTS,
  payload,
});

export const userFilterMinProducts = (payload) => ({
  type: USER_FILTER_MIN_PRODUCTS,
  payload,
});

export const initialRequestAdsSuccess = (payload) => ({
  type: INITIAL_REQUEST_ADS_SUCCESS,
  payload,
});

export const uploadAdsSuccess = (payload) => ({
  type: UPLOAD_ADS_SUCCESS,
  payload,
});

export const uploadFilesSuccess = (payload) => ({
  type: UPLOAD_FILES_SUCCESS,
  payload,
});

export const categoriesFilterSuccess = (payload) => ({
  type: CATEGORIES_FILTER_SUCCESS,
  payload,
});

export const initialRequestProductSuccess = (payload) => ({
  type: INITIAL_REQUEST_PRODUCT_SUCCESS,
  payload,
});

export const initialRequestProductSuccess2 = (payload) => ({
  type: INITIAL_REQUEST_PRODUCT_SUCCESS2,
  payload,
});

export const searchProductSuccess = (payload) => ({
  type: SEARCH_PRODUCT_SUCCESS,
  payload,
});

export const changeProduct_success = (payload) => ({
  type: CHANGE_PRODUCT_SUCCESS,
  payload,
});

export const changeImageProduct_success = (payload) => ({
  type: CHANGE_IMAGE_PRODUCT_SUCCESS,
  payload,
});

export const changePricesProduct_success = (payload) => ({
  type: CHANGE_PRICES_PRODUCT_SUCCESS,
  payload,
});

export const createProductsSuccess = (payload) => ({
  type: CREATE_PRODUCT_SUCCESS,
  payload,
});

export const initialRequestOrderSuccess = (payload) => ({
  type: INITIAL_REQUEST_ORDERS_SUCCESS,
  payload,
});

export const orderFilterMax = (payload) => ({
  type: ORDER_FILTER_MAX,
  payload,
});

export const orderFilterMin = (payload) => ({
  type: ORDER_FILTER_MIN,
  payload,
});

export const orderFilterSearchSuccess = (payload) => ({
  type: ORDER_FILTER_SEARCH_SUCCESS,
  payload,
});

export const orderFilterStatusSuccess = (payload) => ({
  type: ORDER_FILTER_STATUS_SUCCESS,
  payload,
});

export const orderFilterStatus2 = (payload) => ({
  type: ORDER_FILTER_STATUS2,
  payload,
});

export const initialRequestOrdersSecondSuccess = (payload) => ({
  type: INITIAL_REQUEST_ORDERS_SECOND_SUCCESS,
  payload,
});

export const requestBranchesSuccess = (payload) => ({
  type: REQUEST_BRANCHES_SUCCESS,
  payload,
});

export const registerDetailDeliveriesSuccess = (payload) => ({
  type: REGISTER_DETAIL_DELIVERY_SUCCESS,
  payload,
});

export const updateDetailDeliveriesSuccess = (payload) => ({
  type: UPDATE_DETAIL_DELIVERY_SUCCESS,
  payload,
});

export const initialRequestRolesSuccess = (payload) => ({
  type: INITIAL_REQUEST_ROLES_SUCCESS,
  payload,
});

export const initialRequestSugerencySuccess = (payload) => ({
  type: INITIAL_REQUEST_SUGERENCY_SUCCESS,
  payload,
});

export const initialRequestBalanceSuccess = (payload) => ({
  type: INITIAL_REQUEST_BALANCE_SUCCESS,
  payload,
});
