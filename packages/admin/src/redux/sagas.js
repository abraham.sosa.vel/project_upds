import { put, takeLatest, call, all, select } from 'redux-saga/effects';
import {
  categoriesFilterSuccess,
  createProductsSuccess,
  initialRequestAdsSuccess,
  initialRequestBalanceSuccess,
  initialRequestBranchSuccess,
  initialRequestCategoriesCreateSuccess,
  initialRequestCategoriesSuccess,
  initialRequestOrdersSecondSuccess,
  initialRequestOrderSuccess,
  initialRequestProductSuccess,
  initialRequestProductSuccess2,
  initialRequestRolesSuccess,
  initialRequestSearchCategoriesSuccess,
  initialRequestSearchUsersSuccess,
  initialRequestSugerencySuccess,
  initialRequestUsersSuccess,
  orderFilterSearchSuccess,
  orderFilterStatusSuccess,
  registerBranchSuccess,
  requestBranchesSuccess,
  spinTrue,
  uploadAdsSuccess,
  uploadFilesSuccess,
} from './actions';
import request, {
  getOptionsWithToken,
  postOptions,
  postOptionsFormData,
  postOptionsWithToken,
  patchOptionsWithToken,
  getOptionsWithoutToken,
  deleteOptions,
} from 'utlis/request';
import {
  CATEGORIES_FILTER_START,
  CHANGE_IMAGE_PRODUCT_START,
  CHANGE_PASSWORD,
  CHANGE_PRICES_PRODUCT_START,
  CHANGE_PRODUCT_START,
  CREATE_IMAGE_PRODUCT_START,
  CREATE_PRICES_PRODUCT_START,
  CREATE_PRODUCT_START,
  DELETE_REQUEST_ROLES_START,
  DELETE_REQUEST_SUGERENCY,
  INITIAL_REQUEST_ADS_START,
  INITIAL_REQUEST_BALANCE_START,
  INITIAL_REQUEST_BRANCH_START,
  INITIAL_REQUEST_CATEGORIES_CREATE_START,
  INITIAL_REQUEST_CATEGORIES_START,
  INITIAL_REQUEST_ORDERS_SECOND_START,
  INITIAL_REQUEST_ORDERS_START,
  INITIAL_REQUEST_PRODUCT_START,
  INITIAL_REQUEST_PRODUCT_START2,
  INITIAL_REQUEST_ROLES_START,
  INITIAL_REQUEST_SEARCH_CATEGORIES_START,
  INITIAL_REQUEST_SEARCH_USERS_START,
  INITIAL_REQUEST_SUGERENCY_START,
  INITIAL_REQUEST_USERS_START,
  LOGIN_ADMIN_START,
  ORDER_FILTER_SEARCH_START,
  ORDER_FILTER_STATUS_START,
  REGISTER_BRANCH_START,
  REGISTER_DETAIL_DELIVERY_START,
  REGISTER_REQUEST_ROLES_START,
  REQUEST_BRANCHES_START,
  SEARCH_PRODUCT_START,
  UPDATE_DETAIL_DELIVERY_START,
  UPDATE_REQUEST_ROLES_START,
  UPLOAD_ADS_START,
  UPLOAD_FILES_START,
} from './constants';
import { Categories } from 'utlis/services/categories';
import { Users } from 'utlis/services/users';
import { Advertisements } from 'utlis/services/advertisements';
import { Products } from 'utlis/services/product';
import { Orders } from 'utlis/services/orders';
import { date } from 'yup';
export function* initialRequestCategoriesSaga({ payload }) {
  const objectServices = new Categories();
  try {
    //yield put(showLoader());
    const filterTags = yield objectServices.getCategories(payload);
    filterTags.map((e) => {
      if (e.typeCategoryId == '1e11c24e-e829-4a9e-ad46-28a9945f89b1') {
        e.type = 'Mano de obra';
        delete e.typeCategoryId;
        return e;
      }
      if (e.typeCategoryId == '6c441d0f-9662-412d-9bb0-447f74873546') {
        e.type = 'Materiales de construccion';
        delete e.typeCategoryId;
        return e;
      }
      if (e.typeCategoryId == 'e07367c7-fdac-4347-99ae-75d32b5c9499') {
        e.type = 'Herramientas';
        delete e.typeCategoryId;
        return e;
      }
    });
    yield all([put(initialRequestCategoriesSuccess(filterTags))]);
  } catch (err) {
    // yield put(hideLoader());
    //yield showMessageError(err);
    console.log('error');
  }
}
export function* initialRequestCategoriesCreateSaga({
  payload: { resolve, reject, ...payload },
}) {
  const { categories } = yield select((state) => state);
  try {
    const url = process.env.REACT_APP_URL_API + '/categories';
    const option = postOptionsWithToken(payload);
    const registerUser = yield call(request, url, option);
    yield all([
      put(initialRequestCategoriesCreateSuccess([...categories, registerUser])),
    ]);
    yield call(resolve, 'Se registro con exito');
  } catch (e) {
    yield call(reject, 'La contraseña es incorrecta');
  }
}

export function* initialRequestBranchSaga() {
  const token = localStorage.getItem('pickbazar_token');
  try {
    const url = process.env.REACT_APP_URL_API + `/branches`;
    let option = getOptionsWithToken(token);
    let registerPayment = yield call(request, url, option);
    console.log(registerPayment);
    yield all([put(initialRequestBranchSuccess(registerPayment))]);
    console.log('Se obtubo con exito');
  } catch (e) {
    console.log('Se obtubo sin exito');
  }
}
export function* loginAmdimSaga({ payload: { resolve, reject, ...payload } }) {
  try {
    const url = process.env.REACT_APP_URL_API + `/login`;
    let option = postOptions({
      email: payload.username,
      password: payload.password,
    });
    let registerPayment = yield call(request, url, option);
    yield call(resolve, registerPayment);
  } catch (e) {
    yield call(reject, 'La contraseña es incorrecta');
  }
}
export function* registerBranchSaga({
  payload: { resolve, reject, ...payload },
}) {
  const { coupons } = yield select((state) => state);
  try {
    const url = process.env.REACT_APP_URL_API + `/branches`;
    let option = postOptionsWithToken(payload);
    let registerPayment = yield call(request, url, option);
    yield all([put(registerBranchSuccess([...coupons, registerPayment]))]);
    yield call(resolve, 'Se agrego con extio la sucursal');
  } catch (e) {
    yield call(reject, 'No se pudo agregar la sucursal');
  }
}

export function* initialRequestUsersSaga() {
  const userServices = new Users();
  try {
    const filterUsers = yield userServices.getUsers();
    let cat = [];
    cat = filterUsers.filter((e) => e.payments !== undefined);
    cat.map((e) => {
      e.payments.sort((a, b) => {
        if (a.createDate < b.createDate) return 1;
        if (a.createDate > b.createDate) return -1;
      });
      e.Pedidos = e.payments.length;
      e.fecha = e.payments[0].createDate.substr(0, 10);
      e.name = `${e.firstName} ${e.lastName}`;
      delete e.email;
      delete e.firstName;
      delete e.userName;
      delete e.lastName;
      delete e.gps;
      delete e.id;
      delete e.password;
      delete e.payments;
      delete e.roleId;
    });
    yield all([put(initialRequestUsersSuccess(cat))]);
    console.log('Se obtubo con exito');
  } catch (e) {
    console.log('Se obtubo sin exito');
  }
}

export function* initialRequestSearchCategoriesSaga({ payload }) {
  const objectServices = new Categories();
  try {
    const filterTags = yield objectServices.getCategoriesSearch(payload);
    filterTags.map((e) => {
      if (e.typeCategoryId == '1e11c24e-e829-4a9e-ad46-28a9945f89b1') {
        e.type = 'Mano de obra';
        delete e.typeCategoryId;
        return e;
      }
      if (e.typeCategoryId == '6c441d0f-9662-412d-9bb0-447f74873546') {
        e.type = 'Materiales de construccion';
        delete e.typeCategoryId;
        return e;
      }
      if (e.typeCategoryId == 'e07367c7-fdac-4347-99ae-75d32b5c9499') {
        e.type = 'Herramientas';
        delete e.typeCategoryId;
        return e;
      }
    });
    yield all([put(initialRequestSearchCategoriesSuccess(filterTags))]);
  } catch (err) {
    // yield put(hideLoader());
    //yield showMessageError(err);
    console.log('error');
  }
}

export function* initialRequestSearchUsersSaga({ payload }) {
  const userServices = new Users();
  try {
    const filterUsers = yield userServices.getUsersSearch(payload);
    console.log.og(filterUsers);
    let cat = [];
    cat = filterUsers.filter((e) => e.payments !== undefined);
    cat.map((e) => {
      e.Pedidos = e.payments.length;
      e.fecha = e.payments[e.payments.length - 1].createDate;
      e.name = `${e.firstName} ${e.lastName}`;
      delete e.email;
      delete e.firstName;
      delete e.userName;
      delete e.lastName;
      delete e.gps;
      delete e.id;
      delete e.password;
      delete e.payments;
      delete e.roleId;
    });
    yield all([put(initialRequestSearchUsersSuccess(cat))]);
    console.log('Se obtubo con exito');
  } catch (e) {
    console.log('Se obtubo sin exito');
  }
}

export function* initialRequestAdsSagas() {
  const AdsSaga = new Advertisements();
  try {
    yield put(spinTrue());
    const filterAds = yield AdsSaga.getAds();
    let data = {
      items: [],
      hasMore: {},
    };
    filterAds.map((e) => {
      e.image = e.fileStorages[0].linkFile;
    });
    data.items = filterAds;
    yield all([put(initialRequestAdsSuccess(data))]);
    console.log('Se obtubo con exito');
  } catch (error) {
    console.log('error');
  }
}
export const buildFormData = (data) => {
  const formData = new FormData();
  formData.append('file', data, data.name);
  return formData;
};
export function* uploadFilesSaga({ payload: { resolve, reject, ...payload } }) {
  try {
    yield put(spinTrue());
    const url = process.env.REACT_APP_URL_API + '/containers/ads/upload';
    const formData = buildFormData(payload.image.image[0]);
    const option = postOptionsFormData(formData);
    let requestUpload = yield call(request, url, option);

    const url2 =
      process.env.REACT_APP_URL_API + `/file-storages/${payload.datos.id}`;
    const option2 = patchOptionsWithToken({
      id: payload.datos.id,
      format: '.png',
      size: requestUpload.size,
      originalName: requestUpload.originalName,
      linkFile: requestUpload.mediaLink,
      adId: payload.datos.adId,
    });
    let registerPayment = yield call(request, url2, option2);
    console.log(registerPayment);
    yield all([put({ type: INITIAL_REQUEST_ADS_START })]);
    yield all([put(uploadFilesSuccess(requestUpload))]);
    yield call(resolve, 'Se registro con exito');
  } catch (error) {
    yield call(reject, 'La contraseña es incorrecta');
  }
}
export function* uploadAdsSagas({ payload: { resolve, reject, ...payload } }) {
  try {
    const url = process.env.REACT_APP_URL_API + `/ads/${payload.id}`;
    let option = patchOptionsWithToken(payload);
    let registerPayment = yield call(request, url, option);
    yield all([put({ type: INITIAL_REQUEST_ADS_START })]);
    yield call(resolve, 'Se registro con exito');
  } catch (e) {
    yield call(reject, 'La contraseña es incorrecta');
  }
}

export function* categoriesFilterSaga({ payload }) {
  const categoriesServices = new Categories();
  try {
    const filterTags = yield categoriesServices.getCategoriesSearchData(
      payload
    );
    filterTags.map((e) => {
      e.value = e.id;
      e.name = e.title;
    });
    yield all([put(categoriesFilterSuccess(filterTags))]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}
export function* initialRequestProductSaga({ payload }) {
  const productsServices = new Products();
  let cat = {
    items: [],
    hasMore: {},
  };
  try {
    const filterProducts = yield productsServices.getProducts();
    console.log(filterProducts);
    filterProducts.map((e) => {
      e.subcat = e.category?.typeCategoryId;
      e.categoryId = e.category?.id;
      e.image = e.fileStorages[0]?.linkFile;
      e.price = e.prices[0]?.price;
    });
    console.log(filterProducts);
    cat.items = filterProducts;
    yield all([put(initialRequestProductSuccess(cat))]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* initialRequestProductSaga2({ payload }) {
  const productsServices = new Products();
  let cat = {
    items: [],
    hasMore: {},
  };
  try {
    const filterProducts = yield productsServices.getProducts();
    filterProducts.map((e) => {
      e.subcat = e.category?.typeCategoryId;
      e.categoryId = e.category?.id;
      e.image = e.fileStorages[0]?.linkFile;
      e.price = e.prices[0]?.price;
    });
    cat.items = filterProducts.filter((e) => e.subcat == payload);
    yield all([put(initialRequestProductSuccess2(cat))]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}
export function* searchProductSaga({ payload }) {
  const productsServices = new Products();
  let cat = {
    items: [],
    hasMore: {},
  };
  try {
    const filterProducts = yield productsServices.getProductsSearch(payload);
    filterProducts.map((e) => {
      e.subcat = e.category?.typeCategoryId;
      e.categoryId = e.category?.id;
      e.image = e.fileStorages[0]?.linkFile;
      e.price = e.prices[0]?.price;
    });
    cat.items = filterProducts;
    yield all([put(initialRequestProductSuccess2(cat))]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* changeProductsSaga({
  payload: { resolve, reject, ...payload },
}) {
  console.log(payload);
  try {
    const url = process.env.REACT_APP_URL_API + `/products/${payload.id}`;
    let option = patchOptionsWithToken(payload);
    let registerPayment = yield call(request, url, option);
    yield all([put({ type: INITIAL_REQUEST_PRODUCT_START })]);
    yield call(resolve, 'Se registro con exito');
  } catch (e) {
    yield call(reject, 'La contraseña es incorrecta');
  }
}

export function* changePricesSaga({ payload }) {
  try {
    const url = process.env.REACT_APP_URL_API + `/prices/${payload.id}`;
    let option = patchOptionsWithToken(payload);
    let registerPayment = yield call(request, url, option);
    console.log(registerPayment);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* changeImageProductSaga({
  payload: { resolve, reject, ...payload },
}) {
  try {
    yield put(spinTrue());
    const url = process.env.REACT_APP_URL_API + '/containers/products/upload';
    const formData = buildFormData(payload.image.image[0]);
    const option = postOptionsFormData(formData);
    let requestUpload = yield call(request, url, option);

    const url2 =
      process.env.REACT_APP_URL_API + `/file-storages/${payload.datos.id}`;
    const option2 = patchOptionsWithToken({
      id: payload.datos.id,
      format: '.png',
      size: requestUpload.size,
      originalName: requestUpload.originalName,
      linkFile: requestUpload.mediaLink,
      productId: payload.datos.productId,
    });
    let registerPayment = yield call(request, url2, option2);
    console.log(registerPayment);
    yield all([put({ type: INITIAL_REQUEST_PRODUCT_START })]);
    yield call(resolve, 'Se registro con exito');
  } catch (error) {
    yield call(reject, 'La contraseña es incorrecta');
  }
}

export function* createProductsSaga({
  payload: { resolve, reject, ...payload },
}) {
  try {
    const url = process.env.REACT_APP_URL_API + `/products`;
    let option = postOptionsWithToken(payload);
    let registerPayment = yield call(request, url, option);
    yield all([put(createProductsSuccess(registerPayment))]);
    yield call(resolve, 'Se registro con exito');
  } catch (e) {
    yield call(reject, 'La contraseña es incorrecta');
  }
}

export function* createPricesSaga({ payload }) {
  const { productCr } = yield select((state) => state);
  payload.productId = productCr.id;
  try {
    const url = process.env.REACT_APP_URL_API + `/prices`;
    let option = postOptionsWithToken(payload);
    let registerPayment = yield call(request, url, option);
    console.log(registerPayment);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* createImageProductSaga({
  payload: { resolve, reject, ...payload },
}) {
  console.log('hola');
  const { productCr } = yield select((state) => state);
  try {
    yield put(spinTrue());
    const url = process.env.REACT_APP_URL_API + '/containers/products/upload';
    const formData = buildFormData(payload.image.image[0]);
    const option = postOptionsFormData(formData);
    let requestUpload = yield call(request, url, option);
    console.log(requestUpload);
    const url2 = process.env.REACT_APP_URL_API + `/file-storages`;
    const option2 = postOptionsWithToken({
      format: '.png',
      size: requestUpload.size,
      originalName: 'Envio de imagen',
      linkFile: requestUpload.mediaLink,
      productId: productCr.id,
    });
    let registerPayment = yield call(request, url2, option2);
    console.log(registerPayment);
    yield all([put({ type: INITIAL_REQUEST_PRODUCT_START })]);
    yield call(resolve, 'Se registro con exito');
  } catch (error) {
    yield call(reject, 'La contraseña es incorrecta');
  }
}

export function* initialOrdersSaga() {
  const orderServices = new Orders();
  try {
    const filterOrders = yield orderServices.getOrders();
    filterOrders.map((e, i) => {
      e.phone = e.payment.user.celphone;
      e.pedido = e.payment.createDate.substr(0, 10);
      e.entrega = e.dateDelivery.substr(0, 10);
      if (e.detailDeliveries == undefined) {
        e.state = 'Sin revisar';
      } else {
        e.state = e.detailDeliveries[0].statusDelivery.type;
      }
      delete e.dateDelivery;
      delete e.detailDeliveries;
      delete e.hoursDelivery;
      delete e.payment;
      delete e.paymentId;
    });
    console.log(filterOrders);
    yield all([put(initialRequestOrderSuccess(filterOrders))]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* orderFilterStatusSaga({ payload }) {
  const orderServices = new Orders();
  try {
    const filterOrders = yield orderServices.getOrders();
    filterOrders.map((e, i) => {
      e.phone = e.payment.user.celphone;
      e.pedido = e.payment.createDate.substr(0, 10);
      e.entrega = e.dateDelivery.substr(0, 10);
      if (e.detailDeliveries == undefined) {
        e.state = 'Sin revisar';
      } else {
        e.state = e.detailDeliveries[0].statusDelivery.type;
      }
      delete e.dateDelivery;
      delete e.detailDeliveries;
      delete e.hoursDelivery;
      delete e.payment;
      delete e.paymentId;
    });
    console.log(filterOrders);
    let cat = filterOrders.filter((e) => e.state == payload);
    console.log(cat);
    yield all([put(orderFilterStatusSuccess(cat))]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* orderFilterSearchStart({ payload }) {
  const orderServices = new Orders();
  try {
    const filterOrders = yield orderServices.getOrders();
    filterOrders.map((e, i) => {
      e.phone = e.payment.user.celphone;
      e.pedido = e.payment.createDate.substr(0, 10);
      e.entrega = e.dateDelivery.substr(0, 10);
      if (e.detailDeliveries == undefined) {
        e.state = 'Sin revisar';
      } else {
        e.state = e.detailDeliveries[0].statusDelivery.type;
      }
      delete e.dateDelivery;
      delete e.detailDeliveries;
      delete e.hoursDelivery;
      delete e.payment;
      delete e.paymentId;
    });
    let cat = filterOrders.filter((e) => e.phone == payload);
    yield all([put(orderFilterSearchSuccess(cat))]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* initialRequestOrdersSecondSaga({ payload }) {
  const orderServices = new Orders();
  try {
    const filterOrders = yield orderServices.getOrdersFilter(payload);
    yield all([put(initialRequestOrdersSecondSuccess(filterOrders))]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* requestBranchesSaga() {
  try {
    const url = process.env.REACT_APP_URL_API + `/branches`;
    let option = getOptionsWithToken();
    let registerPayment = yield call(request, url, option);
    registerPayment.map((e) => {
      e.value = e.id;
      delete e.direction;
      delete e.celphone;
    });
    yield all([put(requestBranchesSuccess(registerPayment))]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* registerDetailDeliverySaga({ payload }) {
  try {
    const url = process.env.REACT_APP_URL_API + `/detail-deliveries`;
    let option = postOptionsWithToken(payload);
    let registerPayment = yield call(request, url, option);
    yield all([put({ type: INITIAL_REQUEST_ORDERS_START })]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}
export function* updateDetailDeliverySaga({ payload }) {
  try {
    const url =
      process.env.REACT_APP_URL_API + `/detail-deliveries/${payload.id}`;
    let option = patchOptionsWithToken(payload);
    let registerPayment = yield call(request, url, option);
    yield all([put({ type: INITIAL_REQUEST_ORDERS_START })]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* changePasswordSaga({ payload }) {
  console.log(payload);
  try {
    const url = process.env.REACT_APP_URL_API + `/users/change-password`;
    let option = postOptionsWithToken(payload);
    let registerPayment = yield call(request, url, option);
    console.log(registerPayment);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* initialRequestRolesSaga() {
  try {
    const url = process.env.REACT_APP_URL_API + `/users`;
    let option = getOptionsWithToken();
    let registerPayment = yield call(request, url, option);
    let cat = [];
    cat = registerPayment.filter(
      (e) => e.roleId == 'fe571a2f-e540-4564-91cc-39c061e53828'
    );
    cat.map((e) => {
      delete e.direction;
      e.name = `${e.firstName} ${e.lastName}`;
      e.first = e.firstName;
      e.second = e.lastName;
      delete e.firstName;
      delete e.lastName;
      delete e.roleId;
    });
    console.log(cat);
    yield all([put(initialRequestRolesSuccess(cat))]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}
export function* registerRequestRolesSaga({ payload }) {
  try {
    const url = process.env.REACT_APP_URL_API + `/users/${payload.id}`;
    let option = patchOptionsWithToken(payload);
    let registerPayment = yield call(request, url, option);
    yield all([put({ type: INITIAL_REQUEST_ROLES_START })]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* updateRequestRolesSaga({ payload }) {
  try {
    const url = process.env.REACT_APP_URL_API + `/users`;
    let option = postOptionsWithToken(payload);
    let registerPayment = yield call(request, url, option);
    yield all([put({ type: INITIAL_REQUEST_ROLES_START })]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* deleteRequestRolesSaga({ payload }) {
  try {
    const url = process.env.REACT_APP_URL_API + `/users/${payload}`;
    let option = deleteOptions(payload);
    let registerPayment = yield call(request, url, option);
    yield all([put({ type: INITIAL_REQUEST_ROLES_START })]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* initialRequestSugerencySaga() {
  try {
    const url = process.env.REACT_APP_URL_API + `/account-banks`;
    let option = getOptionsWithToken();
    let registerPayment = yield call(request, url, option);

    registerPayment.map((e) => {
      delete e.typeAccount;
      delete e.nameAccount;
      e.fecha = e.numberAccount.substr(0, 10);
      delete e.numberAccount;
    });
    yield all([put(initialRequestSugerencySuccess(registerPayment))]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* deleteRequestSugerencySaga({ payload }) {
  try {
    const url = process.env.REACT_APP_URL_API + `/account-banks/${payload}`;
    let option = deleteOptions(payload);
    let registerPayment = yield call(request, url, option);
    yield all([put({ type: INITIAL_REQUEST_SUGERENCY_START })]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* initialRequestBalanceSaga() {
  let totalr = 0;
  try {
    const url = process.env.REACT_APP_URL_API + `/detail-payments`;
    let option = getOptionsWithToken();
    let registerPayment = yield call(request, url, option);
    registerPayment.map((e) => {
      totalr += e.total;
      delete e.priceDelivery;
      delete e.hoursDelivery;
      delete e.paymentId;
      e.hour = e.dateDelivery.substr(0, 10);
      delete e.dateDelivery;
      e.suma = totalr;
    });
    console.log(registerPayment);
    console.log(totalr);
    yield all([put(initialRequestBalanceSuccess(registerPayment))]);
    console.log('exito');
  } catch (e) {
    console.log('fracaso');
  }
}
export function* sagaLogin() {
  yield takeLatest(
    INITIAL_REQUEST_CATEGORIES_START,
    initialRequestCategoriesSaga
  );
  yield takeLatest(
    INITIAL_REQUEST_CATEGORIES_CREATE_START,
    initialRequestCategoriesCreateSaga
  );
  yield takeLatest(INITIAL_REQUEST_BRANCH_START, initialRequestBranchSaga);
  yield takeLatest(LOGIN_ADMIN_START, loginAmdimSaga);
  yield takeLatest(REGISTER_BRANCH_START, registerBranchSaga);
  yield takeLatest(INITIAL_REQUEST_USERS_START, initialRequestUsersSaga);
  yield takeLatest(
    INITIAL_REQUEST_SEARCH_CATEGORIES_START,
    initialRequestSearchCategoriesSaga
  );
  yield takeLatest(
    INITIAL_REQUEST_SEARCH_USERS_START,
    initialRequestSearchUsersSaga
  );
  yield takeLatest(INITIAL_REQUEST_ADS_START, initialRequestAdsSagas);
  yield takeLatest(UPLOAD_FILES_START, uploadFilesSaga);
  yield takeLatest(UPLOAD_ADS_START, uploadAdsSagas);
  yield takeLatest(CATEGORIES_FILTER_START, categoriesFilterSaga);
  yield takeLatest(INITIAL_REQUEST_PRODUCT_START, initialRequestProductSaga);
  yield takeLatest(INITIAL_REQUEST_PRODUCT_START2, initialRequestProductSaga2);
  yield takeLatest(SEARCH_PRODUCT_START, searchProductSaga);
  yield takeLatest(CHANGE_PRODUCT_START, changeProductsSaga);
  yield takeLatest(CHANGE_PRICES_PRODUCT_START, changePricesSaga);
  yield takeLatest(CHANGE_IMAGE_PRODUCT_START, changeImageProductSaga);

  yield takeLatest(CREATE_PRODUCT_START, createProductsSaga);
  yield takeLatest(CREATE_PRICES_PRODUCT_START, createPricesSaga);
  yield takeLatest(CREATE_IMAGE_PRODUCT_START, createImageProductSaga);
  yield takeLatest(INITIAL_REQUEST_ORDERS_START, initialOrdersSaga);

  yield takeLatest(ORDER_FILTER_SEARCH_START, orderFilterSearchStart);

  yield takeLatest(ORDER_FILTER_STATUS_START, orderFilterStatusSaga);
  yield takeLatest(
    INITIAL_REQUEST_ORDERS_SECOND_START,
    initialRequestOrdersSecondSaga
  );
  yield takeLatest(REQUEST_BRANCHES_START, requestBranchesSaga);

  yield takeLatest(REGISTER_DETAIL_DELIVERY_START, registerDetailDeliverySaga);
  yield takeLatest(UPDATE_DETAIL_DELIVERY_START, updateDetailDeliverySaga);

  yield takeLatest(CHANGE_PASSWORD, changePasswordSaga);
  yield takeLatest(INITIAL_REQUEST_ROLES_START, initialRequestRolesSaga);
  yield takeLatest(REGISTER_REQUEST_ROLES_START, registerRequestRolesSaga);
  yield takeLatest(UPDATE_REQUEST_ROLES_START, updateRequestRolesSaga);

  yield takeLatest(DELETE_REQUEST_ROLES_START, deleteRequestRolesSaga);

  yield takeLatest(
    INITIAL_REQUEST_SUGERENCY_START,
    initialRequestSugerencySaga
  );
  yield takeLatest(DELETE_REQUEST_SUGERENCY, deleteRequestSugerencySaga);
  yield takeLatest(INITIAL_REQUEST_BALANCE_START, initialRequestBalanceSaga);
}
