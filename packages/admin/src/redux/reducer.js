import {
  CATEGORIES_FILTER_SUCCESS,
  CREATE_PRODUCT_SUCCESS,
  INITIAL_REQUEST_ADS_SUCCESS,
  INITIAL_REQUEST_BALANCE_SUCCESS,
  INITIAL_REQUEST_BRANCH_SUCCESS,
  INITIAL_REQUEST_CATEGORIES_CREATE_SUCCESS,
  INITIAL_REQUEST_CATEGORIES_SUCCESS,
  INITIAL_REQUEST_ORDERS_SECOND_SUCCESS,
  INITIAL_REQUEST_ORDERS_SUCCESS,
  INITIAL_REQUEST_PRODUCT_SUCCESS,
  INITIAL_REQUEST_PRODUCT_SUCCESS2,
  INITIAL_REQUEST_ROLES_SUCCESS,
  INITIAL_REQUEST_SEARCH_CATEGORIES_SUCCESS,
  INITIAL_REQUEST_SEARCH_USERS_SUCCESS,
  INITIAL_REQUEST_SUGERENCY_SUCCESS,
  INITIAL_REQUEST_USERS_SUCCESS,
  ORDER_FILTER_MAX,
  ORDER_FILTER_MIN,
  ORDER_FILTER_SEARCH_SUCCESS,
  ORDER_FILTER_STATUS2,
  ORDER_FILTER_STATUS_SUCCESS,
  REGISTER_BRANCH_SUCCESS,
  REQUEST_BRANCHES_SUCCESS,
  SEARCH_PRODUCT_SUCCESS,
  SPIN_TRUE,
  UPLOAD_ADS_SUCCESS,
  UPLOAD_FILES_SUCCESS,
  USER_FILTER_MAX,
  USER_FILTER_MAX_PRODUCTS,
  USER_FILTER_MIN,
  USER_FILTER_MIN_PRODUCTS,
} from './constants';

const initialState = {
  spin: true,
  categories: [],
  coupons: [],
  customers: [],
  ads: [],
  files: [],
  options: [],
  products: [],
  productCr: [],
  orders: [],
  orderDetail: [],
  branch: [],
  roles: [],
  sugerency: [],
  balance: [],
};

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case SPIN_TRUE:
      return { ...state, spin: true };
    case INITIAL_REQUEST_CATEGORIES_SUCCESS:
      return { ...state, categories: action.payload };
    case INITIAL_REQUEST_CATEGORIES_CREATE_SUCCESS:
      return { ...state, categories: action.payload };
    case INITIAL_REQUEST_BRANCH_SUCCESS:
      return { ...state, coupons: action.payload };
    case REGISTER_BRANCH_SUCCESS:
      return { ...state, coupons: action.payload };
    case INITIAL_REQUEST_USERS_SUCCESS:
      return { ...state, customers: action.payload };
    case INITIAL_REQUEST_SEARCH_CATEGORIES_SUCCESS:
      return { ...state, categories: action.payload };
    case INITIAL_REQUEST_SEARCH_USERS_SUCCESS:
      return { ...state, customers: action.payload };
    case USER_FILTER_MIN:
      return { ...state, customers: action.payload };
    case USER_FILTER_MAX:
      return { ...state, customers: action.payload };
    case INITIAL_REQUEST_ADS_SUCCESS:
      return { ...state, ads: action.payload };
    case UPLOAD_FILES_SUCCESS:
      return { ...state, files: action.payload };
    case UPLOAD_ADS_SUCCESS:
      return { ...state, ads: action.payload };
    case CATEGORIES_FILTER_SUCCESS:
      return { ...state, options: action.payload };
    case INITIAL_REQUEST_PRODUCT_SUCCESS:
      return { ...state, products: action.payload };
    case INITIAL_REQUEST_PRODUCT_SUCCESS2:
      return { ...state, products: action.payload };
    case USER_FILTER_MIN_PRODUCTS:
      return { ...state, products: action.payload };
    case USER_FILTER_MAX_PRODUCTS:
      return { ...state, products: action.payload };
    case SEARCH_PRODUCT_SUCCESS:
      return { ...state, products: action.payload };
    case CREATE_PRODUCT_SUCCESS:
      return { ...state, productCr: action.payload };
    case INITIAL_REQUEST_ORDERS_SUCCESS:
      return { ...state, orders: action.payload };
    case ORDER_FILTER_MAX:
      return { ...state, orders: action.payload };
    case ORDER_FILTER_MIN:
      return { ...state, orders: action.payload };
    case ORDER_FILTER_STATUS2:
      return { ...state, orders: action.payload };
    case ORDER_FILTER_SEARCH_SUCCESS:
      return { ...state, orders: action.payload };
    case ORDER_FILTER_STATUS_SUCCESS:
      return { ...state, orders: action.payload };
    case INITIAL_REQUEST_ORDERS_SECOND_SUCCESS:
      return { ...state, orderDetail: action.payload };
    case REQUEST_BRANCHES_SUCCESS:
      return { ...state, branch: action.payload };
    case INITIAL_REQUEST_ROLES_SUCCESS:
      return { ...state, roles: action.payload };
    case INITIAL_REQUEST_SUGERENCY_SUCCESS:
      return { ...state, sugerency: action.payload };
    case INITIAL_REQUEST_BALANCE_SUCCESS:
      return { ...state, balance: action.payload };
    default:
      return state;
  }
};

export default profileReducer;
