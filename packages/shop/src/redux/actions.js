import {
  SPIN_TRUE,
  INITIAL_REQUEST_CATEGORIES_SUCCESS,
  INITIAL_REQUEST_PRODUCTS_SUCCESS,
  INITIAL_REQUEST_PRODUCTS_SEARCH_SUCCESS,
  INITIAL_REQUEST_ADS_SUCCESS,
  REGISTER_USER_SUCCESS,
  UPDATE_USER_SUCCESS,
  DELETE_USER_SUCCESS,
  INSERT_DATA_DAY,
  INSERT_DATA_HOUR,
  REGISTER_PAYMENT_SUCCESS,
  LOGIN_USER_SUCCESS,
  COUNT_PRODUCTS_START,
  REGISTER_PAYMENT_DETAILS_SUCCESS,
  REGISTER_PAYMENT_ORDERS_SUCCESS,
  ERROR_LOGIN_SUCCESS,
  SEARCH_USERS_SUCCESS,
  SEARCH_PRODUCTS_USERS_SUCCESS,
  SEARCH_PRODUCTS_USERS_SUCCESS2,
} from './constants';

export const spinTrue = () => ({
  type: SPIN_TRUE,
});
export const initialRequestCategoriesSuccess = (payload) => ({
  type: INITIAL_REQUEST_CATEGORIES_SUCCESS,
  payload,
});
export const initialRequestProductsSuccess = (payload) => ({
  type: INITIAL_REQUEST_PRODUCTS_SUCCESS,
  payload,
});
export const initialRequestProductsSearchSuccess = (payload) => ({
  type: INITIAL_REQUEST_PRODUCTS_SEARCH_SUCCESS,
  payload,
});
export const initialRequestAdsSuccess = (payload) => ({
  type: INITIAL_REQUEST_ADS_SUCCESS,
  payload,
});
export const registerUserSuccess = (payload) => ({
  type: REGISTER_USER_SUCCESS,
  payload,
});

export const UpdateUserSuccess = (payload) => ({
  type: UPDATE_USER_SUCCESS,
  payload,
});

export const insertDataDay = (payload) => ({
  type: INSERT_DATA_DAY,
  payload,
});

export const insertDataHour = (payload) => ({
  type: INSERT_DATA_HOUR,
  payload,
});

export const registerPaymentSUCCESS = (payload) => ({
  type: REGISTER_PAYMENT_SUCCESS,
  payload,
});

export const registerPaymentDetailsSUCCESS = (payload) => ({
  type: REGISTER_PAYMENT_DETAILS_SUCCESS,
  payload,
});

export const registerPaymentOrdersSuccess = (payload) => ({
  type: REGISTER_PAYMENT_ORDERS_SUCCESS,
  payload,
});

export const loginUserSuccess = (payload) => ({
  type: LOGIN_USER_SUCCESS,
  payload,
});

export const countProductsStart = (payload) => ({
  type: COUNT_PRODUCTS_START,
  payload,
});

export const errorLoginSuccess = (payload) => ({
  type: ERROR_LOGIN_SUCCESS,
  payload,
});

export const searchUsersSuccess = (payload) => ({
  type: SEARCH_USERS_SUCCESS,
  payload,
});

export const searchProductsUsersSuccess = (payload) => ({
  type: SEARCH_PRODUCTS_USERS_SUCCESS,
  payload,
});

export const searchProductsUsersSuccess2 = (payload) => ({
  type: SEARCH_PRODUCTS_USERS_SUCCESS2,
  payload,
});
