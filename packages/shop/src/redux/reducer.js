import {
  COUNT_PRODUCTS_START,
  DELETE_USER_SUCCESS,
  ERROR_LOGIN_SUCCESS,
  INITIAL_REQUEST_ADS_SUCCESS,
  INITIAL_REQUEST_CATEGORIES_SUCCESS,
  INITIAL_REQUEST_PRODUCTS_SUCCESS,
  INSERT_DATA_DAY,
  INSERT_DATA_HOUR,
  LOGIN_USER_SUCCESS,
  REGISTER_PAYMENT_DETAILS_SUCCESS,
  REGISTER_PAYMENT_SUCCESS,
  REGISTER_USER_SUCCESS,
  SEARCH_USERS_SUCCESS,
  SPIN_TRUE,
  UPDATE_USER_SUCCESS,
  SEARCH_PRODUCTS_USERS_SUCCESS,
  SEARCH_PRODUCTS_USERS_SUCCESS2,
} from './constants';

const initialState = {
  spin: true,
  categories: [],
  products: {},
  ads: [],
  address: [],
  day: '',
  hour: '',
  payment: {},
  paymentDetails: [],
  tokenUser: '',
  count: [],
  orders: [],
};

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case SPIN_TRUE:
      return { ...state, spin: true };
    case INITIAL_REQUEST_CATEGORIES_SUCCESS:
      return { ...state, categories: action.payload };
    case INITIAL_REQUEST_PRODUCTS_SUCCESS:
      return { ...state, products: action.payload };
    case INITIAL_REQUEST_ADS_SUCCESS:
      return { ...state, ads: action.payload };
    case INSERT_DATA_HOUR:
      return { ...state, hour: action.payload };
    case INSERT_DATA_DAY:
      return { ...state, day: action.payload };
    case REGISTER_USER_SUCCESS:
      return { ...state, address: [action.payload] };
    case UPDATE_USER_SUCCESS:
      return { ...state, address: [action.payload] };
    case DELETE_USER_SUCCESS:
      return { ...state, address: [], tokenUser: '' };
    case ERROR_LOGIN_SUCCESS:
      return { ...state, address: [] };
    case SEARCH_USERS_SUCCESS:
      return { ...state, address: [action.payload] };
    case REGISTER_PAYMENT_SUCCESS:
      return { ...state, payment: action.payload };
    case LOGIN_USER_SUCCESS:
      return { ...state, tokenUser: action.payload };
    case COUNT_PRODUCTS_START:
      return { ...state, count: action.payload };
    case REGISTER_PAYMENT_DETAILS_SUCCESS:
      return { ...state, paymentDetails: action.payload };
    case SEARCH_PRODUCTS_USERS_SUCCESS:
      return { ...state, tokenUser: action.payload };
    case SEARCH_PRODUCTS_USERS_SUCCESS2:
      return { ...state, orders: action.payload };
    default:
      return state;
  }
};

export default profileReducer;
