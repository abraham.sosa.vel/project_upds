export const SPIN_TRUE = 'SPIN_TRUE';
export const INITIAL_REQUEST_CATEGORIES_START =
  'INITIAL_REQUEST_CATEGORIES_START';
export const INITIAL_REQUEST_CATEGORIES_SUCCESS =
  'INITIAL_REQUEST_CATEGORIES_SUCCESS';

export const INITIAL_REQUEST_PRODUCTS_START = 'INITIAL_REQUEST_PRODUCTS_START';
export const INITIAL_REQUEST_PRODUCTS_SUCCESS =
  'INITIAL_REQUEST_PRODUCTS_SUCCESS';

export const INITIAL_REQUEST_PRODUCTS_SEARCH_START =
  'INITIAL_REQUEST_PRODUCTS_SEARCH_START';
export const INITIAL_REQUEST_PRODUCTS_SEARCH_SUCCESS =
  'INITIAL_REQUEST_PRODUCTS_SEARCH_SUCCESS';

export const INITIAL_REQUEST_ADS_START = 'INITIAL_REQUEST_ADS_START';
export const INITIAL_REQUEST_ADS_SUCCESS = 'INITIAL_REQUEST_ADS_SUCCESS';

export const REGISTER_USER_START = 'REGISTER_USER_START';
export const REGISTER_USER_SUCCESS = 'REGISTER_USER_SUCCESS';

export const UPDATE_USER_START = 'UPDATE_USER_START';
export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS';

export const DELETE_USER_START = 'DELETE_USER_START';
export const DELETE_USER_SUCCESS = 'DELETE_USER_SUCCESS';

export const LOGIN_USER_START = 'LOGIN_USER_START';
export const LOGIN_USER_SUCCESS = 'LOGIN_USER_SUCCESS';

export const INSERT_DATA_DAY = 'INSERT_DATA_DAY';

export const INSERT_DATA_HOUR = 'INSERT_DATA_HOUR';

export const REGISTER_PAYMENT_START = 'REGISTER_PAYMENT_START';
export const REGISTER_PAYMENT_SUCCESS = 'REGISTER_PAYMENT_SUCCESS';

export const REGISTER_PAYMENT_DETAILS_START = 'REGISTER_PAYMENT_DETAILS_START';
export const REGISTER_PAYMENT_DETAILS_SUCCESS =
  'REGISTER_PAYMENT_DETAILS_SUCCESS';

export const REGISTER_PAYMENT_ORDERS_START = 'REGISTER_PAYMENT_ORDERS_START';
export const REGISTER_PAYMENT_ORDERS_SUCCESS =
  'REGISTER_PAYMENT_ORDERS_SUCCESS';

export const COUNT_PRODUCTS_START = 'COUNT_PRODUCTS_START';
export const ERROR_LOGIN_SUCCESS = 'ERROR_LOGIN_SUCCESS';

export const SEARCH_USERS_START = 'SEARCH_USERS_START';
export const SEARCH_USERS_SUCCESS = 'SEARCH_USERS_SUCCESS';

export const SEARCH_PRODUCTS_USERS_START = 'SEARCH_PRODUCTS_USERS_START';
export const SEARCH_PRODUCTS_USERS_SUCCESS = 'SEARCH_PRODUCTS_USERS_SUCCESS';

export const SEARCH_PRODUCTS_USERS_START2 = 'SEARCH_PRODUCTS_USERS_START2';
export const SEARCH_PRODUCTS_USERS_SUCCESS2 = 'SEARCH_PRODUCTS_USERS_SUCCESS2';

export const REGISTER_USER_SUGERENCY = 'REGISTER_USER_SUGERENCY';
