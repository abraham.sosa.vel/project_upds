import { put, takeLatest, call, all, select } from 'redux-saga/effects';
import request, {
  deleteOptionsWithToken,
  getOptionsWithoutToken,
  patchOptions,
  postOptions,
  postOptionsWithoutToken,
  postOptionsWithToken,
  getOptionsWithTokenSecond,
  postOptions2,
} from 'utils/request';
import { Advertisements } from 'utils/services/advertisements';
import { Categories } from 'utils/services/categories';
import { Products } from 'utils/services/products';
import { Search } from 'utils/services/search';
import {
  deleteUserSuccess,
  initialRequestAdsSuccess,
  initialRequestCategoriesSuccess,
  initialRequestProductsSuccess,
  loginUserSuccess,
  registerPaymentDetailsSUCCESS,
  registerPaymentSUCCESS,
  registerUserSuccess,
  searchProductsUsersSuccess,
  searchProductsUsersSuccess2,
  searchUsersSuccess,
  spinTrue,
  UpdateUserSuccess,
} from './actions';
import {
  DELETE_USER_START,
  DELETE_USER_SUCCESS,
  INITIAL_REQUEST_ADS_START,
  INITIAL_REQUEST_CATEGORIES_START,
  INITIAL_REQUEST_PRODUCTS_SEARCH_START,
  INITIAL_REQUEST_PRODUCTS_START,
  LOGIN_USER_START,
  REGISTER_PAYMENT_DETAILS_START,
  REGISTER_PAYMENT_ORDERS_START,
  REGISTER_PAYMENT_START,
  REGISTER_USER_START,
  REGISTER_USER_SUGERENCY,
  SEARCH_PRODUCTS_USERS_START,
  SEARCH_PRODUCTS_USERS_START2,
  SEARCH_USERS_START,
  UPDATE_USER_START,
} from './constants';
export function* initialRequestCategoriesSaga({ payload }) {
  let dato;
  const objectServices = new Categories();
  const productsServices = new Products();
  try {
    yield put(spinTrue());
    if (payload == 'servicios') {
      dato = '1e11c24e-e829-4a9e-ad46-28a9945f89b1';
    }
    if (payload == 'materialesConstruccion') {
      dato = '6c441d0f-9662-412d-9bb0-447f74873546';
    }
    if (payload == 'herramientas') {
      dato = 'e07367c7-fdac-4347-99ae-75d32b5c9499';
    }
    const filterCategories = yield objectServices.getCategories(dato);
    const filterProducts = yield productsServices.getProducts(dato);
    let cat = {
      items: [],
      hasmore: {},
    };
    cat.items = filterProducts.filter((e) => e.category !== undefined);
    let res;
    cat.items.map(
      (e) => (
        (res = e.prices.filter((e) => e.amountMin === 1)),
        (e.image = e.fileStorages[0].linkFile),
        (e.price = res[0].price),
        (e.title = e.name),
        (e.slug = e.title.replace(' ', '_')),
        (e.gallery = e.fileStorages),
        delete e.name,
        delete e.fileStorages,
        e.gallery.map(
          (e) => ((e.url = e.linkFile), delete e.linkFile, delete e.productId)
        )
      )
    );
    console.log(cat);
    yield all([
      put(initialRequestCategoriesSuccess(filterCategories)),
      put(initialRequestProductsSuccess(cat)),
    ]);
  } catch (e) {
    console.log('error');
  }
}
export function* initialRequestProductsSaga({ payload }) {
  const productServices = new Products();
  try {
    yield put(spinTrue());
    const filterProducts = yield productServices.getProductsFilter(payload);
    let cat = {
      items: [],
      hasmore: {},
    };
    cat.items = filterProducts.filter((e) => e.category !== undefined);
    let res;
    cat.items.map(
      (e) => (
        (res = e.prices.filter((e) => e.amountMin === 1)),
        (e.image = e.fileStorages[0].linkFile),
        (e.price = res[0].price),
        (e.title = e.name),
        (e.slug = e.title.replace(' ', '_')),
        (e.gallery = e.fileStorages),
        delete e.name,
        delete e.fileStorages,
        e.gallery.map(
          (e) => ((e.url = e.linkFile), delete e.linkFile, delete e.productId)
        )
      )
    );
    yield all([put(initialRequestProductsSuccess(cat))]);
  } catch (e) {
    console.log('error');
  }
}
export function* initialRequestProductsSearchSaga({ payload }) {
  let dato;
  const productsServices = new Products();
  try {
    yield put(spinTrue());
    if (payload.type == 'servicios') {
      dato = '1e11c24e-e829-4a9e-ad46-28a9945f89b1';
    }
    if (payload.type == 'materialesConstruccion') {
      dato = '6c441d0f-9662-412d-9bb0-447f74873546';
    }
    if (payload.type == 'herramientas') {
      dato = 'e07367c7-fdac-4347-99ae-75d32b5c9499';
    }
    const filterProducts = yield productsServices.getProductsFilterSearch(
      dato,
      payload.searchTerm
    );
    let cat = {
      items: [],
      hasmore: {},
    };
    cat.items = filterProducts.filter((e) => e.category !== undefined);
    let res;
    cat.items.map(
      (e) => (
        (res = e.prices.filter((e) => e.amountMin === 1)),
        (e.image = e.fileStorages[0].linkFile),
        (e.price = res[0].price),
        (e.title = e.name),
        (e.slug = e.title.replace(' ', '_')),
        (e.gallery = e.fileStorages),
        delete e.name,
        delete e.fileStorages,
        e.gallery.map(
          (e) => ((e.url = e.linkFile), delete e.linkFile, delete e.productId)
        )
      )
    );
    console.log(cat);
    yield all([put(initialRequestProductsSuccess(cat))]);
  } catch (e) {
    console.log('error');
  }
}
export function* initialRequestAdsSagas() {
  const AdsSaga = new Advertisements();
  try {
    yield put(spinTrue());
    const filterAds = yield AdsSaga.getAds();
    let color = [
      '#ff4d4f',
      '#ff7a45',
      '#ffa940',
      '#ff4d4f',
      '#ff7a45',
      '#ffa940',
    ];
    filterAds.map((e, index) => (e, (e.color = color[index])));
    yield all([put(initialRequestAdsSuccess(filterAds))]);
  } catch (error) {
    console.log('error');
  }
}
export function* registerUserSaga({
  payload: { resolve, reject, ...payload },
}) {
  try {
    const url = process.env.NEXT_PUBLIC_URL_API + '/users';
    const option = postOptionsWithoutToken(payload);
    const registerUser = yield call(request, url, option);
    yield all([put(registerUserSuccess(registerUser))]);
    yield call(resolve, 'Se registro con exito');
  } catch (e) {
    yield call(reject, 'No se pudo registrar en la base de datos');
  }
}
export function* updateUserSaga({ payload: { resolve, reject, ...payload } }) {
  const { address } = yield select((state) => state);
  payload.addressValue.roleId = 'd1254697-6d43-451a-876f-7b1ca235e0ae';
  delete payload.addressValue.type;
  try {
    const url =
      process.env.NEXT_PUBLIC_URL_API + `/users/${payload.addressValue.id}`;
    const option = patchOptions(payload.addressValue);
    const registerUser = yield call(request, url, option);
    yield all([put(UpdateUserSuccess(payload.addressValue))]);
    yield call(resolve, 'Se registro con exito');
  } catch (e) {
    yield call(reject, 'No se pudo registrar en la base de datos');
  }
}
export function* deleteUserSaga({ payload }) {
  const { tokenUser } = yield select((state) => state);
  try {
    yield put(spinTrue());
    const url = process.env.NEXT_PUBLIC_URL_API + `/users/${payload}`;
    const option = deleteOptionsWithToken('', 'DELETE', tokenUser.token);
    const deleterRegister = yield call(request, url, option);
    yield all([put({ type: DELETE_USER_SUCCESS })]);
    console.log('Se elimino con exito');
  } catch (e) {
    console.log('Ocurrio un error al eliminar el usuario');
  }
}
export function* registerPaymentsSaga({ payload: { resolve, reject } }) {
  const data = yield select((state) => state);
  try {
    const url = process.env.NEXT_PUBLIC_URL_API + `/payments`;
    const option = postOptionsWithToken(
      {
        createDate: new Date().toISOString(),
        userId: data.address[0].id,
        methodPaymentId: 'a7c2f046-4373-4d1f-8f9f-02056e28d5c8',
        typeStatusPaymentId: 'c700ec5d-384d-445f-9c51-eb5897b24063',
      },
      'POST',
      data.tokenUser.token
    );
    const registerPayment = yield call(request, url, option);
    yield all([put(registerPaymentSUCCESS(registerPayment))]);
    yield call(resolve, 'Se registro con exito');
  } catch (e) {
    yield call(reject, 'No se pudo registrar en la base de datos payment');
  }
}
export function* loginUserSaga({ payload: { resolve, reject } }) {
  const data = yield select((state) => state);
  try {
    const url = process.env.NEXT_PUBLIC_URL_API + `/users/login`;
    const option = postOptions({ celphone: data.address[0].celphone });
    const registerPayment = yield call(request, url, option);
    yield all([put(loginUserSuccess(registerPayment))]);
    yield call(resolve, 'Se registro con exito');
  } catch (e) {
    yield call(reject, 'No se pudo registrar en la base de datos');
  }
}

export function* registerPaymentsDetailsSaga({
  payload: { resolve, reject, ...payload },
}) {
  const data = yield select((state) => state);
  try {
    const url = process.env.NEXT_PUBLIC_URL_API + `/detail-payments`;
    const option = postOptionsWithToken(
      {
        priceDelivery: 0,
        total: parseInt(payload.price),
        dateDelivery: new Date(data.day.replace('/', ' ')).toISOString(),
        hoursDelivery: data.hour,
        paymentId: data.payment.id,
      },
      'POST',
      data.tokenUser.token
    );
    const registerPayment = yield call(request, url, option);
    yield all([put(registerPaymentDetailsSUCCESS(registerPayment))]);
    yield call(resolve, 'Se registro con exito payments-details');
  } catch (e) {
    yield call(
      reject,
      'No se pudo registrar en la base de datos payment-details'
    );
  }
}

export function* registerPaymentsOrdersSaga({
  payload: { resolve, reject, ...payload },
}) {
  const data = yield select((state) => state);
  console.log(data);
  try {
    const url = process.env.NEXT_PUBLIC_URL_API + `/order-details`;
    let option = postOptionsWithToken(
      { detailPaymentId: data.paymentDetails.id, productId: payload.productId },
      'POST',
      data.tokenUser.token
    );
    let registerPayment = yield call(request, url, option);
    yield call(resolve, 'Se registro con exito payments-order');
  } catch (e) {
    yield call(
      reject,
      'No se pudo registrar en la base de datos payment-order'
    );
  }
}

export function* searchUsersSaga({ payload: { resolve, reject, ...payload } }) {
  try {
    const url = process.env.NEXT_PUBLIC_URL_API + `/users/login`;
    const option = postOptions({ celphone: parseInt(payload.s) });
    const registerPayment = yield call(request, url, option);

    const url2 = process.env.NEXT_PUBLIC_URL_API + `/whoAmi`;
    const option2 = getOptionsWithTokenSecond(registerPayment.token);
    const registerPayment2 = yield call(request, url2, option2);
    yield all([put(searchUsersSuccess(registerPayment2))]);
    yield call(resolve, 'Se registro con exito');
  } catch (e) {
    yield call(reject, 'No se pudo registrar en la base de datos');
  }
}

export function* searchProductsUsersSaga({
  payload: { resolve, reject, ...payload },
}) {
  try {
    const url = process.env.NEXT_PUBLIC_URL_API + `/users/login`;
    const option = postOptions({ celphone: parseInt(payload.celphone) });
    const registerPayment = yield call(request, url, option);
    yield all([put(searchProductsUsersSuccess(registerPayment.token))]);
    yield call(resolve, 'Se registro con exito');
  } catch (e) {
    yield call(reject, 'No se pudo registrar en la base de datos');
  }
}
export function* searchProductsUsersSaga2({ payload }) {
  const { tokenUser } = yield select((state) => state);
  const SearchServices = new Search();
  try {
    let cat = [];
    let products = [];
    const filterAds = yield SearchServices.getSearch(payload, tokenUser);
    cat = filterAds.filter((e) => e.payment.user !== undefined);
    cat.map((e) => {
      e.amount = e.total;
      e.date = e.payment.createDate.substr(0, 10);
      e.deliveryAddress = e.payment.user.direction;
      e.deliveryTime = e.dateDelivery.substr(0, 10);
      if (e.detailDeliveries !== undefined) {
        switch (e.detailDeliveries[0].statusDelivery.type) {
          case 'recibido':
            e.status = '3';
            break;
          case 'rechazado':
            e.status = '2';
            break;
          case 'entregado':
            e.status = '5';
            break;
          case 'enviando':
            e.status = '4';
            break;
          default:
            e.status = '1';
            break;
        }
      } else {
        e.status = '1';
      }
      e.subtotal = e.total;
      e.orderDetails.map((e, i) => {
        products[i] = {
          id: e.product.id,
          image: e.product.fileStorages[0].linkFile,
          //price:e.product.prices[0].price,
          quantity: 1,
          title: e.product.name,
          //total:e.product.prices[0].price
        };
      });
      e.products = products;
      products = [];
    });
    yield all([put(searchProductsUsersSuccess2(cat))]);
    console.log('extio');
  } catch (e) {
    console.log('fue un error');
  }
}

export function* registerUserSugerencySaga({ payload }) {
  let ddd =
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6ImVhZTY5N2RlLTE0NGUtNDIxNi04ZjEzLTQwZDBhNTY2YTU3MyIsInJvbGVJZCI6ImZmYWI5YWM2LTcxNGMtNGI4MC1iZDg3LTc5NGUzZjY2YjY0MiIsImVtYWlsIjoiYWRtaW5Sb290QGZlcnJldGVyaWEuY29tIiwiY2VscGhvbmUiOm51bGwsImlhdCI6MTYyMjE2MjEwMywiZXhwIjoxNjIyMzcyNzAzfQ.fszOJBXZ6KlR--xwdUBm4NAo9mfvSbVdY02BpkHxAx4';
  try {
    const url = process.env.NEXT_PUBLIC_URL_API + `/account-banks`;
    const option = postOptions2(payload, ddd);
    const registerPayment = yield call(request, url, option);
    console.log(registerPayment);
    console.log('extio');
  } catch (e) {
    console.log('fracaso');
  }
}

export function* sagaLogin() {
  yield takeLatest(
    INITIAL_REQUEST_CATEGORIES_START,
    initialRequestCategoriesSaga
  );
  yield takeLatest(INITIAL_REQUEST_PRODUCTS_START, initialRequestProductsSaga);
  yield takeLatest(
    INITIAL_REQUEST_PRODUCTS_SEARCH_START,
    initialRequestProductsSearchSaga
  );
  yield takeLatest(INITIAL_REQUEST_ADS_START, initialRequestAdsSagas);
  yield takeLatest(REGISTER_USER_START, registerUserSaga);
  yield takeLatest(UPDATE_USER_START, updateUserSaga);
  yield takeLatest(DELETE_USER_START, deleteUserSaga);
  yield takeLatest(REGISTER_PAYMENT_START, registerPaymentsSaga);
  yield takeLatest(REGISTER_PAYMENT_DETAILS_START, registerPaymentsDetailsSaga);
  yield takeLatest(REGISTER_PAYMENT_ORDERS_START, registerPaymentsOrdersSaga);
  yield takeLatest(LOGIN_USER_START, loginUserSaga);
  yield takeLatest(SEARCH_USERS_START, searchUsersSaga);
  yield takeLatest(SEARCH_PRODUCTS_USERS_START, searchProductsUsersSaga);
  yield takeLatest(SEARCH_PRODUCTS_USERS_START2, searchProductsUsersSaga2);
  yield takeLatest(REGISTER_USER_SUGERENCY, registerUserSugerencySaga);
}
