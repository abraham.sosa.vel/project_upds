import { RootStateOrAny, useSelector } from "react-redux";
/**
 * Parses the JSON returned by a network request
 *
 * @param  {object} response A response from a network request
 *
 * @return {object}          The parsed JSON from the request
 */
 function parseJSON(response) {
    return response.status === 204 ? "" : response.json();
  }
  
  /**
   * Checks if a network request came back fine, and throws an error if not
   *
   * @param  {object} response   A response from a network request
   *
   * @return {object|undefined} Returns either the response, or throws an error
   */
  function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
      return response;
    }
  
    const error = new Error(response.status);
    // error.response = response;
    throw error;
  }
  
  /**
   * Requests a URL, returning a promise
   *
   * @param  {string} url     The URL we want to request
   * @param  {object} [options] The options we want to pass to "fetch"
   *
   * @return {object}           The response data
   */
  export default function request(url, options) {
    return fetch(url, options).then(checkStatus).then(parseJSON);
  }
  
  export function postOptionsFormData(body = {}, method = "POST") {
    return {
      method,
      headers: {
        // Authorization: `Bearer ${tokenUser}`,
      },
      body,
    };
  }
  
  export function postOptionsWithoutToken(body = {}, method = "POST") {
    return {
      method,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    };
  }
  
  export function getOptions(method = "GET") {
    return {
      method,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        // Authorization: `Bearer ${tokenUser}`,
      },
    };
  }
  
  export function getOptionsWithToken(method = "GET") {
    let {tokenUser}= useSelector((state: RootStateOrAny) => state)
    console.log(tokenUser)
    return {
      method,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${tokenUser.token}`,
      },
    };
  }

  export function getOptionsWithTokenSecond(tokenUser="", method = "GET") {
    return {
      method,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${tokenUser}`,
      },
    };
  }
  export function postOptionsWithToken(body = {}, method = "POST",token) {
    return {
      method,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(body),
    };
  }
  export function putOptionsWithToken(body = {}, method = "PUT") {
    let {tokenUser}= useSelector((state: RootStateOrAny) => state)
    return {
      method,
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${tokenUser.token}`,
      },
      body: JSON.stringify(body),
    };
  }
  
export function patchOptionsWithToken(body = {}, method = "PATCH") {
  let {tokenUser}= useSelector((state: RootStateOrAny) => state)
	return {
		method,
		headers: {
			"Content-Type": "application/json",
			Accept: "application/json",
			Authorization: `Bearer ${tokenUser.token}`,
		},
		body: JSON.stringify(body),
	};
}

export function deleteOptionsWithToken(body, method = "DELETE",data="") {
	return {
		method,
		headers: {
			Accept: "application/json",
			Authorization: `Bearer ${data}`,
		},
		body: JSON.stringify(body),
	};
}



  export function getOptionsWithoutToken(method = "GET") {
    return {
      method,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
    };
  }
  
  export function postOptions(body = {}, method = "POST") {
    return {
      method,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        // Authorization: `Bearer ${tokenUser}`,
      },
      body: JSON.stringify(body),
    };
  }
  export function postOptions2(body = {},tokenUser, method = "POST") {
    return {
      method,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
         Authorization: `Bearer ${tokenUser}`,
      },
      body: JSON.stringify(body),
    };
  }
  
  export function putOptions(body = {}, method = "PUT") {
    return {
      method,
      headers: {
        Accept: "application/json",
        // Authorization: `Bearer ${tokenUser}`,
      },
      body: JSON.stringify(body),
    };
  }
  
  export function patchOptions(body = {}, method = "PATCH") {
    return {
      method,
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
        // Authorization: `Bearer ${tokenUser}`,
      },
      body: JSON.stringify(body),
    };
  }
  
  export function deleteOptions(body, method = "DELETE") {
    //const data= useSelector((state: RootStateOrAny) => state)
    return {
      method,
      headers: {
        Accept: "application/json",
         //Authorization: `Bearer ${tokenUser.token}`,
      },
      body: JSON.stringify(body),
    };
  }
  
