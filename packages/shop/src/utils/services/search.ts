import { StorageServices } from "./storage";

export class Search extends StorageServices {
  async getSearch(data,token) {
    const filter = {
        fields:{
             priceDelivery:false
        },
         include:[
                {
                    relation:"detailDeliveries",
                    scope:{
                        fields:{
                        },
                        include:[
                            {
                                relation:"statusDelivery"
                                
                            }
                        ]
                    },
                },
                {
                    relation:"payment",
                    scope:{
                        fields:{
                            lastUpdate:false
                        }, 
                        include:[
                             {
                                relation:"user",
                                scope:{
                                    fields:{
                                        firstName:false,
                                        lastName:false,
                                        email:false,
                                        gps:false,
                                        password:false,
                                        userName:false
                                    },
                                     where:{
                                     celphone:data
                                        },
                                }
                            },
                        ]
                    }
                },
                 {
                    relation:"orderDetails",
                    scope:{
                        fields:{},
                         include:[
                             {
                                relation:"product",
                                scope:{
                                    fields:{
                                        description:false,
                                        amountMin:false,
                                        shortDelivery:false,
                                        longDelivery:false,
                                        unit:false,
                                    },
                                    include:[
                                        {
                                            relation:"fileStorages",
                                            scope:{
                                                fields:{
                                                    id:true,
                                                    linkFile:true,
                                                    productId:true
                                                }
                                            }},
                                            {
                                                relation:"prices",
                                                scope:{
                                                    fields:{
                                                     
                                                    }
                                                }}
                                    ]
                                }
                            },
                        ]
                    }
                },
            ],
     };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpointWhitToken(`detail-payments`,token);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}
