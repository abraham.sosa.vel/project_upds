import { StorageServices } from "./storage";

export class Products extends StorageServices {
  async getProducts(data) {
    const filter = {
        fields: {
        },
        where:{
            typeCategoryId:data
          },
        include:[
             {
                relation:"prices",
            },
            {
                relation:"category",
                scope:{
                    fields:{
                        typeCategoryId:true,
                        id:true
                    },
                    where:{
                        typeCategoryId: data
                    }
                }
            },
            {
                relation:"fileStorages",
                scope:{
                    fields:{
                        linkFile:true,
                        productId:true
                    }
                }
            }
        ],
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`products`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }

  async getProductsFilter(data) {
    const filter = {
        fields: {
        },
        include:[
             {
                relation:"prices",
            },
            {
                relation:"category",
                scope:{
                    fields:{
                        typeCategoryId:true,
                        id:true,
                        title:true
                    },
                    where:{
                        title: data
                    }
                }
            },
            {
                relation:"fileStorages",
                scope:{
                    fields:{
                        linkFile:true,
                        productId:true
                    }
                }
            }
        ],
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`products`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
  async getProductsFilterSearch(data,data2) {
    const filter = {
        fields: {
        },
        where:{
            name:data2
          },
        include:[
             {
                relation:"prices",
            },
            {
                relation:"category",
                scope:{
                    fields:{
                        typeCategoryId:true,
                        id:true,
                    },
                    where:{
                        typeCategoryId: data
                    }
                }
            },
            {
                relation:"fileStorages",
                scope:{
                    fields:{
                        linkFile:true,
                        productId:true
                    }
                }
            }
        ],
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`products`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
  
}
