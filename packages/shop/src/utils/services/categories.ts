import { StorageServices } from "./storage";

export class Categories extends StorageServices {
  async getCategories(data) {
    const filter = {
      where:{
        typeCategoryId:data
      }
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`categories`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}
