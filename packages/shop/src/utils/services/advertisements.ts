import { StorageServices } from "./storage";

export class Advertisements extends StorageServices {
  async getAds() {
    const filter = {
        fields: {
        },
        include:[
            {
                relation:"fileStorages",
                scope:{
                    fields:{
                        linkFile:true,
                        adId:true
                            }
                      }
            }
        ],
    };
    try {
      this.setFilterEndpoint(filter);
      return await this.getFetchEndpoint(`ads`);
    } catch (e) {
      console.log(e);
      return [];
    }
  }
}
