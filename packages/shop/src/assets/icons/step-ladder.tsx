import React from "react";

export const StepLadder = ({
  color = "currentColor",
  width = "18px",
  height = "18px",
}) => {
  return (
    <svg
    width={width}
    height={height}
      id="Icons"
      viewBox="0 0 74 74"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="m53 72a1 1 0 0 1 -1-1v-68a1 1 0 0 1 2 0v68a1 1 0 0 1 -1 1z" />
      <path d="m21 72a1 1 0 0 1 -1-1v-68a1 1 0 0 1 2 0v68a1 1 0 0 1 -1 1z" />
      <path d="m53 12h-32a1 1 0 0 1 0-2h32a1 1 0 0 1 0 2z" />
      <path d="m53 25h-32a1 1 0 0 1 0-2h32a1 1 0 0 1 0 2z" />
      <path d="m53 38h-32a1 1 0 0 1 0-2h32a1 1 0 0 1 0 2z" />
      <path d="m53 51h-32a1 1 0 0 1 0-2h32a1 1 0 0 1 0 2z" />
      <path d="m53 64h-32a1 1 0 0 1 0-2h32a1 1 0 0 1 0 2z" />
    </svg>
  );
};
