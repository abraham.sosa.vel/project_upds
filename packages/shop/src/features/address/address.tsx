import React, { useContext } from 'react';
import { FormattedMessage } from 'react-intl';
import RadioGroup from 'components/radio-group/radio-group';
import RadioCard from 'components/radio-card/radio-card';
import { Button } from 'components/button/button';
import UpdateAddress from 'components/address-card/address-card';
import { handleModal } from 'features/checkouts/checkout-modal';
import { ProfileContext } from 'contexts/profile/profile.context';
import { useMutation } from '@apollo/client';
import { DELETE_ADDRESS } from 'graphql/mutation/address';
import { CardHeader } from 'components/card-header/card-header';
import { ButtonGroup } from 'components/button-group/button-group';
import { Box } from 'components/box';
import { Plus } from 'assets/icons/PlusMinus';
import { RootStateOrAny, useDispatch, useSelector } from 'react-redux';
import { DELETE_USER_START } from 'redux/constants';

interface Props {
  increment?: boolean;
  icon?: boolean;
  buttonProps?: any;
  flexStart?: boolean;
}

const Address = ({
  increment = false,
  flexStart = false,
  buttonProps = {
    size: 'big',
    variant: 'outlined',
    type: 'button',
    className: 'add-button',
  },
  icon = false,
}: Props) => {
  const [deleteAddressMutation] = useMutation(DELETE_ADDRESS);

  const handleOnDelete =  (item) => {
    dispatch({ type:DELETE_USER_START, payload: item.id });
  };
let dispatch=useDispatch();
  const {address}= useSelector((state: RootStateOrAny) => state);
  return (
    <>
      <CardHeader increment={increment}>
        <FormattedMessage
          id='checkoutDeliveryAddress'
          defaultMessage='Select Your Delivery Address'
        />
      </CardHeader>
      <ButtonGroup flexStart={flexStart}>
        <RadioGroup
          items={address}
          component={(item: any,index) => (
            <RadioCard
              id={item.id}
              lastName={item.lastName}
              celphone={item.celphone}
              key={index}
              title={item.firstName}
              content={item.direction}
              name='address'
              checked={item.type === 'primary'}
              onChange={() =>
                dispatch({
                  type: 'SET_PRIMARY_ADDRESS',
                  payload: item.id.toString(),
                })
              }
              onEdit={() => handleModal(UpdateAddress, item)}
              onDelete={() => handleOnDelete( item)}
           
            />
          )}
          secondaryComponent={
            address.length!=0
            ?<Button
            {...buttonProps}
            onClick={() => "holaa"}
          >
            {icon && (
            <Box mr={2}>
              <Plus width='10px' />
            </Box>
          )}
            <FormattedMessage
              id='addAddressBtn'
              defaultMessage='Add Address'
            />
          </Button>
          :<Button
          {...buttonProps}
          onClick={() => handleModal(UpdateAddress, 'add-address-modal')}
        >
          {icon && (
            <Box mr={2}>
              <Plus width='10px' />
            </Box>
          )}
          <FormattedMessage
            id='addAddressBtn'
            defaultMessage='Add Address'
          />
        </Button>
          }
        />
      </ButtonGroup>
    </>
  );
};
export default Address;
