import React, { useContext } from 'react';
import { FormattedMessage } from 'react-intl';
import RadioGroup from 'components/radio-group/radio-group';
import RadioCard from 'components/radio-card/radio-card';

import { ProfileContext } from 'contexts/profile/profile.context';
import { CardHeader } from 'components/card-header/card-header';
import { useDispatch } from 'react-redux';
import { insertDataDay } from 'redux/actions';

interface Props {
  increment?: boolean;
}

const Day = ({ increment = false }: Props) => {
  const {
    state: { day },
    dispatch,
  } = useContext(ProfileContext);

  let dispatcher=useDispatch();
  const changeData=(e,d)=>{
    dispatcher(insertDataDay(d))
      dispatch({
        type: 'SET_PRIMARY_DAY',
        payload: e.toString(),
      })
  }
  return (
    <>
      <CardHeader increment={increment}>
        <FormattedMessage
          id="deliveryDay"
          defaultMessage="Select Your Delivery day"
        />
      </CardHeader>
      <RadioGroup
        items={day}
        component={(item: any) => ( 
          <RadioCard
          celphone={0}
          lastName=""
            id={item.id}
            key={item.id}
            title={item.title}
            content={item.time_slot}
            name="day"
            checked={item.type === 'primary'}
            withActionButtons={false}
            onChange={() => changeData(item.id,item.title)}
          />
        )}
      />
    </>
  );
};

export default Day;
