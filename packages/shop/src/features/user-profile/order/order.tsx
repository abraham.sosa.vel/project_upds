import React, { useState, useEffect } from "react";
import { Scrollbar } from "components/scrollbar/scrollbar";
import { useQuery, gql } from "@apollo/client";
import {
  DesktopView,
  MobileView,
  OrderBox,
  OrderListWrapper,
  OrderList,
  OrderDetailsWrapper,
  Title,
  ImageWrapper,
  ItemWrapper,
  ItemDetails,
  ItemName,
  ItemSize,
  ItemPrice,
  NoOrderFound,
} from "./order.style";

import OrderDetails from "./order-details/order-details";
import OrderCard from "./order-card/order-card";
import OrderCardMobile from "./order-card/order-card-mobile";
import useComponentSize from "utils/useComponentSize";
import { FormattedMessage } from "react-intl";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import { SEARCH_PRODUCTS_USERS_START, SEARCH_PRODUCTS_USERS_START2 } from "redux/constants";

const progressData = [
  "Pedido en revision",
  "Pedido no admitido",
  "Orden recibida",
  "Pedido en camino ",
  "Pedido entregado",
];

const GET_ORDERS = gql`
  query getAllOrders($text: String, $user: Int!, $limit: Int) {
    orders(text: $text, limit: $limit, user: $user) {
      id
      status
      deliveryAddress
      amount
      date
      subtotal
      deliveryFee
      discount
      deliveryTime
      products {
        title
        price
        total
        image
        weight
        quantity
        id
      }
    }
  }
`;

const orderTableColumns = [
  {
    title: <FormattedMessage id="cartItems" defaultMessage="Items" />,
    dataIndex: "",
    key: "items",
    width: 250,
    ellipsis: true,
    render: (text, record) => {
      return (
        <ItemWrapper>
          <ImageWrapper>
            <img src={record.image} alt={record.title} />
          </ImageWrapper>

          <ItemDetails>
            <ItemName>{record.title}</ItemName>
            <ItemSize>{record.weight}</ItemSize>
            <ItemPrice>Bs{record.price}</ItemPrice>
          </ItemDetails>
        </ItemWrapper>
      );
    },
  },
  {
    title: (
      <FormattedMessage id="intlTableColTitle2" defaultMessage="Quantity" />
    ),
    dataIndex: "quantity",
    key: "quantity",
    align: "center",
    width: 100,
  },
  {
    title: <FormattedMessage id="intlTableColTitle3" defaultMessage="Price" />,
    dataIndex: "",
    key: "price",
    align: "right",
    width: 100,
    render: (text, record) => {
      return <p>Bs{record.total}</p>;
    },
  },
];

const OrdersContent: React.FC<{}> = () => {
  const [order, setOrder] = useState(null);
  const [active, setActive] = useState("");
  const dispatch=useDispatch();
  const [targetRef, size] = useComponentSize();
  const orderListHeight = size.height - 79;
  const {  error, loading } = useQuery(GET_ORDERS, {
    variables: {
      limit: 7,
      user: 1,
    },
  });
  const data = useSelector((state: RootStateOrAny) => state);
  console.log(data)
  useEffect(() => {
    if (data && data.orders && data.orders.length !== 0) {
      setOrder(data.orders[0]);
      setActive(data.orders[0].id);
    }
  }, [data && data.orders]);

  if (loading) {
    return <div>Cargando...</div>;
  }

  if (error) return <div>{error.message}</div>;
  const handleClick = (order: any) => {
    setOrder(order);
    setActive(order.id);
  };


  const searchPhone=(e)=>{
    let este=e.target.value;
    return new Promise((resolve, reject) => {
      dispatch({type: SEARCH_PRODUCTS_USERS_START,payload: {celphone:este,resolve,reject}});
    })
      .then((err) => {
        dispatch({type: SEARCH_PRODUCTS_USERS_START2,payload:este});
      })
      .catch((err) => {
        console.log("error");
      });
  }
  console.log(data.orders)
  return (
    <OrderBox>
      <DesktopView>
        <OrderListWrapper style={{ height: size.height }}>
          <div style={{ margin: "1rem" }}>
            <label style={{ color: "#009e7f", fontSize: "1.3rem" }}>
              Ingrese su numero de telefono :<input placeholder="Telefono" onChange={searchPhone} />
            </label>
          </div>
          <Title style={{ padding: "0 20px" }}>
            <FormattedMessage
              id="intlOrderPageTitle"
              defaultMessage="My Order"
            />
          </Title>
          <Scrollbar className="order-scrollbar">
            <OrderList>
              {data.orders.length !== 0 ? (
                data.orders.map((current: any) => (
                  <OrderCard
                    key={current.id}
                    orderId={current.id}
                    className={current.id === active ? "active" : ""}
                    status={progressData[current.status - 1]}
                    date={current.date}
                    deliveryTime={current.deliveryTime}
                    amount={current.amount}
                    onClick={() => {
                      handleClick(current);
                    }}
                  />
                ))
              ) : (
                <NoOrderFound>
                  <FormattedMessage
                    id="intlNoOrderFound"
                    defaultMessage="No order found"
                  />
                </NoOrderFound>
              )}
            </OrderList>
          </Scrollbar>
        </OrderListWrapper>

        <OrderDetailsWrapper ref={targetRef}>
          <Title style={{ padding: "0 20px" }}>
            <FormattedMessage
              id="orderDetailsText"
              defaultMessage="Order Details"
            />
          </Title>
          {order && order.id && (
            <OrderDetails
              progressStatus={order.status}
              progressData={progressData}
              address={order.deliveryAddress}
              subtotal={order.subtotal}
              discount={order.discount}
              deliveryFee={order.deliveryFee}
              grandTotal={order.amount}
              tableData={order.products}
              columns={orderTableColumns}
            />
          )}
        </OrderDetailsWrapper>
      </DesktopView>
      <MobileView>
        <OrderList>
          <div style={{ margin: "1rem" }}>
            <label style={{ color: "#009e7f", fontSize: "1.3rem" }}>
              Ingrese su numero de telefono :<input placeholder="Telefono" onChange={searchPhone}/>
            </label>
          </div>
          <OrderCardMobile
            orders={data.orders}
            className={order && order.id === active ? "active" : ""}
            progressData={progressData}
            columns={orderTableColumns}
            onClick={() => {
              handleClick(order);
            }}
          />
        </OrderList>
      </MobileView>
    </OrderBox>
  );
};

export default OrdersContent;
