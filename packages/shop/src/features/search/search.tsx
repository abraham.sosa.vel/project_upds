import React from 'react';
import { SearchBox } from 'components/search-box/search-box';
import { useAppState, useAppDispatch } from 'contexts/app/app.provider';
import { useRouter } from 'next/router';
import { useIntl } from 'react-intl';
import { useDispatch } from 'react-redux';
import { INITIAL_REQUEST_PRODUCTS_SEARCH_START } from 'redux/constants';

interface Props {
  minimal?: boolean;
  showButtonText?: boolean;
  onSubmit?: () => void;
  [key: string]: unknown;
}

const Search: React.FC<Props> = ({ onSubmit, ...props }) => {
  const searchTerm = useAppState('searchTerm');
  const dispatch = useAppDispatch();
  const router = useRouter();
  const intl = useIntl();
  const dispatchu=useDispatch()

  const handleOnChange = (e) => {
    const { value } = e.target;
    dispatch({ type: 'SET_SEARCH_TERM', payload: value });
  };
  const { pathname, query } = router;
  const onSearch = (e) => {
    e.preventDefault();
    const { type, ...rest } = query;
    console.log(type)
    console.log(searchTerm)
    dispatchu({type:INITIAL_REQUEST_PRODUCTS_SEARCH_START,payload:{type,searchTerm}})
    if (type) {
      router.push(
        {
          pathname,
          query: { ...rest, text: searchTerm },
        },
        {
          pathname: `/${type}`,
          query: { ...rest, text: searchTerm },
        }
      );
    } else {
      router.push({
        pathname,
        query: { ...rest, text: searchTerm },
      });
    }
    dispatch({ type: 'SET_SEARCH_TERM', payload: '' });
    if (onSubmit) {
      onSubmit();
    }
  };
  return (
    <SearchBox
      onEnter={onSearch}
      onChange={handleOnChange}
      value={searchTerm}
      name="search"
      placeholder={intl.formatMessage({
        id: 'searchPlaceholder',
        defaultMessage: 'Busque sus productos aqui',
      })}
      categoryType={query.type || 'bakery'}
      buttonText={intl.formatMessage({
        id: 'searchButtonText',
        defaultMessage: 'Buscar',
      })}
      {...props}
    />
  );
};

export default Search;
