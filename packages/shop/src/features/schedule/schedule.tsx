import React, { useContext } from 'react';
import { FormattedMessage } from 'react-intl';
import RadioGroup from 'components/radio-group/radio-group';
import RadioCard from 'components/radio-card/radio-card';

import { ProfileContext } from 'contexts/profile/profile.context';
import { CardHeader } from 'components/card-header/card-header';
import { useDispatch } from 'react-redux';
import { insertDataHour } from 'redux/actions';

interface Props {
  increment?: boolean;
}

const Schedules = ({ increment = false }: Props) => {
  const {
    state: { schedules },
    dispatch,
  } = useContext(ProfileContext);

  let dispatcher=useDispatch();
  const changeData=(e,d)=>{
    dispatcher(insertDataHour(d))
    dispatch({
      type: 'SET_PRIMARY_SCHEDULE',
      payload: e.toString(),
    })
}
  return (
    <>
      <CardHeader increment={increment}>
        <FormattedMessage
          id="deliverySchedule"
          defaultMessage="Select Your Delivery Schedule"
        />
      </CardHeader>
      <RadioGroup
        items={schedules}
        component={(item: any) => (
          <RadioCard
          celphone={0}
          lastName=""
            id={item.id}
            key={item.id}
            title={item.title}
            content={item.time_slot}
            name="schedule"
            checked={item.type === 'primary'}
            withActionButtons={false}
            onChange={() => changeData(item.id,item.title)}
          />
        )}
      />
    </>
  );
};

export default Schedules;
