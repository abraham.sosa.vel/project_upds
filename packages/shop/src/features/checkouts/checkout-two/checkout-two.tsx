import React, { useContext, useState, useEffect } from "react";
import Router from "next/router";
import Link from "next/link";
import { Button } from "components/button/button";
import { CURRENCY } from "utils/constant";
import { Scrollbar } from "components/scrollbar/scrollbar";
import CheckoutWrapper, {
  CheckoutContainer,
  CheckoutInformation,
  InformationBox,
  DeliverySchedule,
  CheckoutSubmit,
  HaveCoupon,
  CouponBoxWrapper,
  CouponInputBox,
  CouponCode,
  RemoveCoupon,
  TermConditionText,
  TermConditionLink,
  CartWrapper,
  CalculationWrapper,
  OrderInfo,
  Title,
  ItemsWrapper,
  Items,
  Quantity,
  Multiplier,
  ItemInfo,
  Price,
  TextWrapper,
  Text,
  Bold,
  Small,
  NoProductMsg,
  NoProductImg,
} from "./checkout-two.style";

import { NoCartBag } from "assets/icons/NoCartBag";

import Sticky from "react-stickynode";
import { ProfileContext } from "contexts/profile/profile.context";
import { FormattedMessage } from "react-intl";
import { useCart } from "contexts/cart/use-cart";
import { useLocale } from "contexts/language/language.provider";
import { useWindowSize } from "utils/useWindowSize";
import Coupon from "features/coupon/coupon";
import Address from "features/address/address";
import Schedules from "features/schedule/schedule";
import Contact from "features/contact/contact";
import Payment from "features/payment/payment";
import Day from "features/day/day";
import { RootStateOrAny, useDispatch, useSelector } from "react-redux";
import { REGISTER_PAYMENT_DETAILS_START, REGISTER_PAYMENT_ORDERS_START, REGISTER_PAYMENT_START } from "redux/constants";
import { countProductsStart } from "redux/actions";

// The type of props Checkout Form receives
interface MyFormProps {
  token: string;
  deviceType: any;
}

type CartItemProps = {
  product: any;
};

const OrderItem: React.FC<CartItemProps> = ({ product }) => {
  const { id, quantity, title, name, unit, price, salePrice, prices } = product;
  const displayPrice = salePrice ? salePrice : price;
  let res;
  res = displayPrice;
  prices.map((e) =>
    quantity >= e.amountMin && quantity <= e.amountMax ? (res = e.price) : res
  );
  return (
    <Items key={id}>
      <Quantity>{quantity}</Quantity>
      <Multiplier>x</Multiplier>
      <ItemInfo>
        {name ? name : title} {unit ? `| ${unit}` : ""}
      </ItemInfo>
      <Price>
        {CURRENCY}
        {(res * quantity).toFixed(2)}
      </Price>
    </Items>
  );
};
const CheckoutWithSidebar: React.FC<MyFormProps> = ({ token, deviceType }) => {
  const [hasCoupon, setHasCoupon] = useState(false);
  const { state } = useContext(ProfileContext);
  const { isRtl } = useLocale();
  const {
    items,
    removeCoupon,
    coupon,
    clearCart,
    cartItemsCount,
    calculatePrice,
    calculateDiscount,
    calculateSubTotalPrice,
    isRestaurant,
    toggleRestaurant,
  } = useCart();
  const [loading, setLoading] = useState(false);
  const [isValid, setIsValid] = useState(false);
  const { address, contact, card, schedules, day } = state;
  const size = useWindowSize();
 ( items.sort((a, b) => a.longDelivery - b.longDelivery)).reverse()
  console.log(items)
  const data = useSelector((state: RootStateOrAny) => state);
  let dispatch = useDispatch();
  const handleSubmit = async () => {
    return new Promise((resolve, reject) => {
      dispatch({ type: REGISTER_PAYMENT_START, payload: { resolve, reject } });
    })
      .then((res) => {
        return new Promise((resolve, reject) => {
          dispatch({ type: REGISTER_PAYMENT_DETAILS_START, payload: { price:calculatePrice(),resolve, reject } });
        })
          .then((res) => {
                return new Promise((resolve, reject) => {

                  items.map(e=>{
                    for(let i=1;i<=e.quantity;i++){
                      dispatch({ type: REGISTER_PAYMENT_ORDERS_START, payload: {productId:e.id,resolve, reject } });
                    }
                  })
                })
                  .then((res) => {
                    dispatch(countProductsStart(items))
                      setLoading(true);
                      if (isValid) {
                       clearCart();
                       Router.push("/order-received");
                         }
                        setLoading(false);
                  })
                  .catch((err) => {
                    console.log(err);
                  });
          })
          .catch((err) => {
            console.log(err);
          });
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    if (
      calculatePrice() > 0 &&
      cartItemsCount > 0 &&
      data.day != "" &&
      data.hour != "" &&
      data.address.length != 0 &&
      contact.length &&
      card.length &&
      schedules.length &&
      day.length
    ) {
      setIsValid(true);
    }
  }, [data]);
  useEffect(() => {
    return () => {
      if (isRestaurant) {
        toggleRestaurant();
        clearCart();
      }
    };
  }, []);

  return (
    <form>
      <CheckoutWrapper>
        <CheckoutContainer>
          <CheckoutInformation>
            {/* DeliveryAddress */}
            <InformationBox>
              <Address
                increment={true}
                flexStart={true}
                buttonProps={{
                  variant: "text",
                  type: "button",
                  className: "addButton",
                }}
                icon={true}
              />
            </InformationBox>

            {/* DeliverySchedule */}
            <InformationBox>
              <DeliverySchedule>
                <Day increment={true} />
              </DeliverySchedule>
            </InformationBox>

            {/* DeliverySchedule */}
            <InformationBox>
              <DeliverySchedule>
                <Schedules increment={true} />
              </DeliverySchedule>
            </InformationBox>

            {/* Contact number */}
            {/*<InformationBox>
              <Contact
                increment={true}
                flexStart={true}
                buttonProps={{
                  variant: 'text',
                  type: 'button',
                  className: 'addButton',
                }}
                icon={true}
              />
            </InformationBox>*/}
            {/* PaymentOption */}

            {/* Coupon start */}
            {/*{coupon ? (
                <CouponBoxWrapper>
                  <CouponCode>
                    <FormattedMessage id='couponApplied' />
                    <span>{coupon.code}</span>

                    <RemoveCoupon
                      onClick={(e) => {
                        e.preventDefault();
                        removeCoupon();
                        setHasCoupon(false);
                      }}
                    >
                      <FormattedMessage id='removeCoupon' />
                    </RemoveCoupon>
                  </CouponCode>
                </CouponBoxWrapper>
              ) : (
                <CouponBoxWrapper>
                  {!hasCoupon ? (
                    <HaveCoupon onClick={() => setHasCoupon((prev) => !prev)}>
                      <FormattedMessage
                        id='specialCode'
                        defaultMessage='Have a special code?'
                      />
                    </HaveCoupon>
                  ) : (
                    <CouponInputBox>
                      <Coupon errorMsgFixed={true} className='normalCoupon' />
                    </CouponInputBox>
                  )}
                </CouponBoxWrapper>
              )}*/}

            {/*<TermConditionText>
                <FormattedMessage
                  id='termAndConditionHelper'
                  defaultMessage='By making this purchase you agree to our'
                />
                <Link href='#'>
                  <TermConditionLink>
                    <FormattedMessage
                      id='termAndCondition'
                      defaultMessage='terms and conditions.'
                    />
                  </TermConditionLink>
                </Link>
              </TermConditionText>

              {/* CheckoutSubmit */}
            <CheckoutSubmit>
              <Button
                type="button"
                onClick={handleSubmit}
                disabled={!isValid}
                size="big"
                loading={loading}
                style={{ width: "100%" }}
              >
                <FormattedMessage
                  id="processCheckout"
                  defaultMessage="Finalizar pedido"
                />
              </Button>
            </CheckoutSubmit>
          </CheckoutInformation>

          <CartWrapper>
            <Sticky
              enabled={size.width >= 768 ? true : false}
              top={120}
              innerZ={999}
            >
              <OrderInfo>
                <Title>
                  <FormattedMessage id="cartTitle" defaultMessage="Su pedido" />
                </Title>

                <Scrollbar className="checkout-scrollbar">
                  <ItemsWrapper>
                    {cartItemsCount > 0 ? (
                      items.map((item) => (
                        <OrderItem key={`cartItem-${item.id}`} product={item} />
                      ))
                    ) : (
                      <>
                        <NoProductImg>
                          <NoCartBag />
                        </NoProductImg>

                        <NoProductMsg>
                          <FormattedMessage
                            id="noProductFound"
                            defaultMessage="No hay productos seleccionados"
                          />
                        </NoProductMsg>
                      </>
                    )}
                  </ItemsWrapper>
                </Scrollbar>

                <CalculationWrapper>
                  <TextWrapper>
                    <Text>
                      <FormattedMessage
                        id="subTotal"
                        defaultMessage="Subtotal"
                      />
                    </Text>
                    <Text>
                      {CURRENCY}
                      {calculateSubTotalPrice()}
                    </Text>
                  </TextWrapper>

                  <TextWrapper>
                    <Text>
                      <FormattedMessage
                        id="intlOrderDetailsDelivery"
                        defaultMessage="Delivery Fee"
                      />
                    </Text>
                    <Text>{CURRENCY}0.00</Text>
                  </TextWrapper>

                  <TextWrapper>
                    <Text>
                      <FormattedMessage
                        id="discountText"
                        defaultMessage="Discount"
                      />
                    </Text>
                    <Text>
                      {CURRENCY}
                      {calculateDiscount()}
                    </Text>
                  </TextWrapper>
                  <TextWrapper>
                    <Text>
                   El costo de envio variara segun los productos y el lugar un estimado es:
                    </Text>
                  </TextWrapper>
                  <TextWrapper>
                    <Text>
                    {items[0]?.shortDelivery}Bs &gt; {items[0]?.longDelivery}Bs
                    </Text>
                  </TextWrapper>

                  <TextWrapper style={{ marginTop: 20 }}>
                    <Bold>
                      <FormattedMessage id="totalText" defaultMessage="Total" />{" "}
                    </Bold>
                    <Bold>
                      {CURRENCY}
                      {calculatePrice()}
                    </Bold>
                  </TextWrapper>
                </CalculationWrapper>
              </OrderInfo>
            </Sticky>
          </CartWrapper>
        </CheckoutContainer>
      </CheckoutWrapper>
    </form>
  );
};

export default CheckoutWithSidebar;
