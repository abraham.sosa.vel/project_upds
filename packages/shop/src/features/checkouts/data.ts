export default [
  {
    id: '1',
    type: 'secondary',
    title: '8am-11am',
    time_slot: '8.00 AM - 11.00 AM',
  },
  {
    id: '2',
    type: 'secondary',
    title: '11am-2pm',
    time_slot: '11.00 AM - 2.00 PM',
  },
  {
    id: '3',
    type: 'secondary',
    title: '2pm-5pm',
    time_slot: '2.00 PM - 6.00 PM',
  },
];

