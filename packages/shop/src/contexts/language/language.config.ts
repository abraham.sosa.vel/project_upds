export const defaultLocale = 'en' as const;

export const locales = ['en'] as const;

// need to customize later
export const languageNames = {
  en: 'English',
};
