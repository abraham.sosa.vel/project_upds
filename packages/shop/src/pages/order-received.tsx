import React from 'react';
import { SEO } from 'components/seo';
import OrderReceived from 'features/order-received/order-received';
import { RootStateOrAny, useSelector } from 'react-redux';

const OrderReceivedPage = () => {
  const data = useSelector((state: RootStateOrAny) => state);
  return (
    <>
      <SEO title="Invoice - PickBazar" description="Invoice Details" />
      <OrderReceived
      data={data} />
    </>
  );
};

export default OrderReceivedPage;
