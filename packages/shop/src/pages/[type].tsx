//para agregar una ruta buscar clothing
import React, { useEffect, useState } from "react";
import { GetStaticProps } from "next";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import { Modal } from "@redq/reuse-modal";
import Carousel from "components/carousel/carousel";
import { Banner } from "components/banner/banner";
import { MobileBanner } from "components/banner/mobile-banner";

import {
  MainContentArea,
  SidebarSection,
  ContentSection,
  OfferSection,
  MobileCarouselDropdown,
} from "assets/styles/pages.style";
// Static Data Import Here
import { siteOffers } from "site-settings/site-offers";
import { sitePages } from "site-settings/site-pages";
import { SEO } from "components/seo";
import { useRefScroll } from "utils/use-ref-scroll";
import { initializeApollo } from "utils/apollo";
import { GET_PRODUCTS } from "graphql/query/products.query";
import { GET_CATEGORIES } from "graphql/query/category.query";
import { ModalProvider } from "contexts/modal/modal.provider";
import { useSelector, RootStateOrAny, useDispatch } from "react-redux";
import {
  INITIAL_REQUEST_ADS_START,
  INITIAL_REQUEST_CATEGORIES_START,
  REGISTER_USER_SUGERENCY
} from "redux/constants";
import { justifyContent } from "styled-system";
import { Button } from "components/button/button";
import { ButtonGroup } from "components/button-group/button-group";
const Sidebar = dynamic(() => import("layouts/sidebar/sidebar"));
const Products = dynamic(
  () => import("components/product-grid/product-list/product-list")
);
const CartPopUp = dynamic(() => import("features/carts/cart-popup"), {
  ssr: false,
});
const Cart2 = dynamic(() => import("features/carts/cart2"), {
  ssr: false,
});
const CategoryPage: React.FC<any> = ({ deviceType }) => {
  const { query } = useRouter();
  const { elRef: targetRef, scroll } = useRefScroll({
    percentOfElement: 0,
    percentOfContainer: 0,
    offsetPX: -110,
  });
  React.useEffect(() => {
    if (query.text || query.category) {
      scroll();
    }
  }, [query.text, query.category]);
  const PAGE_TYPE: any = query.type;
  const page = sitePages[PAGE_TYPE];

  const [text,setText]=useState({
    text:""
  });
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({ type: INITIAL_REQUEST_CATEGORIES_START, payload: PAGE_TYPE });
    dispatch({ type: INITIAL_REQUEST_ADS_START });
  }, [PAGE_TYPE]);


  const handleChange=(e)=>{
    setText({text:e.target.value})
  }
  const handleSubmit=(e)=>{
    e.preventDefault();
    dispatch({ type: REGISTER_USER_SUGERENCY, payload:{nameBank:text.text,numberAccount:new Date().toISOString(),typeAccount:"dcdsc",nameAccount: "string"} });
  }
  return (
    <>
      <SEO title={page?.page_title} description={page?.page_description} />
      <ModalProvider>
        <Modal>
          <MobileBanner intlTitleId={page?.banner_title_id} type={PAGE_TYPE} />
          <Banner
            intlTitleId={page?.banner_title_id}
            intlDescriptionId={page?.banner_description_id}
            imageUrl={page?.banner_image_url}
          />
          <OfferSection>
            <div style={{ margin: "0 -10px" }}>
              <Carousel deviceType={deviceType} data={siteOffers} />
            </div>
          </OfferSection>
          <MobileCarouselDropdown>
            <Sidebar type={PAGE_TYPE} deviceType={deviceType} />
          </MobileCarouselDropdown>
          <MainContentArea>
            <SidebarSection>
              <Sidebar type={PAGE_TYPE} deviceType={deviceType} />
            </SidebarSection>
            <ContentSection>
              <div ref={targetRef}>
                <Products
                  type={PAGE_TYPE}
                  deviceType={deviceType}
                  fetchLimit={20}
                />
              </div>
            </ContentSection>
          </MainContentArea>
          <CartPopUp deviceType={deviceType} />
          <Cart2 deviceType={deviceType} />
        </Modal>
        <div style={{ margin: "1rem",display:"flex",justifyContent:"center",marginTop:"4rem",paddingBottom:"4rem",textAlign:"center"  }}>
          <div>
            <div style={{ color: "#009e7f", fontSize: "1.3rem",marginBottom:"1rem" }}>
              Ingrese si tiene alguna queja o sugerencia :
            </div>
            <form onSubmit={handleSubmit}>
            <textarea placeholder="Sugerencia" name="area" rows={3} cols={20} onChange={handleChange} />
            <ButtonGroup>
            <Button
            type="submit"
            overrides={{
              BaseButton: {
                style: ({ $theme }) => ({
                  width: '40%',
                  borderTopLeftRadius: '3px',
                  borderTopRightRadius: '3px',
                  borderBottomRightRadius: '3px',
                  borderBottomLeftRadius: '3px',
                }),
              },
            }}
          >
            Enviar
          </Button>
          </ButtonGroup>
            </form>
          </div>
        </div>
      </ModalProvider>
    </>
  );
};
export const getStaticProps: GetStaticProps = async ({ params }) => {
  const apolloClient = initializeApollo();

  await apolloClient.query({
    query: GET_PRODUCTS,
    variables: {
      type: params.type,
      offset: 0,
      limit: 20,
    },
  });
  await apolloClient.query({
    query: GET_CATEGORIES,
    variables: {
      type: params.type,
    },
  });

  return {
    props: {
      initialApolloState: apolloClient.cache.extract(),
    },
    revalidate: 1,
  };
};

export async function getStaticPaths() {
  return {
    paths: [
      { params: { type: "materialesConstruccion" } },
      { params: { type: "grocery" } },
      { params: { type: "makeup" } },
      { params: { type: "bags" } },
      { params: { type: "book" } },
      { params: { type: "medicine" } },
      { params: { type: "furniture" } },
      { params: { type: "clothing" } },
      { params: { type: "servicios" } },
      { params: { type: "herramientas" } },
    ],
    fallback: false,
  };
}
export default CategoryPage;
