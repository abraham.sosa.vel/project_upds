import React from 'react';
import {
  CartPopupButtonStyled,
  ButtonImgBox,
  ItemCount,
  PriceBox,
  CartPopupBoxButton,
  PriceBoxAlt,
  TotalItems,
} from './cart-popup.style';
import { ShoppingBag } from 'assets/icons/ShoppingBag';
import Link from 'next/link';
import { Whatsapp } from 'assets/icons/Whatsapp';
import { Box } from 'assets/icons/box';

type CartButtonProps = {
  style?: React.CSSProperties;
  itemCount?: number;
  itemPostfix?: any;
  price?: number;
  pricePrefix?: string;
  className?: string;
  onClick?: (e: any) => void;
};

const CartPopupButton: React.FC<CartButtonProps> = ({
  itemCount,
  itemPostfix = 'items',
  price,
  pricePrefix = 'Bs',
  style,
  onClick,
  className,
}) => (
  <CartPopupButtonStyled style={style} className={className}>
   <Link href="/order">
    <div>
    <Box />
   <div>mis pedidos</div>
   </div>
   </Link>
   <a style={{color:"#000000",marginLeft:"2.5rem",marginRight:"2.5rem"}} href="https://api.whatsapp.com/send?phone=+59169719934&text=Me%20gustaria%20consultar%20sobre%20el%20siguiente%20producto" target="_blanck">
   <Whatsapp />
    <div>whatsap</div>
    </a>
  </CartPopupButtonStyled>
);

export const BoxedCartButton: React.FC<CartButtonProps> = ({
  itemCount,
  itemPostfix = 'items',
  price,
  pricePrefix = 'Bs',
  style,
  onClick,
  className,
}) => (
  <CartPopupBoxButton style={style} className={className}>
   <Link href="/order">
     <div>
     <Box />
   <div>mis pedidos</div>
   </div>
   </Link>
   <a style={{color:"#000000",margin:".5rem 0"}} href="https://api.whatsapp.com/send?phone=+59169719934&text=Me%20gustaria%20consultar%20sobre%20el%20siguiente%20producto" target="_blanck">
   <Whatsapp />
    <div>whatsap</div>
    </a>
  </CartPopupBoxButton>
);

export default CartPopupButton;
