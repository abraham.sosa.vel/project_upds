import React, { useContext } from "react";
import * as Yup from "yup";
import { withFormik, FormikProps, Form } from "formik";
import { closeModal } from "@redq/reuse-modal";
import TextField from "components/forms/text-field";
import { Button } from "components/button/button";
import { useMutation } from "@apollo/client";
import { UPDATE_ADDRESS } from "graphql/mutation/address";
import { FieldWrapper, Heading } from "./address-card.style";
import { ProfileContext } from "contexts/profile/profile.context";
import { FormattedMessage } from "react-intl";
import { useDispatch } from "react-redux";
import {
  LOGIN_USER_START,
  REGISTER_USER_START,
  SEARCH_USERS_START,
  UPDATE_USER_START,
} from "redux/constants";

// Shape of form values
interface FormValues {
  id?: number | null;
  firstName?: string;
  direction?: string;
  celphone?: string;
  lastName?: string;
}

// The type of props MyForm receives
interface MyFormProps {
  item?: any | null;
}

// Wrap our form with the using withFormik HoC
const FormEnhancer = withFormik<MyFormProps, FormValues>({
  // Transform outer props into form values
  mapPropsToValues: (props) => {
    return {
      id: props.item.id || null,
      firstName: props.item.firstName || "",
      direction: props.item.direction || "",
      celphone: props.item.celphone || "",
      lastName: props.item.lastName || "",
    };
  },
  validationSchema: Yup.object().shape({
    firstName: Yup.string().required("El nombre es requerido"),
    direction: Yup.string().required("La direccion es requerida"),
    celphone: Yup.string().required("El celular es requerida"),
    lastName: Yup.string().required("El apellido es requerida"),
  }),
  handleSubmit: (values) => {
    console.log(values, "values");
    // do submitting things
  },
});

const UpdateAddress = (props: FormikProps<FormValues> & MyFormProps) => {
  const {
    isValid,
    item,
    values,
    touched,
    errors,
    dirty,
    handleChange,
    handleBlur,

    handleReset,
    isSubmitting,
  } = props;
  const addressValue = {
    id: values.id,
    type: "secondary",
    firstName: values.firstName,
    direction: values.direction,
    celphone: values.celphone,
    lastName: values.lastName,
  };
  let dispatch = useDispatch();
  const handleSubmit = () => {
    return new Promise((resolve, reject) => {
      typeof item.id == "undefined"
        ? dispatch({
            type: REGISTER_USER_START,
            payload: {
              firstName: addressValue.firstName,
              lastName: addressValue.lastName,
              celphone: addressValue.celphone,
              direction: addressValue.direction,
              roleId: "d1254697-6d43-451a-876f-7b1ca235e0ae",
              resolve,
              reject,
            },
          })
        : dispatch({
            type: UPDATE_USER_START,
            payload: { addressValue, resolve, reject },
          });
    })
      .then((res) => {
        return new Promise((resolve, reject) => {
          dispatch({ type: LOGIN_USER_START,payload:{resolve,reject} });
        }).then((res) => {
          closeModal();
        }).catch((err)=>{
          console.log(err)
        })
      })
      .catch((err) => {
        console.log("error");
      });
  };
  let changeaa=(e)=>{
    if(typeof item.id == "undefined"){
      let s=e.target.value
        return new Promise((resolve, reject) => {
          dispatch({type:SEARCH_USERS_START,payload:{s,resolve,reject}})
        }) .then((res) => {
          return new Promise((resolve, reject) => {
            dispatch({ type: LOGIN_USER_START,payload:{resolve,reject} });
          }).then((res) => {
            closeModal();
          }).catch((err)=>{
            console.log(err)
          })
        }).catch((res)=>{
          console.log(res)
        })
      }
  }
  return (
    <Form>
      <Heading>
        {item && item.id ? "Editar direccion" : "Añadir direccion"}
      </Heading>
      <FieldWrapper>
        <TextField
          id="celphone"
          type="number"
          placeholder="Ingrese su celular"
          error={touched.celphone && errors.celphone}
          value={values.celphone}
          onChange={handleChange}
          onBlur={changeaa}
        />
      </FieldWrapper>

      <FieldWrapper>
        <TextField
          id="firstName"
          type="text"
          placeholder="Ingrese su nombre"
          error={touched.firstName && errors.firstName}
          value={values.firstName}
          onChange={handleChange}
          onBlur={handleBlur}
        />
      </FieldWrapper>
      <FieldWrapper>
        <TextField
          id="lastName"
          type="text"
          placeholder="Ingrese su apellido"
          error={touched.lastName && errors.lastName}
          value={values.lastName}
          onChange={handleChange}
          onBlur={handleBlur}
        />
      </FieldWrapper>
      <FieldWrapper>
        <TextField
          id="direction"
          as="textarea"
          placeholder="Ingrese su direccion"
          error={touched.direction && errors.direction}
          value={values.direction}
          onChange={handleChange}
          onBlur={handleBlur}
        />
      </FieldWrapper>

      <Button
        onClick={handleSubmit}
        type="Guardar"
        style={{ width: "100%", height: "44px" }}
      >
        <FormattedMessage
          id="savedAddressId"
          defaultMessage="Direccion guardada"
        />
      </Button>
    </Form>
  );
};

export default FormEnhancer(UpdateAddress);
