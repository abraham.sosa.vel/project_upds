import localEn from './lang/en.json';
import localEs from './lang/es.json';


export const messages = {
  en: localEn,
  es: localEs,
};
