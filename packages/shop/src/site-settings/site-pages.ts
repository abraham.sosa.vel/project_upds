import groceryImage from 'assets/images/banner/grocery.png';
import makeupImage from 'assets/images/banner/makeup.png';
import bagsImage from 'assets/images/banner/bags.png';
import clothingImage from 'assets/images/banner/cloths.png';
import booksImage from 'assets/images/banner/books.png';
import furnitureImage from 'assets/images/banner/furniture.png';
import medicineImage from 'assets/images/banner/medicine.png';
import bakeryImage from 'assets/images/banner/bakery.jpg';

export const sitePages = {
  materialesConstruccion: {
    page_title: 'Materiales de construccion - Ferreteria',
    page_description: 'Materiales de construccion Details',
    banner_title_id: 'constructionTitle',
    banner_description_id: 'constructionSubTitle',
    banner_image_url: medicineImage,
  },
  grocery: {
    page_title: 'Materiales - Ferreteria',
    page_description: 'Grocery Details',
    banner_title_id: 'groceriesTitle',
    banner_description_id: 'groceriesSubTitle',
    banner_image_url: groceryImage,
  },
  bakery: {
    page_title: 'Bakery - PickBazar',
    page_description: 'Bakery Details',
    banner_title_id: 'bakeryTitle',
    banner_description_id: 'bakerySubTitle',
    banner_image_url: bakeryImage,
  },
  makeup: {
    page_title: 'Herramientas - Ferreteria',
    page_description: 'Makeup Details',
    banner_title_id: 'makeupTitle',
    banner_description_id: 'makeupSubTitle',
    banner_image_url: makeupImage,
  },
  bags: {
    page_title: 'Bags - PickBazar',
    page_description: 'Bags Details',
    banner_title_id: 'bagsTitle',
    banner_description_id: 'bagsSubTitle',
    banner_image_url: bagsImage,
  },
  clothing: {
    page_title: 'Servicios - Ferreteria',
    page_description: 'Clothing Details',
    banner_title_id: 'womenClothsTitle',
    banner_description_id: 'womenClothsSubTitle',
    banner_image_url: clothingImage,
  },
  furniture: {
    page_title: 'Furniture - Ferreteria',
    page_description: 'Furniture Details',
    banner_title_id: 'furnitureTitle',
    banner_description_id: 'furnitureSubTitle',
    banner_image_url: furnitureImage,
  },
  book: {
    page_title: 'Book - Ferreteria',
    page_description: 'Book Details',
    banner_title_id: 'booksTitle',
    banner_description_id: 'booksSubTitle',
    banner_image_url: booksImage,
  },
  medicine: {
    page_title: 'Medicine - PickBazar',
    page_description: 'Medicine Details',
    banner_title_id: 'medicineTitle',
    banner_description_id: 'medicineSubTitle',
    banner_image_url: medicineImage,
  },
  herramientas: {
    page_title: 'Herramientas - Ferreteria',
    page_description: 'Herramientas Details',
    banner_title_id: 'toolsTitle',
    banner_description_id: 'toolsSubTitle',
    banner_image_url: medicineImage,
  },
  servicios: {
    page_title: 'Servicios - Ferreteria',
    page_description: 'servicios Details',
    banner_title_id: 'servicesTitle',
    banner_description_id: 'servicesSubTitle',
    banner_image_url: medicineImage,
  },
};
